package mars.roo.infrastructure.cafebazaar;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import mars.roo.domain.CafebazaarEvent;

/**
 * TestCases for CafeBazaar InAppBilling API
 *
 * https://cafebazaar.ir/developers/docs/developer-api/v2/ref/validate/
 * https://cafebazaar.ir/developers/docs/developer-api/v2/ref/get-subs/
 */
public class CafebazaarApiClientTestCase {

    @Test
    public void checkSubscription() {
        CafebazaarApiClient apiClient = new CafebazaarApiClient();
        SubscriptionCheckDTO dto = apiClient.subscriptionCheck(aRegisteredEvent());
        Assert.assertEquals(new Long(1529020800000l), dto.getInitiationTimestampMsec());
    }

    @Test @Ignore
    public void checkPurchase() {
        CafebazaarApiClient apiClient = new CafebazaarApiClient();
        PurchaseCheckDTO dto = apiClient.purchaseCheck(aRegisteredEvent());
        Assert.assertEquals(new Integer(0), dto.getPurchaseState());
    }

    private CafebazaarEvent aRegisteredEvent() {
        CafebazaarEvent event = new CafebazaarEvent();
        event.setPackageName("mars.roo");
        event.setProductId("SKU_MONTHLY_TEST");
        event.setPurchaseToken("fxrpzUExWJC1PzJE");
        return event;
    }

}
