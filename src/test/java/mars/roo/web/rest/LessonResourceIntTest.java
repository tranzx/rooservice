package mars.roo.web.rest;

import static mars.roo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import mars.roo.RooApp;
import mars.roo.domain.Category;
import mars.roo.domain.Lesson;
import mars.roo.domain.enumeration.Level;
import mars.roo.repository.LessonRepository;
import mars.roo.service.LessonQueryService;
import mars.roo.service.LessonService;
import mars.roo.service.LessonWordService;
import mars.roo.service.QuestionService;
import mars.roo.web.rest.errors.ExceptionTranslator;
import mars.roo.web.rest.lesson.LessonResource;
/**
 * Test class for the LessonResource REST controller.
 *
 * @see LessonResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RooApp.class)
public class LessonResourceIntTest {

    private static final String DEFAULT_UUID = "AAAAAAAAAA";
    private static final String UPDATED_UUID = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE_ENG = "AAAAAAAAAA";
    private static final String UPDATED_TITLE_ENG = "BBBBBBBBBB";

    private static final Integer DEFAULT_INDEX_ORDER = 1;
    private static final Integer UPDATED_INDEX_ORDER = 2;

    private static final String DEFAULT_PICTURE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PICTURE_NAME = "BBBBBBBBBB";

    private static final Level DEFAULT_LEVEL = Level.Beginner;
    private static final Level UPDATED_LEVEL = Level.Intermediate;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LessonService lessonService;

    @Autowired
    private LessonWordService lessonWordService;

    @Autowired
    private LessonQueryService lessonQueryService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLessonMockMvc;

    private Lesson lesson;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LessonResource lessonResource = new LessonResource(lessonService, lessonQueryService, lessonWordService, questionService);
        this.restLessonMockMvc = MockMvcBuilders.standaloneSetup(lessonResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Lesson createEntity(EntityManager em) {
        Lesson lesson = new Lesson()
            .uuid(DEFAULT_UUID)
            .titleEng(DEFAULT_TITLE_ENG)
            .indexOrder(DEFAULT_INDEX_ORDER)
            .pictureName(DEFAULT_PICTURE_NAME)
            .level(DEFAULT_LEVEL);
        // Add required entity
        Category category = CategoryResourceIntTest.createEntity(em);
        em.persist(category);
        em.flush();
        lesson.setCategory(category);
        return lesson;
    }

    @Before
    public void initTest() {
        lesson = createEntity(em);
    }

    @Test
    @Transactional
    public void createLesson() throws Exception {
        int databaseSizeBeforeCreate = lessonRepository.findAll().size();

        // Create the Lesson
        restLessonMockMvc.perform(post("/api/lessons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lesson)))
            .andExpect(status().isCreated());

        // Validate the Lesson in the database
        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeCreate + 1);
        Lesson testLesson = lessonList.get(lessonList.size() - 1);
        assertThat(testLesson.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testLesson.getTitleEng()).isEqualTo(DEFAULT_TITLE_ENG);
        assertThat(testLesson.getIndexOrder()).isEqualTo(DEFAULT_INDEX_ORDER);
        assertThat(testLesson.getPictureName()).isEqualTo(DEFAULT_PICTURE_NAME);
        assertThat(testLesson.getLevel()).isEqualTo(DEFAULT_LEVEL);
    }

    @Test
    @Transactional
    public void createLessonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lessonRepository.findAll().size();

        // Create the Lesson with an existing ID
        lesson.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLessonMockMvc.perform(post("/api/lessons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lesson)))
            .andExpect(status().isBadRequest());

        // Validate the Lesson in the database
        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUuidIsRequired() throws Exception {
        int databaseSizeBeforeTest = lessonRepository.findAll().size();
        // set the field null
        lesson.setUuid(null);

        // Create the Lesson, which fails.

        restLessonMockMvc.perform(post("/api/lessons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lesson)))
            .andExpect(status().isBadRequest());

        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIndexOrderIsRequired() throws Exception {
        int databaseSizeBeforeTest = lessonRepository.findAll().size();
        // set the field null
        lesson.setIndexOrder(null);

        // Create the Lesson, which fails.

        restLessonMockMvc.perform(post("/api/lessons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lesson)))
            .andExpect(status().isBadRequest());

        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLessons() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList
        restLessonMockMvc.perform(get("/api/lessons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lesson.getId().intValue())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID.toString())))
            .andExpect(jsonPath("$.[*].titleEng").value(hasItem(DEFAULT_TITLE_ENG.toString())))
            .andExpect(jsonPath("$.[*].indexOrder").value(hasItem(DEFAULT_INDEX_ORDER)))
            .andExpect(jsonPath("$.[*].pictureName").value(hasItem(DEFAULT_PICTURE_NAME.toString())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL.toString())));
    }

    @Test
    @Transactional
    public void getLesson() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get the lesson
        restLessonMockMvc.perform(get("/api/lessons/{id}", lesson.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(lesson.getId().intValue()))
            .andExpect(jsonPath("$.uuid").value(DEFAULT_UUID.toString()))
            .andExpect(jsonPath("$.titleEng").value(DEFAULT_TITLE_ENG.toString()))
            .andExpect(jsonPath("$.indexOrder").value(DEFAULT_INDEX_ORDER))
            .andExpect(jsonPath("$.pictureName").value(DEFAULT_PICTURE_NAME.toString()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL.toString()));
    }

    @Test
    @Transactional
    public void getAllLessonsByUuidIsEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where uuid equals to DEFAULT_UUID
        defaultLessonShouldBeFound("uuid.equals=" + DEFAULT_UUID);

        // Get all the lessonList where uuid equals to UPDATED_UUID
        defaultLessonShouldNotBeFound("uuid.equals=" + UPDATED_UUID);
    }

    @Test
    @Transactional
    public void getAllLessonsByUuidIsInShouldWork() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where uuid in DEFAULT_UUID or UPDATED_UUID
        defaultLessonShouldBeFound("uuid.in=" + DEFAULT_UUID + "," + UPDATED_UUID);

        // Get all the lessonList where uuid equals to UPDATED_UUID
        defaultLessonShouldNotBeFound("uuid.in=" + UPDATED_UUID);
    }

    @Test
    @Transactional
    public void getAllLessonsByUuidIsNullOrNotNull() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where uuid is not null
        defaultLessonShouldBeFound("uuid.specified=true");

        // Get all the lessonList where uuid is null
        defaultLessonShouldNotBeFound("uuid.specified=false");
    }

    @Test
    @Transactional
    public void getAllLessonsByTitleEngIsEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where titleEng equals to DEFAULT_TITLE_ENG
        defaultLessonShouldBeFound("titleEng.equals=" + DEFAULT_TITLE_ENG);

        // Get all the lessonList where titleEng equals to UPDATED_TITLE_ENG
        defaultLessonShouldNotBeFound("titleEng.equals=" + UPDATED_TITLE_ENG);
    }

    @Test
    @Transactional
    public void getAllLessonsByTitleEngIsInShouldWork() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where titleEng in DEFAULT_TITLE_ENG or UPDATED_TITLE_ENG
        defaultLessonShouldBeFound("titleEng.in=" + DEFAULT_TITLE_ENG + "," + UPDATED_TITLE_ENG);

        // Get all the lessonList where titleEng equals to UPDATED_TITLE_ENG
        defaultLessonShouldNotBeFound("titleEng.in=" + UPDATED_TITLE_ENG);
    }

    @Test
    @Transactional
    public void getAllLessonsByTitleEngIsNullOrNotNull() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where titleEng is not null
        defaultLessonShouldBeFound("titleEng.specified=true");

        // Get all the lessonList where titleEng is null
        defaultLessonShouldNotBeFound("titleEng.specified=false");
    }

    @Test
    @Transactional
    public void getAllLessonsByIndexOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where indexOrder equals to DEFAULT_INDEX_ORDER
        defaultLessonShouldBeFound("indexOrder.equals=" + DEFAULT_INDEX_ORDER);

        // Get all the lessonList where indexOrder equals to UPDATED_INDEX_ORDER
        defaultLessonShouldNotBeFound("indexOrder.equals=" + UPDATED_INDEX_ORDER);
    }

    @Test
    @Transactional
    public void getAllLessonsByIndexOrderIsInShouldWork() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where indexOrder in DEFAULT_INDEX_ORDER or UPDATED_INDEX_ORDER
        defaultLessonShouldBeFound("indexOrder.in=" + DEFAULT_INDEX_ORDER + "," + UPDATED_INDEX_ORDER);

        // Get all the lessonList where indexOrder equals to UPDATED_INDEX_ORDER
        defaultLessonShouldNotBeFound("indexOrder.in=" + UPDATED_INDEX_ORDER);
    }

    @Test
    @Transactional
    public void getAllLessonsByIndexOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where indexOrder is not null
        defaultLessonShouldBeFound("indexOrder.specified=true");

        // Get all the lessonList where indexOrder is null
        defaultLessonShouldNotBeFound("indexOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllLessonsByIndexOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where indexOrder greater than or equals to DEFAULT_INDEX_ORDER
        defaultLessonShouldBeFound("indexOrder.greaterOrEqualThan=" + DEFAULT_INDEX_ORDER);

        // Get all the lessonList where indexOrder greater than or equals to UPDATED_INDEX_ORDER
        defaultLessonShouldNotBeFound("indexOrder.greaterOrEqualThan=" + UPDATED_INDEX_ORDER);
    }

    @Test
    @Transactional
    public void getAllLessonsByIndexOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where indexOrder less than or equals to DEFAULT_INDEX_ORDER
        defaultLessonShouldNotBeFound("indexOrder.lessThan=" + DEFAULT_INDEX_ORDER);

        // Get all the lessonList where indexOrder less than or equals to UPDATED_INDEX_ORDER
        defaultLessonShouldBeFound("indexOrder.lessThan=" + UPDATED_INDEX_ORDER);
    }


    @Test
    @Transactional
    public void getAllLessonsByPictureNameIsEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where pictureName equals to DEFAULT_PICTURE_NAME
        defaultLessonShouldBeFound("pictureName.equals=" + DEFAULT_PICTURE_NAME);

        // Get all the lessonList where pictureName equals to UPDATED_PICTURE_NAME
        defaultLessonShouldNotBeFound("pictureName.equals=" + UPDATED_PICTURE_NAME);
    }

    @Test
    @Transactional
    public void getAllLessonsByPictureNameIsInShouldWork() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where pictureName in DEFAULT_PICTURE_NAME or UPDATED_PICTURE_NAME
        defaultLessonShouldBeFound("pictureName.in=" + DEFAULT_PICTURE_NAME + "," + UPDATED_PICTURE_NAME);

        // Get all the lessonList where pictureName equals to UPDATED_PICTURE_NAME
        defaultLessonShouldNotBeFound("pictureName.in=" + UPDATED_PICTURE_NAME);
    }

    @Test
    @Transactional
    public void getAllLessonsByPictureNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where pictureName is not null
        defaultLessonShouldBeFound("pictureName.specified=true");

        // Get all the lessonList where pictureName is null
        defaultLessonShouldNotBeFound("pictureName.specified=false");
    }

    @Test
    @Transactional
    public void getAllLessonsByLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where level equals to DEFAULT_LEVEL
        defaultLessonShouldBeFound("level.equals=" + DEFAULT_LEVEL);

        // Get all the lessonList where level equals to UPDATED_LEVEL
        defaultLessonShouldNotBeFound("level.equals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllLessonsByLevelIsInShouldWork() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where level in DEFAULT_LEVEL or UPDATED_LEVEL
        defaultLessonShouldBeFound("level.in=" + DEFAULT_LEVEL + "," + UPDATED_LEVEL);

        // Get all the lessonList where level equals to UPDATED_LEVEL
        defaultLessonShouldNotBeFound("level.in=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllLessonsByLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where level is not null
        defaultLessonShouldBeFound("level.specified=true");

        // Get all the lessonList where level is null
        defaultLessonShouldNotBeFound("level.specified=false");
    }

    @Test
    @Transactional
    public void getAllLessonsByCategoryIsEqualToSomething() throws Exception {
        // Initialize the database
        Category category = CategoryResourceIntTest.createEntity(em);
        category.setUuid(UUID.randomUUID().toString());
        em.persist(category);
        em.flush();
        lesson.setCategory(category);
        lessonRepository.saveAndFlush(lesson);
        Long categoryId = category.getId();

        // Get all the lessonList where category equals to categoryId
        defaultLessonShouldBeFound("categoryId.equals=" + categoryId);

        // Get all the lessonList where category equals to categoryId + 1
        defaultLessonShouldNotBeFound("categoryId.equals=" + (categoryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultLessonShouldBeFound(String filter) throws Exception {
        restLessonMockMvc.perform(get("/api/lessons?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lesson.getId().intValue())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID.toString())))
            .andExpect(jsonPath("$.[*].titleEng").value(hasItem(DEFAULT_TITLE_ENG.toString())))
            .andExpect(jsonPath("$.[*].indexOrder").value(hasItem(DEFAULT_INDEX_ORDER)))
            .andExpect(jsonPath("$.[*].pictureName").value(hasItem(DEFAULT_PICTURE_NAME.toString())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultLessonShouldNotBeFound(String filter) throws Exception {
        restLessonMockMvc.perform(get("/api/lessons?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingLesson() throws Exception {
        // Get the lesson
        restLessonMockMvc.perform(get("/api/lessons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLesson() throws Exception {
        // Initialize the database
        lessonService.save(lesson);

        int databaseSizeBeforeUpdate = lessonRepository.findAll().size();

        // Update the lesson
        Lesson updatedLesson = lessonRepository.findOne(lesson.getId());
        // Disconnect from session so that the updates on updatedLesson are not directly saved in db
        em.detach(updatedLesson);
        updatedLesson
            .uuid(UPDATED_UUID)
            .titleEng(UPDATED_TITLE_ENG)
            .indexOrder(UPDATED_INDEX_ORDER)
            .pictureName(UPDATED_PICTURE_NAME)
            .level(UPDATED_LEVEL);

        restLessonMockMvc.perform(put("/api/lessons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLesson)))
            .andExpect(status().isOk());

        // Validate the Lesson in the database
        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeUpdate);
        Lesson testLesson = lessonList.get(lessonList.size() - 1);
        assertThat(testLesson.getUuid()).isEqualTo(UPDATED_UUID);
        assertThat(testLesson.getTitleEng()).isEqualTo(UPDATED_TITLE_ENG);
        assertThat(testLesson.getIndexOrder()).isEqualTo(UPDATED_INDEX_ORDER);
        assertThat(testLesson.getPictureName()).isEqualTo(UPDATED_PICTURE_NAME);
        assertThat(testLesson.getLevel()).isEqualTo(UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void updateNonExistingLesson() throws Exception {
        int databaseSizeBeforeUpdate = lessonRepository.findAll().size();

        // Create the Lesson

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLessonMockMvc.perform(put("/api/lessons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lesson)))
            .andExpect(status().isCreated());

        // Validate the Lesson in the database
        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLesson() throws Exception {
        // Initialize the database
        lessonService.save(lesson);

        int databaseSizeBeforeDelete = lessonRepository.findAll().size();

        // Get the lesson
        restLessonMockMvc.perform(delete("/api/lessons/{id}", lesson.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Lesson.class);
        Lesson lesson1 = new Lesson();
        lesson1.setId(1L);
        Lesson lesson2 = new Lesson();
        lesson2.setId(lesson1.getId());
        assertThat(lesson1).isEqualTo(lesson2);
        lesson2.setId(2L);
        assertThat(lesson1).isNotEqualTo(lesson2);
        lesson1.setId(null);
        assertThat(lesson1).isNotEqualTo(lesson2);
    }
}
