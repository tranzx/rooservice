package mars.roo.web.rest;

import static mars.roo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import mars.roo.RooApp;
import mars.roo.domain.Category;
import mars.roo.repository.CategoryRepository;
import mars.roo.web.rest.category.CategoryResource;
import mars.roo.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the CategoryResource REST controller.
 *
 * @see CategoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RooApp.class)
public class CategoryResourceIntTest {

    private static final String DEFAULT_UUID = UUID.randomUUID().toString();
    private static final String UPDATED_UUID = UUID.randomUUID().toString();

    private static final String DEFAULT_TITLE_ENG = "AAAAAAAAAA";
    private static final String UPDATED_TITLE_ENG = "BBBBBBBBBB";

    private static final Integer DEFAULT_INDEX_ORDER = 1;
    private static final Integer UPDATED_INDEX_ORDER = 2;

    private static final Boolean DEFAULT_FOR_SELL = false;
    private static final Boolean UPDATED_FOR_SELL = true;

    private static final Boolean DEFAULT_CAME_NEW = false;
    private static final Boolean UPDATED_CAME_NEW = true;

    private static final Boolean DEFAULT_COMMING_SOON = false;
    private static final Boolean UPDATED_COMMING_SOON = true;

    private static final String DEFAULT_TITLE_FA = "AAAAAAAAAA";
    private static final String UPDATED_TITLE_FA = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE_DE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE_DE = "BBBBBBBBBB";

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCategoryMockMvc;

    private Category category;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CategoryResource categoryResource = new CategoryResource(categoryRepository);
        this.restCategoryMockMvc = MockMvcBuilders.standaloneSetup(categoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Category createEntity(EntityManager em) {
        Category category = new Category()
            .uuid(DEFAULT_UUID)
            .titleEng(DEFAULT_TITLE_ENG)
            .indexOrder(DEFAULT_INDEX_ORDER)
            .forSell(DEFAULT_FOR_SELL)
            .cameNew(DEFAULT_CAME_NEW)
            .commingSoon(DEFAULT_COMMING_SOON)
            .titleFa(DEFAULT_TITLE_FA)
            .titleDe(DEFAULT_TITLE_DE);
        return category;
    }

    @Before
    public void initTest() {
        category = createEntity(em);
    }

    @Test
    @Transactional
    public void createCategory() throws Exception {
        int databaseSizeBeforeCreate = categoryRepository.findAll().size();

        // Create the Category
        restCategoryMockMvc.perform(post("/api/categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(category)))
            .andExpect(status().isCreated());

        // Validate the Category in the database
        List<Category> categoryList = categoryRepository.findAll();
        assertThat(categoryList).hasSize(databaseSizeBeforeCreate + 1);
        Category testCategory = categoryList.get(categoryList.size() - 1);
        assertThat(testCategory.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testCategory.getTitleEng()).isEqualTo(DEFAULT_TITLE_ENG);
        assertThat(testCategory.getIndexOrder()).isEqualTo(DEFAULT_INDEX_ORDER);
        assertThat(testCategory.isForSell()).isEqualTo(DEFAULT_FOR_SELL);
        assertThat(testCategory.isCameNew()).isEqualTo(DEFAULT_CAME_NEW);
        assertThat(testCategory.isCommingSoon()).isEqualTo(DEFAULT_COMMING_SOON);
        assertThat(testCategory.getTitleFa()).isEqualTo(DEFAULT_TITLE_FA);
        assertThat(testCategory.getTitleDe()).isEqualTo(DEFAULT_TITLE_DE);
    }

    @Test
    @Transactional
    public void createCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = categoryRepository.findAll().size();

        // Create the Category with an existing ID
        category.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategoryMockMvc.perform(post("/api/categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(category)))
            .andExpect(status().isBadRequest());

        // Validate the Category in the database
        List<Category> categoryList = categoryRepository.findAll();
        assertThat(categoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUuidIsRequired() throws Exception {
        int databaseSizeBeforeTest = categoryRepository.findAll().size();
        // set the field null
        category.setUuid(null);

        // Create the Category, which fails.

        restCategoryMockMvc.perform(post("/api/categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(category)))
            .andExpect(status().isBadRequest());

        List<Category> categoryList = categoryRepository.findAll();
        assertThat(categoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIndexOrderIsRequired() throws Exception {
        int databaseSizeBeforeTest = categoryRepository.findAll().size();
        // set the field null
        category.setIndexOrder(null);

        // Create the Category, which fails.

        restCategoryMockMvc.perform(post("/api/categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(category)))
            .andExpect(status().isBadRequest());

        List<Category> categoryList = categoryRepository.findAll();
        assertThat(categoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCategories() throws Exception {
        // Initialize the database
        categoryRepository.saveAndFlush(category);

        // Get all the categoryList
        restCategoryMockMvc.perform(get("/api/categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(category.getId().intValue())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID.toString())))
            .andExpect(jsonPath("$.[*].titleEng").value(hasItem(DEFAULT_TITLE_ENG.toString())))
            .andExpect(jsonPath("$.[*].indexOrder").value(hasItem(DEFAULT_INDEX_ORDER)))
            .andExpect(jsonPath("$.[*].forSell").value(hasItem(DEFAULT_FOR_SELL.booleanValue())))
            .andExpect(jsonPath("$.[*].cameNew").value(hasItem(DEFAULT_CAME_NEW.booleanValue())))
            .andExpect(jsonPath("$.[*].commingSoon").value(hasItem(DEFAULT_COMMING_SOON.booleanValue())))
            .andExpect(jsonPath("$.[*].titleFa").value(hasItem(DEFAULT_TITLE_FA.toString())))
            .andExpect(jsonPath("$.[*].titleDe").value(hasItem(DEFAULT_TITLE_DE.toString())));
    }

    @Test
    @Transactional
    public void getCategory() throws Exception {
        // Initialize the database
        categoryRepository.saveAndFlush(category);

        // Get the category
        restCategoryMockMvc.perform(get("/api/categories/{id}", category.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(category.getId().intValue()))
            .andExpect(jsonPath("$.uuid").value(DEFAULT_UUID.toString()))
            .andExpect(jsonPath("$.titleEng").value(DEFAULT_TITLE_ENG.toString()))
            .andExpect(jsonPath("$.indexOrder").value(DEFAULT_INDEX_ORDER))
            .andExpect(jsonPath("$.forSell").value(DEFAULT_FOR_SELL.booleanValue()))
            .andExpect(jsonPath("$.cameNew").value(DEFAULT_CAME_NEW.booleanValue()))
            .andExpect(jsonPath("$.commingSoon").value(DEFAULT_COMMING_SOON.booleanValue()))
            .andExpect(jsonPath("$.titleFa").value(DEFAULT_TITLE_FA.toString()))
            .andExpect(jsonPath("$.titleDe").value(DEFAULT_TITLE_DE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCategory() throws Exception {
        // Get the category
        restCategoryMockMvc.perform(get("/api/categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategory() throws Exception {
        // Initialize the database
        categoryRepository.saveAndFlush(category);
        int databaseSizeBeforeUpdate = categoryRepository.findAll().size();

        // Update the category
        Category updatedCategory = categoryRepository.findOne(category.getId());
        // Disconnect from session so that the updates on updatedCategory are not directly saved in db
        em.detach(updatedCategory);
        updatedCategory
            .uuid(UPDATED_UUID)
            .titleEng(UPDATED_TITLE_ENG)
            .indexOrder(UPDATED_INDEX_ORDER)
            .forSell(UPDATED_FOR_SELL)
            .cameNew(UPDATED_CAME_NEW)
            .commingSoon(UPDATED_COMMING_SOON)
            .titleFa(UPDATED_TITLE_FA)
            .titleDe(UPDATED_TITLE_DE);

        restCategoryMockMvc.perform(put("/api/categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCategory)))
            .andExpect(status().isOk());

        // Validate the Category in the database
        List<Category> categoryList = categoryRepository.findAll();
        assertThat(categoryList).hasSize(databaseSizeBeforeUpdate);
        Category testCategory = categoryList.get(categoryList.size() - 1);
        assertThat(testCategory.getUuid()).isEqualTo(UPDATED_UUID);
        assertThat(testCategory.getTitleEng()).isEqualTo(UPDATED_TITLE_ENG);
        assertThat(testCategory.getIndexOrder()).isEqualTo(UPDATED_INDEX_ORDER);
        assertThat(testCategory.isForSell()).isEqualTo(UPDATED_FOR_SELL);
        assertThat(testCategory.isCameNew()).isEqualTo(UPDATED_CAME_NEW);
        assertThat(testCategory.isCommingSoon()).isEqualTo(UPDATED_COMMING_SOON);
        assertThat(testCategory.getTitleFa()).isEqualTo(UPDATED_TITLE_FA);
        assertThat(testCategory.getTitleDe()).isEqualTo(UPDATED_TITLE_DE);
    }

    @Test
    @Transactional
    public void updateNonExistingCategory() throws Exception {
        int databaseSizeBeforeUpdate = categoryRepository.findAll().size();

        // Create the Category

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCategoryMockMvc.perform(put("/api/categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(category)))
            .andExpect(status().isCreated());

        // Validate the Category in the database
        List<Category> categoryList = categoryRepository.findAll();
        assertThat(categoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCategory() throws Exception {
        // Initialize the database
        categoryRepository.saveAndFlush(category);
        int databaseSizeBeforeDelete = categoryRepository.findAll().size();

        // Get the category
        restCategoryMockMvc.perform(delete("/api/categories/{id}", category.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Category> categoryList = categoryRepository.findAll();
        assertThat(categoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Category.class);
        Category category1 = new Category();
        category1.setId(1L);
        Category category2 = new Category();
        category2.setId(category1.getId());
        assertThat(category1).isEqualTo(category2);
        category2.setId(2L);
        assertThat(category1).isNotEqualTo(category2);
        category1.setId(null);
        assertThat(category1).isNotEqualTo(category2);
    }
}
