package mars.roo.domain.words;

import java.util.Arrays;
import java.util.Iterator;
import java.util.TreeSet;

import org.junit.Test;

import mars.roo.web.rest.question.CommWordComparator;
import mars.roo.web.rest.question.CommWordPublicDTO.CWord;

import static org.junit.Assert.*;

public class CommWordComparatorTest {

    final CWord c1 = new CWord("Aauthor", 1, null, null, "unique1");
    final CWord c2 = new CWord("Bbase", null, 1, null, "unique2");
    final CWord c3 = new CWord("Cvote", null, null, 3, "unique3");
    final CWord c3_2 = new CWord("Cvote2", null, null, 2, "unique3_2");
    final CWord c3_3 = new CWord("Cvote3", null, null, 1, "unique3_3");
    final CWord c4 = new CWord("Dnorm", null, null, null, "unique4");

    @Test
    public void testComparator() {
        TreeSet<CWord> tset = new TreeSet<CWord>(new CommWordComparator());
        tset.addAll(Arrays.asList(c4, c2, c3, c1, c3_2, c3_3));
        Iterator<CWord> itr = tset.iterator();
        assertEquals(c1, itr.next());
        assertEquals(c2, itr.next());
        assertEquals(c3, itr.next());
        assertEquals(c3_2, itr.next());
        assertEquals(c3_3, itr.next());
        assertEquals(c4, itr.next());
    }

}
