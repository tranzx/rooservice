package mars.roo.domain.enumeration;

import org.junit.Test;

import static org.junit.Assert.*;

public class LearnDirTest {

    @Test
    public void testFromTo() {
        assertEquals(Language.FA_IR, LearnDir.FA_IR$EN_GB.mother());
        assertEquals(Language.EN_GB, LearnDir.FA_IR$EN_GB.target());
    }

}
