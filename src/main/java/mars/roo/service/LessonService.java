package mars.roo.service;

import mars.roo.domain.Category;
import mars.roo.domain.Lesson;
import mars.roo.domain.enumeration.Level;
import mars.roo.repository.LessonRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Lesson.
 */
@Service
@Transactional
public class LessonService {

    private final Logger log = LoggerFactory.getLogger(LessonService.class);

    private final LessonRepository lessonRepository;

    public LessonService(LessonRepository lessonRepository) {
        this.lessonRepository = lessonRepository;
    }

    /**
     * Save a lesson.
     *
     * @param lesson the entity to save
     * @return the persisted entity
     */
    public Lesson save(Lesson lesson) {
        log.debug("Request to save Lesson : {}", lesson);
        return lessonRepository.save(lesson);
    }

    /**
     * Get all the lessons.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Lesson> findAll(Pageable pageable) {
        log.debug("Request to get all Lessons");
        return lessonRepository.findAll(pageable);
    }

    /**
     * Get one lesson by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Lesson findOne(Long id) {
        log.debug("Request to get Lesson : {}", id);
        return lessonRepository.findOne(id);
    }

    /**
     * Delete the lesson by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Lesson : {}", id);
        lessonRepository.delete(id);
    }

    public Lesson findOneByCategoryAndLevelAndIndexOrder(Category category, Level level, Integer indexOrder) {
        return lessonRepository.findOneByCategoryAndLevelAndIndexOrder(category, level, indexOrder);
    }

    public List<Lesson> findByCategoryAndLevel(Category category, Level level) {
        return lessonRepository.findByCategoryAndLevel(category, level);
    }

    public List<Lesson> findTop2ByCategoryAndLevelOrderByIndexOrderDesc(Category category, Level level) {
        return lessonRepository.findTop2ByCategoryAndLevelOrderByIndexOrderDesc(category, level);
    }

    public Lesson findByUuid(String uuid) {
        return lessonRepository.findOneByUuid(uuid);
    }

    public Lesson findOneByIndexOrderAndCategoryIsNull(Integer indexOrder) {
        return lessonRepository.findOneByIndexOrderAndCategoryIsNull(indexOrder);
    }

    public List<Lesson> findByCategoryId(Long categoryId) {
        return lessonRepository.findByCategoryId(categoryId);
    }

}
