package mars.roo.service;

import mars.roo.domain.Word;
import mars.roo.repository.WordRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Word.
 */
@Service
@Transactional
public class WordService {

    private final Logger log = LoggerFactory.getLogger(WordService.class);

    private final WordRepository wordRepository;

    public WordService(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    /**
     * Save a word.
     *
     * @param word the entity to save
     * @return the persisted entity
     */
    public Word save(Word word) {
        log.debug("Request to save Word : {}", word);
        return wordRepository.save(word);
    }

    public List<Word> save(List<Word> list) {
        return wordRepository.save(list);
    }

    /**
     * Get all the words.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Word> findAll(Pageable pageable) {
        log.debug("Request to get all Words");
        return wordRepository.findAll(pageable);
    }

    /**
     * Get one word by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Word findOne(Long id) {
        log.debug("Request to get Word : {}", id);
        return wordRepository.findOne(id);
    }

    /**
     * Delete the word by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Word : {}", id);
        wordRepository.delete(id);
    }
}
