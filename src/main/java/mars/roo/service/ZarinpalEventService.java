package mars.roo.service;

import mars.roo.domain.ZarinpalEvent;
import mars.roo.repository.ZarinpalEventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ZarinpalEvent.
 */
@Service
@Transactional
public class ZarinpalEventService {

    private final Logger log = LoggerFactory.getLogger(ZarinpalEventService.class);

    private final ZarinpalEventRepository zarinpalEventRepository;

    public ZarinpalEventService(ZarinpalEventRepository zarinpalEventRepository) {
        this.zarinpalEventRepository = zarinpalEventRepository;
    }

    /**
     * Save a zarinpalEvent.
     *
     * @param zarinpalEvent the entity to save
     * @return the persisted entity
     */
    public ZarinpalEvent save(ZarinpalEvent zarinpalEvent) {
        log.debug("Request to save ZarinpalEvent : {}", zarinpalEvent);
        return zarinpalEventRepository.save(zarinpalEvent);
    }

    /**
     * Get all the zarinpalEvents.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ZarinpalEvent> findAll(Pageable pageable) {
        log.debug("Request to get all ZarinpalEvents");
        return zarinpalEventRepository.findAll(pageable);
    }

    /**
     * Get one zarinpalEvent by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ZarinpalEvent findOne(Long id) {
        log.debug("Request to get ZarinpalEvent : {}", id);
        return zarinpalEventRepository.findOne(id);
    }

    /**
     * Delete the zarinpalEvent by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ZarinpalEvent : {}", id);
        zarinpalEventRepository.delete(id);
    }
}
