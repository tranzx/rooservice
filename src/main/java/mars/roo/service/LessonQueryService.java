package mars.roo.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mars.roo.domain.Lesson;
import mars.roo.domain.*; // for static metamodels
import mars.roo.repository.LessonRepository;
import mars.roo.service.dto.LessonCriteria;

import mars.roo.domain.enumeration.Level;

/**
 * Service for executing complex queries for Lesson entities in the database.
 * The main input is a {@link LessonCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Lesson} or a {@link Page} of {@link Lesson} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LessonQueryService extends QueryService<Lesson> {

    private final Logger log = LoggerFactory.getLogger(LessonQueryService.class);


    private final LessonRepository lessonRepository;

    public LessonQueryService(LessonRepository lessonRepository) {
        this.lessonRepository = lessonRepository;
    }

    /**
     * Return a {@link List} of {@link Lesson} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Lesson> findByCriteria(LessonCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Lesson> specification = createSpecification(criteria);
        return lessonRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Lesson} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Lesson> findByCriteria(LessonCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Lesson> specification = createSpecification(criteria);
        return lessonRepository.findAll(specification, page);
    }

    /**
     * Function to convert LessonCriteria to a {@link Specifications}
     */
    private Specifications<Lesson> createSpecification(LessonCriteria criteria) {
        Specifications<Lesson> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Lesson_.id));
            }
            if (criteria.getUuid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUuid(), Lesson_.uuid));
            }
            if (criteria.getTitleEng() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitleEng(), Lesson_.titleEng));
            }
            if (criteria.getIndexOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIndexOrder(), Lesson_.indexOrder));
            }
            if (criteria.getPictureName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPictureName(), Lesson_.pictureName));
            }
            if (criteria.getLevel() != null) {
                specification = specification.and(buildSpecification(criteria.getLevel(), Lesson_.level));
            }
            if (criteria.getCategoryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCategoryId(), Lesson_.category, Category_.id));
            }
        }
        return specification;
    }

}
