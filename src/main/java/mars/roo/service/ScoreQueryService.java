package mars.roo.service;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.StringFilter;
// for static metamodels
import mars.roo.domain.Score;
import mars.roo.domain.Score_;
import mars.roo.domain.enumeration.ScoreType;
import mars.roo.repository.ScoreRepository;
import mars.roo.service.dto.ScoreCriteria;
import mars.roo.service.dto.ScoreCriteria.ScoreTypeFilter;

/**
 * Service for executing complex queries for Score entities in the database.
 * The main input is a {@link ScoreCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Score} or a {@link Page} of {@link Score} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ScoreQueryService extends QueryService<Score> {

    private final Logger log = LoggerFactory.getLogger(ScoreQueryService.class);


    private final ScoreRepository scoreRepository;

    public ScoreQueryService(ScoreRepository scoreRepository) {
        this.scoreRepository = scoreRepository;
    }

    /**
     * Return a {@link List} of {@link Score} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Score> findByCriteria(ScoreCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Score> specification = createSpecification(criteria);
        return scoreRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Score} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Score> findByCriteria(ScoreCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Score> specification = createSpecification(criteria);
        return scoreRepository.findAll(specification, page);
    }

    @Transactional(readOnly = true)
    public Score findPreviousDoneDailyScore(String owner) {
        ScoreCriteria criteria = new ScoreCriteria();
        StringFilter ownerFilter = new StringFilter();
        ownerFilter.setEquals(owner);
        criteria.setOwner(ownerFilter);
        ScoreTypeFilter scoreType = new ScoreTypeFilter();
        scoreType.setEquals(ScoreType.DAILY);
        criteria.setType(scoreType);
        LocalDateFilter scoreDateFilter = new LocalDateFilter();
        scoreDateFilter.setLessThan(LocalDate.now(ZoneOffset.UTC));
        criteria.setScoreDate(scoreDateFilter);
        PageRequest pageRequest = new PageRequest(0, 1, Direction.DESC, "scoreDate");
        Page<Score> page = findByCriteria(criteria, pageRequest);
        return page.hasContent() ? page.getContent().get(0) : null;
    }

    /**
     * Function to convert ScoreCriteria to a {@link Specifications}
     */
    private Specifications<Score> createSpecification(ScoreCriteria criteria) {
        Specifications<Score> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Score_.id));
            }
            if (criteria.getOwner() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOwner(), Score_.owner));
            }
            if (criteria.getLearnDir() != null) {
                specification = specification.and(buildSpecification(criteria.getLearnDir(), Score_.learnDir));
            }
            if (criteria.getScore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getScore(), Score_.score));
            }
            if (criteria.getStar() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStar(), Score_.star));
            }
            if (criteria.getScoreDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getScoreDate(), Score_.scoreDate));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildSpecification(criteria.getType(), Score_.type));
            }
            if (criteria.getDifficultyLevel() != null) {
                specification = specification.and(buildSpecification(criteria.getDifficultyLevel(), Score_.difficultyLevel));
            }
            if (criteria.getLessonUuid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLessonUuid(), Score_.lessonUuid));
            }
            if (criteria.getCategoryUuid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategoryUuid(), Score_.categoryUuid));
            }
        }
        return specification;
    }

}
