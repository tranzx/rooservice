package mars.roo.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mars.roo.domain.LessonWord;
import mars.roo.domain.*; // for static metamodels
import mars.roo.repository.LessonWordRepository;
import mars.roo.service.dto.LessonWordCriteria;


/**
 * Service for executing complex queries for LessonWord entities in the database.
 * The main input is a {@link LessonWordCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LessonWord} or a {@link Page} of {@link LessonWord} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LessonWordQueryService extends QueryService<LessonWord> {

    private final Logger log = LoggerFactory.getLogger(LessonWordQueryService.class);


    private final LessonWordRepository lessonWordRepository;

    public LessonWordQueryService(LessonWordRepository lessonWordRepository) {
        this.lessonWordRepository = lessonWordRepository;
    }

    /**
     * Return a {@link List} of {@link LessonWord} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LessonWord> findByCriteria(LessonWordCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<LessonWord> specification = createSpecification(criteria);
        return lessonWordRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link LessonWord} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LessonWord> findByCriteria(LessonWordCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<LessonWord> specification = createSpecification(criteria);
        return lessonWordRepository.findAll(specification, page);
    }

    /**
     * Function to convert LessonWordCriteria to a {@link Specifications}
     */
    private Specifications<LessonWord> createSpecification(LessonWordCriteria criteria) {
        Specifications<LessonWord> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), LessonWord_.id));
            }
            if (criteria.getIndexOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIndexOrder(), LessonWord_.indexOrder));
            }
            if (criteria.getBackground() != null) {
                specification = specification.and(buildSpecification(criteria.getBackground(), LessonWord_.background));
            }
            if (criteria.getCapitalization() != null) {
                specification = specification.and(buildSpecification(criteria.getCapitalization(), LessonWord_.capitalization));
            }
            if (criteria.getWordId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getWordId(), LessonWord_.word, Word_.id));
            }
            if (criteria.getLessonId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getLessonId(), LessonWord_.lesson, Lesson_.id));
            }
        }
        return specification;
    }

}
