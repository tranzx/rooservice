package mars.roo.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mars.roo.domain.Lesson;
import mars.roo.domain.LessonCommWord;
import mars.roo.domain.LessonWord;
import mars.roo.domain.Word;
import mars.roo.repository.LessonCommWordRepository;
import mars.roo.repository.LessonRepository;
import mars.roo.repository.LessonWordRepository;
import mars.roo.repository.WordRepository;
import mars.roo.web.rest.lesson.LessonWordCommand;


/**
 * Service Implementation for managing LessonWord.
 */
@Service
@Transactional
public class LessonWordService {

    private final Logger log = LoggerFactory.getLogger(LessonWordService.class);

    private final LessonWordRepository lessonWordRepository;
    private final LessonRepository lessonRepository;
    private final WordRepository wordRepository;
    private final LessonCommWordRepository lessonCommWordRepository;

    public LessonWordService(LessonWordRepository lessonWordRepository,
                             LessonRepository lessonRepository,
                             WordRepository wordRepository,
                             LessonCommWordRepository lessonCommWordRepository) {
        this.lessonWordRepository = lessonWordRepository;
        this.lessonRepository = lessonRepository;
        this.wordRepository = wordRepository;
        this.lessonCommWordRepository = lessonCommWordRepository;
    }

    /**
     * Save a lessonWord.
     *
     * @param lessonWord the entity to save
     * @return the persisted entity
     */
    public LessonWord save(LessonWord lessonWord) {
        log.debug("Request to save LessonWord : {}", lessonWord);
        LessonWord saved = lessonWordRepository.save(lessonWord);
        List<LessonCommWord> lcws = lessonCommWordRepository.findByLessonIdAndWordId(saved.getLesson().getId(), saved.getWord().getId());
        lcws.forEach(lcw -> {
            lcw.setIndexOrder(saved.getIndexOrder());
            lcw.setPicture(saved.isPicture());
            lcw.setCapitalization(saved.isCapitalization());
            lcw.setBackground(saved.isBackground());
        });
        lessonCommWordRepository.save(lcws);
        return saved;
    }

    public List<LessonWord> save(List<LessonWord> lws) {
        return lessonWordRepository.save(lws);
    }

    public LessonWord saveOrFind(LessonWordCommand cmd) {
        Lesson lesson = lessonRepository.findOne(cmd.getLessonId());
        Word word = cmd.hasWordId() ? wordRepository.findOne(cmd.getWordId()) : wordRepository.save(cmd.toWord());
        return save(cmd.toLessonWord(lesson, word, nextIndexOrder(lesson)));
    }

    public Integer nextIndexOrder(Lesson lesson) {
       LessonWord lw = lessonWordRepository.findTop1ByLessonOrderByIndexOrderDesc(lesson);
       if (lw == null) {
           return 1;
       } else {
           return lw.getIndexOrder() + 1;
       }
    }

    /**
     * Get all the lessonWords.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LessonWord> findAll(Pageable pageable) {
        log.debug("Request to get all LessonWords");
        return lessonWordRepository.findAll(pageable);
    }

    /**
     * Get one lessonWord by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public LessonWord findOne(Long id) {
        log.debug("Request to get LessonWord : {}", id);
        return lessonWordRepository.findOne(id);
    }

    /**
     * Delete the lessonWord by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete LessonWord : {}", id);
        lessonWordRepository.delete(id);
    }

    public List<LessonWord> findByLesson(Lesson lesson) {
        return lessonWordRepository.findByLesson(lesson);
    }

    public void deleteByLesson(Lesson lesson) {
        lessonCommWordRepository.deleteByLessonId(lesson.getId());
        lessonWordRepository.deleteByLesson(lesson);
    }

}
