package mars.roo.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mars.roo.domain.Profile;
import mars.roo.domain.*; // for static metamodels
import mars.roo.repository.ProfileRepository;
import mars.roo.service.dto.ProfileCriteria;

import mars.roo.domain.enumeration.LearnDir;
import mars.roo.domain.enumeration.Level;

/**
 * Service for executing complex queries for Profile entities in the database.
 * The main input is a {@link ProfileCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Profile} or a {@link Page} of {@link Profile} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProfileQueryService extends QueryService<Profile> {

    private final Logger log = LoggerFactory.getLogger(ProfileQueryService.class);


    private final ProfileRepository profileRepository;

    public ProfileQueryService(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    /**
     * Return a {@link List} of {@link Profile} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Profile> findByCriteria(ProfileCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Profile> specification = createSpecification(criteria);
        return profileRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Profile} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Profile> findByCriteria(ProfileCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Profile> specification = createSpecification(criteria);
        return profileRepository.findAll(specification, page);
    }

    /**
     * Function to convert ProfileCriteria to a {@link Specifications}
     */
    private Specifications<Profile> createSpecification(ProfileCriteria criteria) {
        Specifications<Profile> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Profile_.id));
            }
            if (criteria.getOwner() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOwner(), Profile_.owner));
            }
            if (criteria.getLearning() != null) {
                specification = specification.and(buildSpecification(criteria.getLearning(), Profile_.learning));
            }
            if (criteria.getDifficultyLevel() != null) {
                specification = specification.and(buildSpecification(criteria.getDifficultyLevel(), Profile_.difficultyLevel));
            }
            if (criteria.getLanguage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLanguage(), Profile_.language));
            }
            if (criteria.getDname() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDname(), Profile_.dname));
            }
            if (criteria.getSoundEffects() != null) {
                specification = specification.and(buildSpecification(criteria.getSoundEffects(), Profile_.soundEffects));
            }
            if (criteria.getAutoPlayVoice() != null) {
                specification = specification.and(buildSpecification(criteria.getAutoPlayVoice(), Profile_.autoPlayVoice));
            }
            if (criteria.getAutoContinue() != null) {
                specification = specification.and(buildSpecification(criteria.getAutoContinue(), Profile_.autoContinue));
            }
            if (criteria.getVoiceSpeedRate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoiceSpeedRate(), Profile_.voiceSpeedRate));
            }
        }
        return specification;
    }

}
