package mars.roo.service.dto;

import java.io.Serializable;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import mars.roo.domain.enumeration.Level;
import mars.roo.domain.enumeration.QuestionType;






/**
 * Criteria class for the Question entity. This class is used in QuestionResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /questions?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class QuestionCriteria implements Serializable {
    /**
     * Class for filtering QuestionType
     */
    public static class QuestionTypeFilter extends Filter<QuestionType> {
    }

    /**
     * Class for filtering Level
     */
    public static class LevelFilter extends Filter<Level> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter uuid;

    private QuestionTypeFilter type;

    private LevelFilter level;

    private IntegerFilter indexOrder;

    private LongFilter lessonId;

    public QuestionCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getUuid() {
        return uuid;
    }

    public void setUuid(StringFilter uuid) {
        this.uuid = uuid;
    }

    public QuestionTypeFilter getType() {
        return type;
    }

    public void setType(QuestionTypeFilter type) {
        this.type = type;
    }

    public LevelFilter getLevel() {
        return level;
    }

    public void setLevel(LevelFilter level) {
        this.level = level;
    }

    public IntegerFilter getIndexOrder() {
        return indexOrder;
    }

    public void setIndexOrder(IntegerFilter indexOrder) {
        this.indexOrder = indexOrder;
    }

    public LongFilter getLessonId() {
        return lessonId;
    }

    public void setLessonId(LongFilter lessonId) {
        this.lessonId = lessonId;
    }

    @Override
    public String toString() {
        return "QuestionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (uuid != null ? "uuid=" + uuid + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (level != null ? "level=" + level + ", " : "") +
                (indexOrder != null ? "indexOrder=" + indexOrder + ", " : "") +
                (lessonId != null ? "lessonId=" + lessonId + ", " : "") +
            "}";
    }

}
