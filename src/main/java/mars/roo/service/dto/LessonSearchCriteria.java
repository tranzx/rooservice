package mars.roo.service.dto;

import java.io.Serializable;

import mars.roo.domain.enumeration.Language;
import mars.roo.domain.enumeration.Level;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the LessonSearch entity. This class is used in LessonSearchResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /lesson-searches?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LessonSearchCriteria implements Serializable {
    /**
     * Class for filtering Level
     */
    public static class LevelFilter extends Filter<Level> {
    }

    public static class LanguageFilter extends Filter<Language> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter categoryUuid;

    private IntegerFilter categoryIndexOrder;

    private StringFilter categoryTitleEn;

    private StringFilter categoryTitleFa;

    private StringFilter categoryTitleDe;

    private StringFilter lessonUuid;

    private IntegerFilter lessonIndexOrder;

    private LevelFilter lessonDifficLevel;

    private LanguageFilter language;

    private StringFilter text;

    public LessonSearchCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCategoryUuid() {
        return categoryUuid;
    }

    public void setCategoryUuid(StringFilter categoryUuid) {
        this.categoryUuid = categoryUuid;
    }

    public IntegerFilter getCategoryIndexOrder() {
        return categoryIndexOrder;
    }

    public void setCategoryIndexOrder(IntegerFilter categoryIndexOrder) {
        this.categoryIndexOrder = categoryIndexOrder;
    }

    public StringFilter getCategoryTitleEn() {
        return categoryTitleEn;
    }

    public void setCategoryTitleEn(StringFilter categoryTitleEn) {
        this.categoryTitleEn = categoryTitleEn;
    }

    public StringFilter getCategoryTitleFa() {
        return categoryTitleFa;
    }

    public void setCategoryTitleFa(StringFilter categoryTitleFa) {
        this.categoryTitleFa = categoryTitleFa;
    }

    public StringFilter getCategoryTitleDe() {
        return categoryTitleDe;
    }

    public void setCategoryTitleDe(StringFilter categoryTitleDe) {
        this.categoryTitleDe = categoryTitleDe;
    }

    public StringFilter getLessonUuid() {
        return lessonUuid;
    }

    public void setLessonUuid(StringFilter lessonUuid) {
        this.lessonUuid = lessonUuid;
    }

    public IntegerFilter getLessonIndexOrder() {
        return lessonIndexOrder;
    }

    public void setLessonIndexOrder(IntegerFilter lessonIndexOrder) {
        this.lessonIndexOrder = lessonIndexOrder;
    }

    public LevelFilter getLessonDifficLevel() {
        return lessonDifficLevel;
    }

    public void setLessonDifficLevel(LevelFilter lessonDifficLevel) {
        this.lessonDifficLevel = lessonDifficLevel;
    }

    public LanguageFilter getLanguage() {
        return language;
    }

    public void setLanguage(LanguageFilter language) {
        this.language = language;
    }

    public StringFilter getText() {
        return text;
    }

    public void setText(StringFilter text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "LessonSearchCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (categoryUuid != null ? "categoryUuid=" + categoryUuid + ", " : "") +
                (categoryIndexOrder != null ? "categoryIndexOrder=" + categoryIndexOrder + ", " : "") +
                (categoryTitleEn != null ? "categoryTitleEn=" + categoryTitleEn + ", " : "") +
                (categoryTitleFa != null ? "categoryTitleFa=" + categoryTitleFa + ", " : "") +
                (categoryTitleDe != null ? "categoryTitleDe=" + categoryTitleDe + ", " : "") +
                (lessonUuid != null ? "lessonUuid=" + lessonUuid + ", " : "") +
                (lessonIndexOrder != null ? "lessonIndexOrder=" + lessonIndexOrder + ", " : "") +
                (lessonDifficLevel != null ? "lessonDifficLevel=" + lessonDifficLevel + ", " : "") +
                (language != null ? "language=" + language + ", " : "") +
                (text != null ? "text=" + text + ", " : "") +
            "}";
    }

}
