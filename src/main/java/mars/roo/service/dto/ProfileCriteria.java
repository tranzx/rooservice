package mars.roo.service.dto;

import java.io.Serializable;
import mars.roo.domain.enumeration.LearnDir;
import mars.roo.domain.enumeration.Level;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Profile entity. This class is used in ProfileResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /profiles?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProfileCriteria implements Serializable {
    /**
     * Class for filtering LearnDir
     */
    public static class LearnDirFilter extends Filter<LearnDir> {
    }

    /**
     * Class for filtering Level
     */
    public static class LevelFilter extends Filter<Level> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter owner;

    private LearnDirFilter learning;

    private LevelFilter difficultyLevel;

    private StringFilter language;

    private StringFilter dname;

    private BooleanFilter soundEffects;

    private BooleanFilter autoPlayVoice;

    private BooleanFilter autoContinue;

    private IntegerFilter voiceSpeedRate;

    public ProfileCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getOwner() {
        return owner;
    }

    public void setOwner(StringFilter owner) {
        this.owner = owner;
    }

    public LearnDirFilter getLearning() {
        return learning;
    }

    public void setLearning(LearnDirFilter learning) {
        this.learning = learning;
    }

    public LevelFilter getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(LevelFilter difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public StringFilter getLanguage() {
        return language;
    }

    public void setLanguage(StringFilter language) {
        this.language = language;
    }

    public StringFilter getDname() {
        return dname;
    }

    public void setDname(StringFilter dname) {
        this.dname = dname;
    }

    public BooleanFilter getSoundEffects() {
        return soundEffects;
    }

    public void setSoundEffects(BooleanFilter soundEffects) {
        this.soundEffects = soundEffects;
    }

    public BooleanFilter getAutoPlayVoice() {
        return autoPlayVoice;
    }

    public void setAutoPlayVoice(BooleanFilter autoPlayVoice) {
        this.autoPlayVoice = autoPlayVoice;
    }

    public BooleanFilter getAutoContinue() {
        return autoContinue;
    }

    public void setAutoContinue(BooleanFilter autoContinue) {
        this.autoContinue = autoContinue;
    }

    public IntegerFilter getVoiceSpeedRate() {
        return voiceSpeedRate;
    }

    public void setVoiceSpeedRate(IntegerFilter voiceSpeedRate) {
        this.voiceSpeedRate = voiceSpeedRate;
    }

    @Override
    public String toString() {
        return "ProfileCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (owner != null ? "owner=" + owner + ", " : "") +
                (learning != null ? "learning=" + learning + ", " : "") +
                (difficultyLevel != null ? "difficultyLevel=" + difficultyLevel + ", " : "") +
                (language != null ? "language=" + language + ", " : "") +
                (dname != null ? "dname=" + dname + ", " : "") +
                (soundEffects != null ? "soundEffects=" + soundEffects + ", " : "") +
                (autoPlayVoice != null ? "autoPlayVoice=" + autoPlayVoice + ", " : "") +
                (autoContinue != null ? "autoContinue=" + autoContinue + ", " : "") +
                (voiceSpeedRate != null ? "voiceSpeedRate=" + voiceSpeedRate + ", " : "") +
            "}";
    }

}
