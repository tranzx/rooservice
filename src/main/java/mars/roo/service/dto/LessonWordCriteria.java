package mars.roo.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the LessonWord entity. This class is used in LessonWordResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /lesson-words?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LessonWordCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private IntegerFilter indexOrder;

    private BooleanFilter background;

    private BooleanFilter capitalization;

    private LongFilter wordId;

    private LongFilter lessonId;

    public LessonWordCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getIndexOrder() {
        return indexOrder;
    }

    public void setIndexOrder(IntegerFilter indexOrder) {
        this.indexOrder = indexOrder;
    }

    public BooleanFilter getBackground() {
        return background;
    }

    public void setBackground(BooleanFilter background) {
        this.background = background;
    }

    public BooleanFilter getCapitalization() {
        return capitalization;
    }

    public void setCapitalization(BooleanFilter capitalization) {
        this.capitalization = capitalization;
    }

    public LongFilter getWordId() {
        return wordId;
    }

    public void setWordId(LongFilter wordId) {
        this.wordId = wordId;
    }

    public LongFilter getLessonId() {
        return lessonId;
    }

    public void setLessonId(LongFilter lessonId) {
        this.lessonId = lessonId;
    }

    @Override
    public String toString() {
        return "LessonWordCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (indexOrder != null ? "indexOrder=" + indexOrder + ", " : "") +
                (background != null ? "background=" + background + ", " : "") +
                (capitalization != null ? "capitalization=" + capitalization + ", " : "") +
                (wordId != null ? "wordId=" + wordId + ", " : "") +
                (lessonId != null ? "lessonId=" + lessonId + ", " : "") +
            "}";
    }

}
