package mars.roo.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Membership entity. This class is used in MembershipResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /memberships?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MembershipCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter owner;

    private LocalDateFilter startDate;

    private LocalDateFilter endDate;

    private ZonedDateTimeFilter creationTime;

    private LongFilter zarinpalEventId;

    private LongFilter cafebazaarEventId;

    public MembershipCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getOwner() {
        return owner;
    }

    public void setOwner(StringFilter owner) {
        this.owner = owner;
    }

    public LocalDateFilter getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateFilter startDate) {
        this.startDate = startDate;
    }

    public LocalDateFilter getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateFilter endDate) {
        this.endDate = endDate;
    }

    public ZonedDateTimeFilter getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(ZonedDateTimeFilter creationTime) {
        this.creationTime = creationTime;
    }

    public LongFilter getZarinpalEventId() {
        return zarinpalEventId;
    }

    public void setZarinpalEventId(LongFilter zarinpalEventId) {
        this.zarinpalEventId = zarinpalEventId;
    }

    public LongFilter getCafebazaarEventId() {
        return cafebazaarEventId;
    }

    public void setCafebazaarEventId(LongFilter cafebazaarEventId) {
        this.cafebazaarEventId = cafebazaarEventId;
    }

    @Override
    public String toString() {
        return "MembershipCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (owner != null ? "owner=" + owner + ", " : "") +
                (startDate != null ? "startDate=" + startDate + ", " : "") +
                (endDate != null ? "endDate=" + endDate + ", " : "") +
                (creationTime != null ? "creationTime=" + creationTime + ", " : "") +
                (zarinpalEventId != null ? "zarinpalEventId=" + zarinpalEventId + ", " : "") +
                (cafebazaarEventId != null ? "cafebazaarEventId=" + cafebazaarEventId + ", " : "") +
            "}";
    }

}
