package mars.roo.service.dto;

import java.io.Serializable;
import mars.roo.domain.enumeration.WordType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Word entity. This class is used in WordResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /words?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WordCriteria implements Serializable {
    /**
     * Class for filtering WordType
     */
    public static class WordTypeFilter extends Filter<WordType> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private WordTypeFilter wordType;

    private StringFilter enGb;

    private StringFilter faIr;

    private StringFilter deDe;

    public WordCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public WordTypeFilter getWordType() {
        return wordType;
    }

    public void setWordType(WordTypeFilter wordType) {
        this.wordType = wordType;
    }

    public StringFilter getEnGb() {
        return enGb;
    }

    public void setEnGb(StringFilter enGb) {
        this.enGb = enGb;
    }

    public StringFilter getFaIr() {
        return faIr;
    }

    public void setFaIr(StringFilter faIr) {
        this.faIr = faIr;
    }

    public StringFilter getDeDe() {
        return deDe;
    }

    public void setDeDe(StringFilter deDe) {
        this.deDe = deDe;
    }

    @Override
    public String toString() {
        return "WordCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (wordType != null ? "wordType=" + wordType + ", " : "") +
                (enGb != null ? "enGb=" + enGb + ", " : "") +
                (faIr != null ? "faIr=" + faIr + ", " : "") +
                (deDe != null ? "deDe=" + deDe + ", " : "") +
            "}";
    }

}
