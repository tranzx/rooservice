package mars.roo.service.dto;

import java.io.Serializable;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import mars.roo.domain.enumeration.LearnDir;
import mars.roo.domain.enumeration.Level;
import mars.roo.domain.enumeration.ScoreType;



/**
 * Criteria class for the Score entity. This class is used in ScoreResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /scores?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ScoreCriteria implements Serializable {
    /**
     * Class for filtering LearnDir
     */
    public static class LearnDirFilter extends Filter<LearnDir> {
    }

    /**
     * Class for filtering ScoreType
     */
    public static class ScoreTypeFilter extends Filter<ScoreType> {
    }

    /**
     * Class for filtering Level
     */
    public static class LevelFilter extends Filter<Level> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter owner;

    private LearnDirFilter learnDir;

    private IntegerFilter score;

    private IntegerFilter star;

    private LocalDateFilter scoreDate;

    private ScoreTypeFilter type;

    private LevelFilter difficultyLevel;

    private StringFilter lessonUuid;

    private StringFilter categoryUuid;

    public ScoreCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getOwner() {
        return owner;
    }

    public void setOwner(StringFilter owner) {
        this.owner = owner;
    }

    public LearnDirFilter getLearnDir() {
        return learnDir;
    }

    public void setLearnDir(LearnDirFilter learnDir) {
        this.learnDir = learnDir;
    }

    public IntegerFilter getScore() {
        return score;
    }

    public void setScore(IntegerFilter score) {
        this.score = score;
    }

    public IntegerFilter getStar() {
        return star;
    }

    public void setStar(IntegerFilter star) {
        this.star = star;
    }

    public LocalDateFilter getScoreDate() {
        return scoreDate;
    }

    public void setScoreDate(LocalDateFilter scoreDate) {
        this.scoreDate = scoreDate;
    }

    public ScoreTypeFilter getType() {
        return type;
    }

    public void setType(ScoreTypeFilter type) {
        this.type = type;
    }

    public LevelFilter getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(LevelFilter difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public StringFilter getLessonUuid() {
        return lessonUuid;
    }

    public void setLessonUuid(StringFilter lessonUuid) {
        this.lessonUuid = lessonUuid;
    }

    public StringFilter getCategoryUuid() {
        return categoryUuid;
    }

    public void setCategoryUuid(StringFilter categoryUuid) {
        this.categoryUuid = categoryUuid;
    }

    @Override
    public String toString() {
        return "ScoreCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (owner != null ? "owner=" + owner + ", " : "") +
                (learnDir != null ? "learnDir=" + learnDir + ", " : "") +
                (score != null ? "score=" + score + ", " : "") +
                (star != null ? "star=" + star + ", " : "") +
                (scoreDate != null ? "scoreDate=" + scoreDate + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (difficultyLevel != null ? "difficultyLevel=" + difficultyLevel + ", " : "") +
                (lessonUuid != null ? "lessonUuid=" + lessonUuid + ", " : "") +
                (categoryUuid != null ? "categoryUuid=" + categoryUuid + ", " : "") +
            "}";
    }

}
