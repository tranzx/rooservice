package mars.roo.service.dto;

import java.io.Serializable;

import mars.jzarinpal.domain.ZarinStatus;
import mars.roo.domain.enumeration.RooCurrency;
import mars.roo.domain.enumeration.SubscriptionType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the ZarinpalEvent entity. This class is used in ZarinpalEventResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /zarinpal-events?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ZarinpalEventCriteria implements Serializable {
    /**
     * Class for filtering RooCurrency
     */
    public static class RooCurrencyFilter extends Filter<RooCurrency> {
    }

    /**
     * Class for filtering ZarinStatus
     */
    public static class ZarinStatusFilter extends Filter<ZarinStatus> {
    }

    /**
     * Class for filtering SubscriptionType
     */
    public static class SubscriptionTypeFilter extends Filter<SubscriptionType> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter paymentToken;

    private IntegerFilter amount;

    private RooCurrencyFilter currency;

    private StringFilter description;

    private StringFilter email;

    private StringFilter mobile;

    private ZarinStatusFilter zarinStatus;

    private LongFilter refId;

    private ZonedDateTimeFilter creationTime;

    private StringFilter owner;

    private SubscriptionTypeFilter subscriptionType;

    public ZarinpalEventCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPaymentToken() {
        return paymentToken;
    }

    public void setPaymentToken(StringFilter paymentToken) {
        this.paymentToken = paymentToken;
    }

    public IntegerFilter getAmount() {
        return amount;
    }

    public void setAmount(IntegerFilter amount) {
        this.amount = amount;
    }

    public RooCurrencyFilter getCurrency() {
        return currency;
    }

    public void setCurrency(RooCurrencyFilter currency) {
        this.currency = currency;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getMobile() {
        return mobile;
    }

    public void setMobile(StringFilter mobile) {
        this.mobile = mobile;
    }

    public ZarinStatusFilter getZarinStatus() {
        return zarinStatus;
    }

    public void setZarinStatus(ZarinStatusFilter zarinStatus) {
        this.zarinStatus = zarinStatus;
    }

    public LongFilter getRefId() {
        return refId;
    }

    public void setRefId(LongFilter refId) {
        this.refId = refId;
    }

    public ZonedDateTimeFilter getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(ZonedDateTimeFilter creationTime) {
        this.creationTime = creationTime;
    }

    public StringFilter getOwner() {
        return owner;
    }

    public void setOwner(StringFilter owner) {
        this.owner = owner;
    }

    public SubscriptionTypeFilter getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(SubscriptionTypeFilter subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    @Override
    public String toString() {
        return "ZarinpalEventCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (paymentToken != null ? "paymentToken=" + paymentToken + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (currency != null ? "currency=" + currency + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (mobile != null ? "mobile=" + mobile + ", " : "") +
                (zarinStatus != null ? "zarinStatus=" + zarinStatus + ", " : "") +
                (refId != null ? "refId=" + refId + ", " : "") +
                (creationTime != null ? "creationTime=" + creationTime + ", " : "") +
                (owner != null ? "owner=" + owner + ", " : "") +
                (subscriptionType != null ? "subscriptionType=" + subscriptionType + ", " : "") +
            "}";
    }

}
