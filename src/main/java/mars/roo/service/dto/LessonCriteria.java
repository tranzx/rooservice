package mars.roo.service.dto;

import java.io.Serializable;
import mars.roo.domain.enumeration.Level;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Lesson entity. This class is used in LessonResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /lessons?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LessonCriteria implements Serializable {
    /**
     * Class for filtering Level
     */
    public static class LevelFilter extends Filter<Level> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter uuid;

    private StringFilter titleEng;

    private IntegerFilter indexOrder;

    private StringFilter pictureName;

    private LevelFilter level;

    private LongFilter categoryId;

    public LessonCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getUuid() {
        return uuid;
    }

    public void setUuid(StringFilter uuid) {
        this.uuid = uuid;
    }

    public StringFilter getTitleEng() {
        return titleEng;
    }

    public void setTitleEng(StringFilter titleEng) {
        this.titleEng = titleEng;
    }

    public IntegerFilter getIndexOrder() {
        return indexOrder;
    }

    public void setIndexOrder(IntegerFilter indexOrder) {
        this.indexOrder = indexOrder;
    }

    public StringFilter getPictureName() {
        return pictureName;
    }

    public void setPictureName(StringFilter pictureName) {
        this.pictureName = pictureName;
    }

    public LevelFilter getLevel() {
        return level;
    }

    public void setLevel(LevelFilter level) {
        this.level = level;
    }

    public LongFilter getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(LongFilter categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "LessonCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (uuid != null ? "uuid=" + uuid + ", " : "") +
                (titleEng != null ? "titleEng=" + titleEng + ", " : "") +
                (indexOrder != null ? "indexOrder=" + indexOrder + ", " : "") +
                (pictureName != null ? "pictureName=" + pictureName + ", " : "") +
                (level != null ? "level=" + level + ", " : "") +
                (categoryId != null ? "categoryId=" + categoryId + ", " : "") +
            "}";
    }

}
