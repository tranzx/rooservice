package mars.roo.service;

import mars.roo.domain.Profile;
import mars.roo.repository.ProfileRepository;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Profile.
 */
@Service
@Transactional
public class ProfileService {

    private final Logger log = LoggerFactory.getLogger(ProfileService.class);

    private final ProfileRepository profileRepository;

    public static final Map<String, String> dnameIdentityMap = new HashMap<String, String>();

    public ProfileService(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    /**
     * Save a profile.
     *
     * @param profile the entity to save
     * @return the persisted entity
     */
    public Profile save(Profile profile) {
        log.debug("Request to save Profile : {}", profile);
        Profile saved = profileRepository.save(profile);
        dnameIdentityMap.put(saved.getOwner(), saved.getDname());
        return saved;
    }

    /**
     * Get all the profiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Profile> findAll(Pageable pageable) {
        log.debug("Request to get all Profiles");
        return profileRepository.findAll(pageable);
    }

    /**
     * Get one profile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Profile findOne(Long id) {
        log.debug("Request to get Profile : {}", id);
        Profile profile = profileRepository.findOne(id);
        dnameIdentityMap.put(profile.getOwner(), profile.getDname());
        return profile;
    }

    @Transactional(readOnly = true)
    public Profile findOneByOwner(String owner) {
        log.debug("Request to get Profile : {}", owner);
        Profile profile = profileRepository.findOneByOwner(owner);
        if (profile != null) {
            dnameIdentityMap.put(profile.getOwner(), profile.getDname());
        }
        return profile;
    }


    /**
     * Delete the profile by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Profile : {}", id);
        profileRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public String getDname(String username) {
        if (dnameIdentityMap.containsKey(username)) {
            return dnameIdentityMap.get(username);
        }
        Profile p = findOneByOwner(username);
        return p != null ? p.getDname() : "Guest_";
    }

}
