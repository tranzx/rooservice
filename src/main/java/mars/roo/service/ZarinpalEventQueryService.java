package mars.roo.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
// for static metamodels
import mars.roo.domain.ZarinpalEvent;
import mars.roo.domain.ZarinpalEvent_;
import mars.roo.repository.ZarinpalEventRepository;
import mars.roo.service.dto.ZarinpalEventCriteria;

/**
 * Service for executing complex queries for ZarinpalEvent entities in the database.
 * The main input is a {@link ZarinpalEventCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ZarinpalEvent} or a {@link Page} of {@link ZarinpalEvent} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ZarinpalEventQueryService extends QueryService<ZarinpalEvent> {

    private final Logger log = LoggerFactory.getLogger(ZarinpalEventQueryService.class);


    private final ZarinpalEventRepository zarinpalEventRepository;

    public ZarinpalEventQueryService(ZarinpalEventRepository zarinpalEventRepository) {
        this.zarinpalEventRepository = zarinpalEventRepository;
    }

    /**
     * Return a {@link List} of {@link ZarinpalEvent} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ZarinpalEvent> findByCriteria(ZarinpalEventCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ZarinpalEvent> specification = createSpecification(criteria);
        return zarinpalEventRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ZarinpalEvent} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ZarinpalEvent> findByCriteria(ZarinpalEventCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ZarinpalEvent> specification = createSpecification(criteria);
        return zarinpalEventRepository.findAll(specification, page);
    }

    /**
     * Function to convert ZarinpalEventCriteria to a {@link Specifications}
     */
    private Specifications<ZarinpalEvent> createSpecification(ZarinpalEventCriteria criteria) {
        Specifications<ZarinpalEvent> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ZarinpalEvent_.id));
            }
            if (criteria.getPaymentToken() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPaymentToken(), ZarinpalEvent_.paymentToken));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), ZarinpalEvent_.amount));
            }
            if (criteria.getCurrency() != null) {
                specification = specification.and(buildSpecification(criteria.getCurrency(), ZarinpalEvent_.currency));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), ZarinpalEvent_.description));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), ZarinpalEvent_.email));
            }
            if (criteria.getMobile() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobile(), ZarinpalEvent_.mobile));
            }
            if (criteria.getZarinStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getZarinStatus(), ZarinpalEvent_.zarinStatus));
            }
            if (criteria.getRefId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRefId(), ZarinpalEvent_.refId));
            }
            if (criteria.getCreationTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationTime(), ZarinpalEvent_.creationTime));
            }
            if (criteria.getOwner() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOwner(), ZarinpalEvent_.owner));
            }
            if (criteria.getSubscriptionType() != null) {
                specification = specification.and(buildSpecification(criteria.getSubscriptionType(), ZarinpalEvent_.subscriptionType));
            }
        }
        return specification;
    }

}
