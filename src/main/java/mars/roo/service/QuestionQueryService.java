package mars.roo.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
import mars.roo.domain.Lesson_;
import mars.roo.domain.Question;
import mars.roo.domain.Question_;
import mars.roo.repository.QuestionRepository;
import mars.roo.service.dto.QuestionCriteria;

/**
 * Service for executing complex queries for Question entities in the database.
 * The main input is a {@link QuestionCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Question} or a {@link Page} of {@link Question} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class QuestionQueryService extends QueryService<Question> {

    private final Logger log = LoggerFactory.getLogger(QuestionQueryService.class);


    private final QuestionRepository questionRepository;

    public QuestionQueryService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    /**
     * Return a {@link List} of {@link Question} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Question> findByCriteria(QuestionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Question> specification = createSpecification(criteria);
        return questionRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Question} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Question> findByCriteria(QuestionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Question> specification = createSpecification(criteria);
        return questionRepository.findAll(specification, page);
    }

    @Transactional(readOnly = true)
    public long countByCriteria(QuestionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Question> specification = createSpecification(criteria);
        return questionRepository.count(specification);
    }


    /**
     * Function to convert QuestionCriteria to a {@link Specifications}
     */
    private Specifications<Question> createSpecification(QuestionCriteria criteria) {
        Specifications<Question> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Question_.id));
            }
            if (criteria.getUuid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUuid(), Question_.uuid));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildSpecification(criteria.getType(), Question_.type));
            }
            if (criteria.getLevel() != null) {
                specification = specification.and(buildSpecification(criteria.getLevel(), Question_.level));
            }
            if (criteria.getIndexOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIndexOrder(), Question_.indexOrder));
            }
            if (criteria.getLessonId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getLessonId(), Question_.lesson, Lesson_.id));
            }
        }
        return specification;
    }

}
