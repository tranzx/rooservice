package mars.roo.service;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.StringFilter;
// for static metamodels
import mars.roo.domain.Membership;
import mars.roo.domain.Membership_;
import mars.roo.domain.ZarinpalEvent_;
import mars.roo.domain.CafebazaarEvent_;
import mars.roo.repository.MembershipRepository;
import mars.roo.service.dto.MembershipCriteria;


/**
 * Service for executing complex queries for Membership entities in the database.
 * The main input is a {@link MembershipCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Membership} or a {@link Page} of {@link Membership} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MembershipQueryService extends QueryService<Membership> {

    private final Logger log = LoggerFactory.getLogger(MembershipQueryService.class);


    private final MembershipRepository membershipRepository;

    public MembershipQueryService(MembershipRepository membershipRepository) {
        this.membershipRepository = membershipRepository;
    }

    /**
     * Check if given user is a member of premium account.
     * @param login the user identity
     * @return 'true' if use is member
     */
    public boolean isMember(String login) {
        if (membershipCache.asMap().containsKey(login)) {
            return true;
        }
        else {
            if (isMemberQuery(login)) {
                membershipCache.put(login, login);
                return true;
            }
        }
        return false;
    }

    /**
     * Return a {@link List} of {@link Membership} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Membership> findByCriteria(MembershipCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Membership> specification = createSpecification(criteria);
        return membershipRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Membership} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Membership> findByCriteria(MembershipCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Membership> specification = createSpecification(criteria);
        return membershipRepository.findAll(specification, page);
    }

    /**
     * Function to convert MembershipCriteria to a {@link Specifications}
     */
    private Specifications<Membership> createSpecification(MembershipCriteria criteria) {
        Specifications<Membership> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Membership_.id));
            }
            if (criteria.getOwner() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOwner(), Membership_.owner));
            }
            if (criteria.getStartDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartDate(), Membership_.startDate));
            }
            if (criteria.getEndDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndDate(), Membership_.endDate));
            }
            if (criteria.getCreationTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationTime(), Membership_.creationTime));
            }
            if (criteria.getZarinpalEventId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getZarinpalEventId(), Membership_.zarinpalEvent, ZarinpalEvent_.id));
            }
            if (criteria.getCafebazaarEventId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCafebazaarEventId(), Membership_.cafebazaarEvent, CafebazaarEvent_.id));
            }

        }
        return specification;
    }

    private boolean isMemberQuery(String login) {
        MembershipCriteria criteria = new MembershipCriteria();
        StringFilter ownerFilter = new StringFilter();
        ownerFilter.setEquals(login);
        LocalDateFilter endDateFilter = new LocalDateFilter();
        endDateFilter.setGreaterOrEqualThan(LocalDate.now(ZoneOffset.UTC));
        criteria.setOwner(ownerFilter);
        criteria.setEndDate(endDateFilter);
        List<Membership> list = findByCriteria(criteria);
        return list != null && list.size() > 0;
    }

    private final Cache<String, String> membershipCache = CacheBuilder.newBuilder()
            .maximumSize(Integer.MAX_VALUE)
            .expireAfterWrite(6, TimeUnit.HOURS)
            .build();

}
