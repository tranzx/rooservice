package mars.roo.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mars.roo.domain.Word;
import mars.roo.domain.*; // for static metamodels
import mars.roo.repository.WordRepository;
import mars.roo.service.dto.WordCriteria;

import mars.roo.domain.enumeration.WordType;

/**
 * Service for executing complex queries for Word entities in the database.
 * The main input is a {@link WordCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Word} or a {@link Page} of {@link Word} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WordQueryService extends QueryService<Word> {

    private final Logger log = LoggerFactory.getLogger(WordQueryService.class);


    private final WordRepository wordRepository;

    public WordQueryService(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    /**
     * Return a {@link List} of {@link Word} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Word> findByCriteria(WordCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Word> specification = createSpecification(criteria);
        return wordRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Word} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Word> findByCriteria(WordCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Word> specification = createSpecification(criteria);
        return wordRepository.findAll(specification, page);
    }

    /**
     * Function to convert WordCriteria to a {@link Specifications}
     */
    private Specifications<Word> createSpecification(WordCriteria criteria) {
        Specifications<Word> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Word_.id));
            }
            if (criteria.getWordType() != null) {
                specification = specification.and(buildSpecification(criteria.getWordType(), Word_.wordType));
            }
            if (criteria.getEnGb() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEnGb(), Word_.enGb));
            }
            if (criteria.getFaIr() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFaIr(), Word_.faIr));
            }
            if (criteria.getDeDe() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDeDe(), Word_.deDe));
            }
        }
        return specification;
    }

}
