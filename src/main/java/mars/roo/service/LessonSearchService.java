package mars.roo.service;

import mars.roo.domain.LessonSearch;
import mars.roo.repository.LessonSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing LessonSearch.
 */
@Service
@Transactional
public class LessonSearchService {

    private final Logger log = LoggerFactory.getLogger(LessonSearchService.class);

    private final LessonSearchRepository lessonSearchRepository;

    public LessonSearchService(LessonSearchRepository lessonSearchRepository) {
        this.lessonSearchRepository = lessonSearchRepository;
    }

    /**
     * Save a lessonSearch.
     *
     * @param lessonSearch the entity to save
     * @return the persisted entity
     */
    public LessonSearch save(LessonSearch lessonSearch) {
        log.debug("Request to save LessonSearch : {}", lessonSearch);
        return lessonSearchRepository.save(lessonSearch);
    }

    /**
     * Get all the lessonSearches.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LessonSearch> findAll(Pageable pageable) {
        log.debug("Request to get all LessonSearches");
        return lessonSearchRepository.findAll(pageable);
    }

    /**
     * Get one lessonSearch by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public LessonSearch findOne(Long id) {
        log.debug("Request to get LessonSearch : {}", id);
        return lessonSearchRepository.findOne(id);
    }

    /**
     * Delete the lessonSearch by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete LessonSearch : {}", id);
        lessonSearchRepository.delete(id);
    }
}
