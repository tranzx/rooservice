package mars.roo.service;

import mars.roo.domain.Lesson;
import mars.roo.domain.Question;
import mars.roo.domain.enumeration.Level;
import mars.roo.repository.QuestionRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Question.
 */
@Service
@Transactional
public class QuestionService {

    private final Logger log = LoggerFactory.getLogger(QuestionService.class);

    private final QuestionRepository questionRepository;

    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    /**
     * Save a question.
     *
     * @param question the entity to save
     * @return the persisted entity
     */
    public Question save(Question question) {
        log.debug("Request to save Question : {}", question);
        return questionRepository.save(question);
    }

    public List<Question> save(List<Question> questions) {
        return this.questionRepository.save(questions);
    }

    /**
     * Get all the questions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Question> findAll(Pageable pageable) {
        log.debug("Request to get all Questions");
        return questionRepository.findAll(pageable);
    }

    /**
     * Get one question by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Question findOne(Long id) {
        log.debug("Request to get Question : {}", id);
        return questionRepository.findOne(id);
    }

    /**
     * Delete the question by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Question : {}", id);
        questionRepository.delete(id);
    }

    public List<Question> findByLessonAndLevelOrderByIndexOrderAsc(Lesson lesson, Level level) {
        return questionRepository.findByLessonAndLevelOrderByIndexOrderAsc(lesson, level);
    }

    public List<Question> findByLesson(Lesson lesson) {
        return questionRepository.findByLesson(lesson);
    }

    public void deleteByLesson(Lesson lesson) {
        questionRepository.deleteByLesson(lesson);
    }

}
