package mars.roo.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mars.roo.domain.LessonSearch;
import mars.roo.domain.*; // for static metamodels
import mars.roo.repository.LessonSearchRepository;
import mars.roo.service.dto.LessonSearchCriteria;
import mars.roo.domain.enumeration.Language;
import mars.roo.domain.enumeration.Level;

/**
 * Service for executing complex queries for LessonSearch entities in the database.
 * The main input is a {@link LessonSearchCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LessonSearch} or a {@link Page} of {@link LessonSearch} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LessonSearchQueryService extends QueryService<LessonSearch> {

    private final Logger log = LoggerFactory.getLogger(LessonSearchQueryService.class);


    private final LessonSearchRepository lessonSearchRepository;

    public LessonSearchQueryService(LessonSearchRepository lessonSearchRepository) {
        this.lessonSearchRepository = lessonSearchRepository;
    }

    /**
     * Return a {@link List} of {@link LessonSearch} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LessonSearch> findByCriteria(LessonSearchCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<LessonSearch> specification = createSpecification(criteria);
        return lessonSearchRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link LessonSearch} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LessonSearch> findByCriteria(LessonSearchCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<LessonSearch> specification = createSpecification(criteria);
        return lessonSearchRepository.findAll(specification, page);
    }

    /**
     * Function to convert LessonSearchCriteria to a {@link Specifications}
     */
    private Specifications<LessonSearch> createSpecification(LessonSearchCriteria criteria) {
        Specifications<LessonSearch> specification = Specifications.where(null);
        if (criteria != null) {
//            if (criteria.getId() != null) {
//                specification = specification.and(buildSpecification(criteria.getId(), LessonSearch_.id));
//            }
            if (criteria.getCategoryUuid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategoryUuid(), LessonSearch_.categoryUuid));
            }
            if (criteria.getCategoryIndexOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCategoryIndexOrder(), LessonSearch_.categoryIndexOrder));
            }
            if (criteria.getCategoryTitleEn() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategoryTitleEn(), LessonSearch_.categoryTitleEn));
            }
            if (criteria.getCategoryTitleFa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategoryTitleFa(), LessonSearch_.categoryTitleFa));
            }
            if (criteria.getCategoryTitleDe() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategoryTitleDe(), LessonSearch_.categoryTitleDe));
            }
            if (criteria.getLessonUuid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLessonUuid(), LessonSearch_.lessonUuid));
            }
            if (criteria.getLessonIndexOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLessonIndexOrder(), LessonSearch_.lessonIndexOrder));
            }
            if (criteria.getLessonDifficLevel() != null) {
                specification = specification.and(buildSpecification(criteria.getLessonDifficLevel(), LessonSearch_.lessonDifficLevel));
            }
            if (criteria.getLanguage() != null) {
                Specifications<LessonSearch> specs = Specifications.where(null);
                specification = specification.and(specs.and(buildSpecification(criteria.getLanguage(), LessonSearch_.language)));
            }
            if (criteria.getText() != null) {
                Specifications<LessonSearch> specs = Specifications.where(null);
                specification = specification.and(specs.and(buildStringSpecification(criteria.getText(), LessonSearch_.text)));
//                if (criteria.getLanguage().getEquals().equals(Language.EN_GB)) {
//                    specification = specification.or(buildStringSpecification(criteria.getText(), LessonSearch_.categoryTitleEn));
//                } else if (criteria.getLanguage().getEquals().equals(Language.FA_IR)) {
//                    specification = specification.or(buildStringSpecification(criteria.getText(), LessonSearch_.categoryTitleFa));
//                } else if (criteria.getLanguage().getEquals().equals(Language.DE_DE)) {
//                    specification = specification.or(buildStringSpecification(criteria.getText(), LessonSearch_.categoryTitleDe));
//                }
            }
        }
        return specification;
    }

}
