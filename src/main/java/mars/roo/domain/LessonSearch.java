package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import mars.roo.domain.enumeration.Language;
import mars.roo.domain.enumeration.Level;

/**
 * A LessonSearch.
 */
@Entity
@Table(name = "lesson_search")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LessonSearch implements Serializable {

    private static final long serialVersionUID = 1L;

//    @Id
//    private Long id;

    @Column(name = "category_uuid")
    private String categoryUuid;

    @Column(name = "category_index_order")
    private Integer categoryIndexOrder;

    @Column(name = "category_title_en")
    private String categoryTitleEn;

    @Column(name = "category_title_fa")
    private String categoryTitleFa;

    @Column(name = "category_title_de")
    private String categoryTitleDe;

    @Id
    @Column(name = "lesson_uuid")
    private String lessonUuid;

    @Column(name = "lesson_index_order")
    private Integer lessonIndexOrder;

    @Enumerated(EnumType.STRING)
    @Column(name = "lesson_diffic_level")
    private Level lessonDifficLevel;

    @Enumerated(EnumType.STRING)
    @Column(name = "language", nullable = false)
    private Language language;

    @Column(name = "text", nullable = false)
    private String text;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public LessonSearch() {
    }

    public LessonSearch(String categoryUuid, Integer categoryIndexOrder, String categoryTitleEn, String categoryTitleFa, String categoryTitleDe,
            String lessonUuid, Integer lessonIndexOrder) {
        this.categoryUuid = categoryUuid;
        this.categoryIndexOrder = categoryIndexOrder;
        this.categoryTitleEn = categoryTitleEn;
        this.categoryTitleFa = categoryTitleFa;
        this.categoryTitleDe = categoryTitleDe;
        this.lessonUuid = lessonUuid;
        this.lessonIndexOrder = lessonIndexOrder;
    }

    public String getCategoryUuid() {
        return categoryUuid;
    }

    public LessonSearch categoryUuid(String categoryUuid) {
        this.categoryUuid = categoryUuid;
        return this;
    }

    public void setCategoryUuid(String categoryUuid) {
        this.categoryUuid = categoryUuid;
    }

    public Integer getCategoryIndexOrder() {
        return categoryIndexOrder;
    }

    public LessonSearch categoryIndexOrder(Integer categoryIndexOrder) {
        this.categoryIndexOrder = categoryIndexOrder;
        return this;
    }

    public void setCategoryIndexOrder(Integer categoryIndexOrder) {
        this.categoryIndexOrder = categoryIndexOrder;
    }

    public String getCategoryTitleEn() {
        return categoryTitleEn;
    }

    public LessonSearch categoryTitleEn(String categoryTitleEn) {
        this.categoryTitleEn = categoryTitleEn;
        return this;
    }

    public void setCategoryTitleEn(String categoryTitleEn) {
        this.categoryTitleEn = categoryTitleEn;
    }

    public String getCategoryTitleFa() {
        return categoryTitleFa;
    }

    public LessonSearch categoryTitleFa(String categoryTitleFa) {
        this.categoryTitleFa = categoryTitleFa;
        return this;
    }

    public void setCategoryTitleFa(String categoryTitleFa) {
        this.categoryTitleFa = categoryTitleFa;
    }

    public String getCategoryTitleDe() {
        return categoryTitleDe;
    }

    public LessonSearch categoryTitleDe(String categoryTitleDe) {
        this.categoryTitleDe = categoryTitleDe;
        return this;
    }

    public void setCategoryTitleDe(String categoryTitleDe) {
        this.categoryTitleDe = categoryTitleDe;
    }

    public String getLessonUuid() {
        return lessonUuid;
    }

    public LessonSearch lessonUuid(String lessonUuid) {
        this.lessonUuid = lessonUuid;
        return this;
    }

    public void setLessonUuid(String lessonUuid) {
        this.lessonUuid = lessonUuid;
    }

    public Integer getLessonIndexOrder() {
        return lessonIndexOrder;
    }

    public LessonSearch lessonIndexOrder(Integer lessonIndexOrder) {
        this.lessonIndexOrder = lessonIndexOrder;
        return this;
    }

    public void setLessonIndexOrder(Integer lessonIndexOrder) {
        this.lessonIndexOrder = lessonIndexOrder;
    }

    public Level getLessonDifficLevel() {
        return lessonDifficLevel;
    }

    public LessonSearch lessonDifficLevel(Level lessonDifficLevel) {
        this.lessonDifficLevel = lessonDifficLevel;
        return this;
    }

    public void setLessonDifficLevel(Level lessonDifficLevel) {
        this.lessonDifficLevel = lessonDifficLevel;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LessonSearch lessonSearch = (LessonSearch) o;
        if (lessonSearch.getLessonUuid() == null || getLessonUuid() == null) {
            return false;
        }
        return Objects.equals(getLessonUuid(), lessonSearch.getLessonUuid());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getLessonUuid());
    }

    @Override
    public String toString() {
        return "LessonSearch{" +
//            "id=" + getId() +
            ", categoryUuid='" + getCategoryUuid() + "'" +
            ", categoryIndexOrder=" + getCategoryIndexOrder() +
            ", categoryTitleEn='" + getCategoryTitleEn() + "'" +
            ", categoryTitleFa='" + getCategoryTitleFa() + "'" +
            ", categoryTitleDe='" + getCategoryTitleDe() + "'" +
            ", lessonUuid='" + getLessonUuid() + "'" +
            ", lessonIndexOrder=" + getLessonIndexOrder() +
            ", lessonDifficLevel='" + getLessonDifficLevel() + "'" +
            ", wordEnGb='" + getLanguage() + "'" +
            ", wordFaIr='" + getText() + "'" +
            "}";
    }
}
