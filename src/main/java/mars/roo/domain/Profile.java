package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import mars.roo.domain.enumeration.LearnDir;

import mars.roo.domain.enumeration.Level;
import mars.roo.web.rest.profile.ProfileDTO;

/**
 * A Profile.
 */
@Entity
@Table(name = "profile")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "owner", nullable = false)
    private String owner;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "learning", nullable = false)
    private LearnDir learning;

    @Enumerated(EnumType.STRING)
    @Column(name = "difficulty_level")
    private Level difficultyLevel;

    @Column(name = "language")
    private String language;

    @Column(name = "dname")
    private String dname;

    @Column(name = "sound_effects")
    private Boolean soundEffects;

    @Column(name = "auto_play_voice")
    private Boolean autoPlayVoice;

    @Column(name = "auto_continue")
    private Boolean autoContinue;

    @Column(name = "voice_speed_rate")
    private Integer voiceSpeedRate;

    public static Profile defaultProfile(String owner, ProfileDTO dto) {
        Profile p = new Profile();
        p.setOwner(owner);
        p.updateWith(dto);
        return p;
    }

    public void updateWith(ProfileDTO dto) {
        setDname(dto.getDname());
        setLearning(dto.getLearnDir());
        setLanguage(dto.getLanguage());
        setDifficultyLevel(dto.getDifficultyLevel());
        setAutoContinue(dto.getAutoContinue());
        setSoundEffects(dto.getSoundEffects());
        setAutoPlayVoice(dto.getAutoPlayVoice());
        setVoiceSpeedRate(dto.getVoiceSpeedRate());
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public Profile owner(String owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public LearnDir getLearning() {
        return learning;
    }

    public Profile learning(LearnDir learning) {
        this.learning = learning;
        return this;
    }

    public void setLearning(LearnDir learning) {
        this.learning = learning;
    }

    public Level getDifficultyLevel() {
        return difficultyLevel;
    }

    public Profile difficultyLevel(Level difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
        return this;
    }

    public void setDifficultyLevel(Level difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public String getLanguage() {
        return language;
    }

    public Profile language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDname() {
        return dname;
    }

    public Profile dname(String dname) {
        this.dname = dname;
        return this;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public Boolean isSoundEffects() {
        return soundEffects;
    }

    public Profile soundEffects(Boolean soundEffects) {
        this.soundEffects = soundEffects;
        return this;
    }

    public void setSoundEffects(Boolean soundEffects) {
        this.soundEffects = soundEffects;
    }

    public Boolean isAutoPlayVoice() {
        return autoPlayVoice;
    }

    public Profile autoPlayVoice(Boolean autoPlayVoice) {
        this.autoPlayVoice = autoPlayVoice;
        return this;
    }

    public void setAutoPlayVoice(Boolean autoPlayVoice) {
        this.autoPlayVoice = autoPlayVoice;
    }

    public Boolean isAutoContinue() {
        return autoContinue;
    }

    public Profile autoContinue(Boolean autoContinue) {
        this.autoContinue = autoContinue;
        return this;
    }

    public void setAutoContinue(Boolean autoContinue) {
        this.autoContinue = autoContinue;
    }

    public Integer getVoiceSpeedRate() {
        return voiceSpeedRate;
    }

    public Profile voiceSpeedRate(Integer voiceSpeedRate) {
        this.voiceSpeedRate = voiceSpeedRate;
        return this;
    }

    public void setVoiceSpeedRate(Integer voiceSpeedRate) {
        this.voiceSpeedRate = voiceSpeedRate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Profile profile = (Profile) o;
        if (profile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), profile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Profile{" +
            "id=" + getId() +
            ", owner='" + getOwner() + "'" +
            ", learning='" + getLearning() + "'" +
            ", difficultyLevel='" + getDifficultyLevel() + "'" +
            ", language='" + getLanguage() + "'" +
            ", dname='" + getDname() + "'" +
            ", soundEffects='" + isSoundEffects() + "'" +
            ", autoPlayVoice='" + isAutoPlayVoice() + "'" +
            ", autoContinue='" + isAutoContinue() + "'" +
            ", voiceSpeedRate=" + getVoiceSpeedRate() +
            "}";
    }
}
