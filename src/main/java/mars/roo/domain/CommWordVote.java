package mars.roo.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A CommWordVote.
 * A Community Word Vote.
 */
@Entity
@Table(name = "comm_word_vote")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CommWordVote implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "comm_word_uuid", nullable = false)
    private String commWordUuid;

    @NotNull
    @Column(name = "voter", nullable = false)
    private String voter;

    @NotNull
    @Column(name = "vote", nullable = false)
    private Integer vote;

    @ManyToOne(optional = false)
    @NotNull
    private CommWord commWord;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommWordUuid() {
        return commWordUuid;
    }

    public CommWordVote commWordUuid(String commWordUuid) {
        this.commWordUuid = commWordUuid;
        return this;
    }

    public void setCommWordUuid(String commWordUuid) {
        this.commWordUuid = commWordUuid;
    }

    public String getVoter() {
        return voter;
    }

    public CommWordVote voter(String voter) {
        this.voter = voter;
        return this;
    }

    public void setVoter(String voter) {
        this.voter = voter;
    }

    public Integer getVote() {
        return vote;
    }

    public CommWordVote vote(Integer vote) {
        this.vote = vote;
        return this;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    public CommWord getCommWord() {
        return commWord;
    }

    public CommWordVote commWord(CommWord commWord) {
        this.commWord = commWord;
        return this;
    }

    public void setCommWord(CommWord commWord) {
        this.commWord = commWord;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommWordVote commWordVote = (CommWordVote) o;
        if (commWordVote.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), commWordVote.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CommWordVote{" +
            "id=" + getId() +
            ", commWordUuid='" + getCommWordUuid() + "'" +
            ", voter='" + getVoter() + "'" +
            ", vote='" + getVote() + "'" +
            "}";
    }
}
