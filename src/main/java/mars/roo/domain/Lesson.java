package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import mars.roo.domain.enumeration.Level;

/**
 * A Lesson.
 */
@Entity
@Table(name = "lesson")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Lesson implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Column(name = "title_eng")
    private String titleEng;

    @NotNull
    @Column(name = "index_order", nullable = false)
    private Integer indexOrder;

    @Column(name = "picture_name")
    private String pictureName;

    @Enumerated(EnumType.STRING)
    @Column(name = "difficulty_level")
    private Level level;

    @ManyToOne
    private Category category;

    public boolean isForSell() {
        if (category == null) {
            return false;
        }
        return category.isForSell();
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public Lesson uuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTitleEng() {
        return titleEng;
    }

    public Lesson titleEng(String titleEng) {
        this.titleEng = titleEng;
        return this;
    }

    public void setTitleEng(String titleEng) {
        this.titleEng = titleEng;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public Lesson indexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
        return this;
    }

    public void setIndexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
    }

    public String getPictureName() {
        return pictureName;
    }

    public Lesson pictureName(String pictureName) {
        this.pictureName = pictureName;
        return this;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public Level getLevel() {
        return level;
    }

    public Lesson level(Level level) {
        this.level = level;
        return this;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public Category getCategory() {
        return category;
    }

    public Lesson category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Lesson lesson = (Lesson) o;
        if (lesson.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lesson.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Lesson{" +
            "id=" + getId() +
            ", uuid='" + getUuid() + "'" +
            ", titleEng='" + getTitleEng() + "'" +
            ", indexOrder=" + getIndexOrder() +
            ", pictureName='" + getPictureName() + "'" +
            ", level='" + getLevel() + "'" +
            "}";
    }
}
