package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A LessonWord.
 */
@Entity
@Table(name = "lesson_word")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LessonWord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "index_order", nullable = false)
    private Integer indexOrder;

    @Column(name = "background")
    private Boolean background;

    @Column(name = "capitalization")
    private Boolean capitalization;

    @Column(name = "picture")
    private Boolean picture;

    @ManyToOne(optional = false)
    @NotNull
    @JoinColumn(name = "word_id", updatable = false)
    private Word word;

    @ManyToOne(optional = false)
    @NotNull
    @JoinColumn(name = "lesson_id", updatable = false)
    private Lesson lesson;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public LessonWord indexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
        return this;
    }

    public void setIndexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
    }

    public Boolean isBackground() {
        return background;
    }

    public LessonWord background(Boolean background) {
        this.background = background;
        return this;
    }

    public void setBackground(Boolean background) {
        this.background = background;
    }

    public Boolean isCapitalization() {
        return capitalization;
    }

    public LessonWord capitalization(Boolean capitalization) {
        this.capitalization = capitalization;
        return this;
    }

    public void setCapitalization(Boolean capitalization) {
        this.capitalization = capitalization;
    }

    public Boolean isPicture() {
        return picture;
    }

    public void setPicture(Boolean picture) {
        this.picture = picture;
    }

    public Word getWord() {
        return word;
    }

    public LessonWord word(Word word) {
        this.word = word;
        return this;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public LessonWord lesson(Lesson lesson) {
        this.lesson = lesson;
        return this;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LessonWord lessonWord = (LessonWord) o;
        if (lessonWord.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lessonWord.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LessonWord{" +
            "id=" + getId() +
            ", indexOrder=" + getIndexOrder() +
            ", background='" + isBackground() + "'" +
            ", capitalization='" + isCapitalization() + "'" +
            "}";
    }
}
