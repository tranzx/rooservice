package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import mars.roo.domain.enumeration.LearnDir;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Category.
 */
@Entity
@Table(name = "category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "uuid", nullable = false, updatable = false)
    private String uuid;

    @Column(name = "title_eng")
    private String titleEng;

    @NotNull
    @Column(name = "index_order", nullable = false)
    private Integer indexOrder;

    @Column(name = "for_sell")
    private Boolean forSell;

    @Column(name = "came_new")
    private Boolean cameNew;

    @Column(name = "comming_soon")
    private Boolean commingSoon;

    @Column(name = "title_fa")
    private String titleFa;

    @Column(name = "title_de")
    private String titleDe;

    public String title(LearnDir learnDir) {
        switch(learnDir.mother()) {
        case FA_IR:
            return titleFa;
        case EN_GB:
            return titleEng;
        case DE_DE:
            return titleDe;
        default:
            return titleEng;
        }
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public Category uuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTitleEng() {
        return titleEng;
    }

    public Category titleEng(String titleEng) {
        this.titleEng = titleEng;
        return this;
    }

    public void setTitleEng(String titleEng) {
        this.titleEng = titleEng;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public Category indexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
        return this;
    }

    public void setIndexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
    }

    public Boolean isForSell() {
        return forSell;
    }

    public Category forSell(Boolean forSell) {
        this.forSell = forSell;
        return this;
    }

    public void setForSell(Boolean forSell) {
        this.forSell = forSell;
    }

    public Boolean isCameNew() {
        return cameNew;
    }

    public Category cameNew(Boolean cameNew) {
        this.cameNew = cameNew;
        return this;
    }

    public void setCameNew(Boolean cameNew) {
        this.cameNew = cameNew;
    }

    public Boolean isCommingSoon() {
        return commingSoon;
    }

    public Category commingSoon(Boolean commingSoon) {
        this.commingSoon = commingSoon;
        return this;
    }

    public void setCommingSoon(Boolean commingSoon) {
        this.commingSoon = commingSoon;
    }

    public String getTitleFa() {
        return titleFa;
    }

    public Category titleFa(String titleFa) {
        this.titleFa = titleFa;
        return this;
    }

    public void setTitleFa(String titleFa) {
        this.titleFa = titleFa;
    }

    public String getTitleDe() {
        return titleDe;
    }

    public Category titleDe(String titleDe) {
        this.titleDe = titleDe;
        return this;
    }

    public void setTitleDe(String titleDe) {
        this.titleDe = titleDe;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Category category = (Category) o;
        if (category.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), category.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Category{" +
            "id=" + getId() +
            ", uuid='" + getUuid() + "'" +
            ", titleEng='" + getTitleEng() + "'" +
            ", indexOrder=" + getIndexOrder() +
            ", forSell='" + isForSell() + "'" +
            ", cameNew='" + isCameNew() + "'" +
            ", commingSoon='" + isCommingSoon() + "'" +
            ", titleFa='" + getTitleFa() + "'" +
            ", titleDe='" + getTitleDe() + "'" +
            "}";
    }

}
