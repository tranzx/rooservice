package mars.roo.domain;

import java.io.Serializable;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import mars.roo.domain.enumeration.SubscriptionType;
import mars.roo.web.rest.xmarket.CafebazaarReceipt;
import mars.roo.web.rest.xmarket.XMarketSubscribeCommand;

/**
 * A CafebazaarEvent.
 */
@Entity
@Table(name = "cafebazaar_event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CafebazaarEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "order_id", updatable = false)
    private String orderId;

    @Column(name = "product_id", updatable = false)
    private String productId;

    @Column(name = "purchase_time", updatable = false)
    private Long purchaseTime;

    @Column(name = "purchase_state")
    private Integer purchaseState;  // 0 = bought | 1 = Cancelled | 2 = Returned

    @Column(name = "purchase_token", updatable = false)
    private String purchaseToken;

    @Column(name = "signature", updatable = false)
    private String signature;

    @Column(name = "package_name", updatable = false)
    private String packageName;

    @Column(name = "developer_payload", updatable = false)
    private String developerPayload;

    @Column(name = "description", updatable = false)
    private String description;

    @NotNull
    @Column(name = "creation_time", nullable = false, updatable = false)
    private ZonedDateTime creationTime;

    @NotNull
    @Column(name = "owner", nullable = false, updatable = false)
    private String owner;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "subscription_type", nullable = false, updatable = false)
    private SubscriptionType subscriptionType;

    public CafebazaarEvent() {
        // Needed by ORM
    }

    public CafebazaarEvent(XMarketSubscribeCommand cmd, CafebazaarReceipt receipt, String owner) {
        this.owner = owner;
        this.subscriptionType = cmd.getSubscriptionType();
        this.creationTime = ZonedDateTime.now(ZoneOffset.UTC);
        this.description = cmd.getDescription();
        this.developerPayload = receipt.getDeveloperPayload();
        this.packageName = receipt.getPackageName();
        this.signature = receipt.getSignature();
        this.purchaseToken = receipt.getPurchaseToken();
        this.purchaseTime = receipt.getPurchaseTime();
        this.purchaseState = receipt.getPurchaseState();
        this.productId = receipt.getProductId();
        this.orderId = receipt.getOrderId();
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public CafebazaarEvent orderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public CafebazaarEvent productId(String productId) {
        this.productId = productId;
        return this;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Long getPurchaseTime() {
        return purchaseTime;
    }

    public Date purchaseTime() {
        return new Date(purchaseTime);
    }

    public CafebazaarEvent purchaseTime(Long purchaseTime) {
        this.purchaseTime = purchaseTime;
        return this;
    }

    public void setPurchaseTime(Long purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public Integer getPurchaseState() {
        return purchaseState;
    }

    public CafebazaarEvent purchaseState(Integer purchaseState) {
        this.purchaseState = purchaseState;
        return this;
    }

    public void setPurchaseState(Integer purchaseState) {
        this.purchaseState = purchaseState;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    public CafebazaarEvent purchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
        return this;
    }

    public void setPurchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
    }

    public String getSignature() {
        return signature;
    }

    public CafebazaarEvent signature(String signature) {
        this.signature = signature;
        return this;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPackageName() {
        return packageName;
    }

    public CafebazaarEvent packageName(String packageName) {
        this.packageName = packageName;
        return this;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDeveloperPayload() {
        return developerPayload;
    }

    public CafebazaarEvent developerPayload(String developerPayload) {
        this.developerPayload = developerPayload;
        return this;
    }

    public void setDeveloperPayload(String developerPayload) {
        this.developerPayload = developerPayload;
    }

    public String getDescription() {
        return description;
    }

    public CafebazaarEvent description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getCreationTime() {
        return creationTime;
    }

    public CafebazaarEvent creationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public String getOwner() {
        return owner;
    }

    public CafebazaarEvent owner(String owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public CafebazaarEvent subscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
        return this;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CafebazaarEvent cafebazaarEvent = (CafebazaarEvent) o;
        if (cafebazaarEvent.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cafebazaarEvent.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CafebazaarEvent{" +
            "id=" + getId() +
            ", orderId='" + getOrderId() + "'" +
            ", productId='" + getProductId() + "'" +
            ", purchaseTime=" + getPurchaseTime() +
            ", purchaseState=" + getPurchaseState() +
            ", purchaseToken='" + getPurchaseToken() + "'" +
            ", signature='" + getSignature() + "'" +
            ", packageName='" + getPackageName() + "'" +
            ", developerPayload='" + getDeveloperPayload() + "'" +
            ", description='" + getDescription() + "'" +
            ", creationTime='" + getCreationTime() + "'" +
            ", owner='" + getOwner() + "'" +
            ", subscriptionType='" + getSubscriptionType() + "'" +
            "}";
    }

}
