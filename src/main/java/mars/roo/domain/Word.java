package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import mars.roo.domain.enumeration.Language;
import mars.roo.domain.enumeration.WordType;

/**
 * A Word.
 */
@Entity
@Table(name = "roo_word")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Word implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "uuid")
    private String uuid;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "word_type", nullable = false)
    private WordType wordType;

    @NotNull
    @Column(name = "en_gb", nullable = false)
    private String enGb;

    @Column(name = "fa_ir")
    private String faIr;

    @Column(name = "de_de")
    private String deDe;

    public String value(Language lang) {
        switch(lang) {
        case EN_GB:
            return getEnGb();
        case FA_IR:
            return getFaIr();
        case DE_DE:
            return getDeDe();
        default:
            return getEnGb();
        }
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public WordType getWordType() {
        return wordType;
    }

    public Word wordType(WordType wordType) {
        this.wordType = wordType;
        return this;
    }

    public void setWordType(WordType wordType) {
        this.wordType = wordType;
    }

    public String getEnGb() {
        return enGb;
    }

    public Word enGb(String enGb) {
        this.enGb = enGb;
        return this;
    }

    public void setEnGb(String enGb) {
        this.enGb = enGb;
    }

    public String getFaIr() {
        return faIr;
    }

    public Word faIr(String faIr) {
        this.faIr = faIr;
        return this;
    }

    public void setFaIr(String faIr) {
        this.faIr = faIr;
    }

    public String getDeDe() {
        return deDe;
    }

    public Word deDe(String deDe) {
        this.deDe = deDe;
        return this;
    }

    public void setDeDe(String deDe) {
        this.deDe = deDe;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Word word = (Word) o;
        if (word.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), word.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Word{" +
            "id=" + getId() +
            ", wordType='" + getWordType() + "'" +
            ", enGb='" + getEnGb() + "'" +
            ", faIr='" + getFaIr() + "'" +
            ", deDe='" + getDeDe() + "'" +
            "}";
    }
}
