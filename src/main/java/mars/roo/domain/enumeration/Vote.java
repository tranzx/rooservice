package mars.roo.domain.enumeration;

/**
 * The Vote enumeration.
 */
public enum Vote {
    UP(1), DOWN(-1);

    private final Integer value;
    private Vote(Integer value) {
        this.value = value;
    }

    public final int value() {
        return this.value;
    }

}
