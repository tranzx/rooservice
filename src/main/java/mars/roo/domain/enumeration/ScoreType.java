package mars.roo.domain.enumeration;

/**
 * The ScoreType enumeration.
 */
public enum ScoreType {
    LESSON, DAILY
}
