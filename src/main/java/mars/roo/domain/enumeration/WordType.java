package mars.roo.domain.enumeration;

/**
 * The WordType enumeration.
 */
public enum WordType {
    Word, Verb, Phrase
}
