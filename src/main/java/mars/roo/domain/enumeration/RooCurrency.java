package mars.roo.domain.enumeration;

/**
 * The RooCurrency enumeration.
 */
public enum RooCurrency {
    IRR, USD, TOMAN
}
