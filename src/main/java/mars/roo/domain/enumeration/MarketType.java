package mars.roo.domain.enumeration;

/**
 * The MarketType enumeration.
 */
public enum MarketType {
    cafebazaar, avvalmarket, iranapps, myket
}
