package mars.roo.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    FA_IR, EN_GB, DE_DE,
    ES_ES, FR_FR, IT_IT, RU_RU, JA_JP, KO_KR, ZH_CN, TR_TR, AR_SA, HE_IL,
    PT_PT, NL_NL, SV_SE, NB_NO, DA_DK, FI_FI, EL_GR, RO_RO, AF_ZA, HR_HR,
    PL_PL, BG_BG, CS_CZ, HU_HU, UK_UA, VI_VN, HI_IN, ID_ID, TH_TH;
}
