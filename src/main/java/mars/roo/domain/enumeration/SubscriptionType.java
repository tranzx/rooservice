package mars.roo.domain.enumeration;

/**
 * The SubscriptionType enumeration.
 */
public enum SubscriptionType {
    ONE_MONTH, ONE_YEAR;

    public Integer priceForZarinpalPay() {
        switch(this) {
        case ONE_MONTH:
            return 8000;
        case ONE_YEAR:
            return 50000;
        default:
            return null;
        }
    }

}
