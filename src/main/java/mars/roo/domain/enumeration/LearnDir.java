package mars.roo.domain.enumeration;

public enum LearnDir {

    FA_IR$EN_GB, FA_IR$DE_DE,
    FA_IR$ES_ES, FA_IR$FR_FR, FA_IR$IT_IT, FA_IR$RU_RU, FA_IR$JA_JP, FA_IR$KO_KR,
    FA_IR$ZH_CN, FA_IR$TR_TR, FA_IR$AR_SA, FA_IR$HE_IL, FA_IR$PT_PT, FA_IR$NL_NL,
    FA_IR$SV_SE, FA_IR$NB_NO, FA_IR$DA_DK, FA_IR$FI_FI, FA_IR$EL_GR, FA_IR$RO_RO,
    FA_IR$AF_ZA, FA_IR$HR_HR, FA_IR$PL_PL, FA_IR$BG_BG, FA_IR$CS_CZ, FA_IR$HU_HU,
    FA_IR$UK_UA, FA_IR$VI_VN, FA_IR$HI_IN, FA_IR$ID_ID, FA_IR$TH_TH,

    EN_GB$DE_DE,
    EN_GB$ES_ES, EN_GB$FR_FR, EN_GB$IT_IT, EN_GB$RU_RU, EN_GB$JA_JP, EN_GB$KO_KR,
    EN_GB$ZH_CN, EN_GB$TR_TR, EN_GB$AR_SA, EN_GB$HE_IL, EN_GB$PT_PT, EN_GB$NL_NL,
    EN_GB$SV_SE, EN_GB$NB_NO, EN_GB$DA_DK, EN_GB$FI_FI, EN_GB$EL_GR, EN_GB$RO_RO,
    EN_GB$AF_ZA, EN_GB$HR_HR, EN_GB$PL_PL, EN_GB$BG_BG, EN_GB$CS_CZ, EN_GB$HU_HU,
    EN_GB$UK_UA, EN_GB$VI_VN, EN_GB$HI_IN, EN_GB$ID_ID, EN_GB$TH_TH,

    DE_DE$EN_GB,
    DE_DE$ES_ES, DE_DE$FR_FR, DE_DE$IT_IT, DE_DE$RU_RU, DE_DE$JA_JP, DE_DE$KO_KR,
    DE_DE$ZH_CN, DE_DE$TR_TR, DE_DE$AR_SA, DE_DE$HE_IL, DE_DE$PT_PT, DE_DE$NL_NL,
    DE_DE$SV_SE, DE_DE$NB_NO, DE_DE$DA_DK, DE_DE$FI_FI, DE_DE$EL_GR, DE_DE$RO_RO,
    DE_DE$AF_ZA, DE_DE$HR_HR, DE_DE$PL_PL, DE_DE$BG_BG, DE_DE$CS_CZ, DE_DE$HU_HU,
    DE_DE$UK_UA, DE_DE$VI_VN, DE_DE$HI_IN, DE_DE$ID_ID, DE_DE$TH_TH;


    public Language mother() {
        return Language.valueOf(name().substring(0, name().indexOf("$")));
    }

    public Language target() {
        return Language.valueOf(name().substring(name().indexOf("$") + 1, name().length()));
    }

    @Override
    public String toString() {
        return name();
    }

}
