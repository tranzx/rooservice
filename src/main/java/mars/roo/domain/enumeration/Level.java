package mars.roo.domain.enumeration;

/**
 * The Difficulty Level enumeration.
 */
public enum Level {
    Beginner, Intermediate, Advanced
}
