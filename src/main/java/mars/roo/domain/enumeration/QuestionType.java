package mars.roo.domain.enumeration;

/**
 * The QuestionType enumeration.
 */
public enum QuestionType {
    MultiSelect, TwoPicture, FourPicture, FourText, MultiCheck, OneCheck,
    Writing, Speaking, Conversation, Words;
}
