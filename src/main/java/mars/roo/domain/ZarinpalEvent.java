package mars.roo.domain;

import java.io.Serializable;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import mars.jzarinpal.domain.ZarinStatus;
import mars.roo.domain.enumeration.RooCurrency;
import mars.roo.domain.enumeration.SubscriptionType;
import mars.roo.web.rest.zarinpal.ZarinCodeCommand;

/**
 * A ZarinpalEvent.
 */
@Entity
@Table(name = "zarinpal_event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ZarinpalEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "payment_token", nullable = false, unique = true, updatable = false)
    private String paymentToken;

    @NotNull
    @Column(name = "amount", nullable = false, updatable = false)
    private Integer amount;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false, updatable = false)
    private RooCurrency currency;

    @NotNull
    @Column(name = "description", nullable = false, updatable = false)
    private String description;

    @Column(name = "email")
    private String email;

    @Column(name = "mobile")
    private String mobile;

    @Enumerated(EnumType.STRING)
    @Column(name = "zarin_status")
    private ZarinStatus zarinStatus;

    @Column(name = "ref_id")
    private Long refId;

    @NotNull
    @Column(name = "creation_time", nullable = false, updatable = false)
    private ZonedDateTime creationTime;

    @NotNull
    @Column(name = "owner", nullable = false, updatable = false)
    private String owner;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "subscription_type", nullable = false, updatable = false)
    private SubscriptionType subscriptionType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public static ZarinpalEvent from(ZarinCodeCommand cmd, String owner) {
        ZarinpalEvent event = new ZarinpalEvent();
        event.setOwner(owner);
        event.setDescription(cmd.getDescription());
        event.setSubscriptionType(cmd.getSubscriptionType());
        event.setAmount(cmd.getSubscriptionType().priceForZarinpalPay());
        event.setCreationTime(ZonedDateTime.now(ZoneOffset.UTC));
        event.setCurrency(RooCurrency.TOMAN);
        return event;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaymentToken() {
        return paymentToken;
    }

    public ZarinpalEvent paymentToken(String paymentToken) {
        this.paymentToken = paymentToken;
        return this;
    }

    public void setPaymentToken(String paymentToken) {
        this.paymentToken = paymentToken;
    }

    public Integer getAmount() {
        return amount;
    }

    public ZarinpalEvent amount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public RooCurrency getCurrency() {
        return currency;
    }

    public ZarinpalEvent currency(RooCurrency currency) {
        this.currency = currency;
        return this;
    }

    public void setCurrency(RooCurrency currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public ZarinpalEvent description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public ZarinpalEvent email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public ZarinpalEvent mobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public ZarinStatus getZarinStatus() {
        return zarinStatus;
    }

    public ZarinpalEvent zarinStatus(ZarinStatus zarinStatus) {
        this.zarinStatus = zarinStatus;
        return this;
    }

    public void setZarinStatus(ZarinStatus zarinStatus) {
        this.zarinStatus = zarinStatus;
    }

    public Long getRefId() {
        return refId;
    }

    public ZarinpalEvent refId(Long refId) {
        this.refId = refId;
        return this;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public ZonedDateTime getCreationTime() {
        return creationTime;
    }

    public ZarinpalEvent creationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public String getOwner() {
        return owner;
    }

    public ZarinpalEvent owner(String owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public ZarinpalEvent subscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
        return this;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ZarinpalEvent zarinpalEvent = (ZarinpalEvent) o;
        if (zarinpalEvent.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), zarinpalEvent.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ZarinpalEvent{" +
            "id=" + getId() +
            ", paymentToken='" + getPaymentToken() + "'" +
            ", amount=" + getAmount() +
            ", currency='" + getCurrency() + "'" +
            ", description='" + getDescription() + "'" +
            ", email='" + getEmail() + "'" +
            ", mobile='" + getMobile() + "'" +
            ", zarinStatus='" + getZarinStatus() + "'" +
            ", refId=" + getRefId() +
            ", creationTime='" + getCreationTime() + "'" +
            ", owner='" + getOwner() + "'" +
            ", subscriptionType='" + getSubscriptionType() + "'" +
            "}";
    }

}
