package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import mars.roo.domain.enumeration.SubscriptionType;
import mars.roo.infrastructure.cafebazaar.SubscriptionCheckDTO;
import mars.roo.web.rest.xmarket.XMarketReceipt;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;

/**
 * A Membership.
 */
@Entity
@Table(name = "membership")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Membership implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "owner", nullable = false, updatable = false)
    private String owner;

    @NotNull
    @Column(name = "start_date", nullable = false, updatable = false)
    private LocalDate startDate;

    @NotNull
    @Column(name = "end_date", nullable = false, updatable = false)
    private LocalDate endDate;

    @NotNull
    @Column(name = "creation_time", nullable = false, updatable = false)
    private ZonedDateTime creationTime;

    @ManyToOne
    @JoinColumn(name = "zarinpal_event_id", updatable = false)
    private ZarinpalEvent zarinpalEvent;

    @ManyToOne
    @JoinColumn(name = "cafebazaar_event_id", updatable = false)
    private CafebazaarEvent cafebazaarEvent;

    @ManyToOne
    @JoinColumn(name = "market_event_id", updatable = false)
    private MarketEvent marketEvent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public boolean isExtensionNeeded(SubscriptionCheckDTO subscriptionCheck) {
        LocalDate end = localDateFrom(subscriptionCheck.endDate());
        return end.isAfter(endDate);
    }

    public Membership extensionFor(CafebazaarEvent event, SubscriptionCheckDTO subscriptionCheck) {
        Membership membership = new Membership(event);
        membership.setEndDate(localDateFrom(subscriptionCheck.endDate()));
        return membership;
    }

    public Membership extensionFor(MarketEvent event, XMarketReceipt receipt) {
        Membership membership = new Membership(event);
        return membership;
    }

    public LocalDate localDateFrom(Date date) {
        return date.toInstant().atZone(ZoneOffset.UTC).toLocalDate();
    }

    public Membership() { }

    public Membership(ZarinpalEvent event) {
        this.zarinpalEvent = event;
        this.owner = event.getOwner();
        this.startDate = LocalDate.now(ZoneOffset.UTC);
        if (SubscriptionType.ONE_MONTH.equals(event.getSubscriptionType())) {
            this.endDate = LocalDate.now(ZoneOffset.UTC).plusMonths(1).plusDays(1);
        } else if (SubscriptionType.ONE_YEAR.equals(event.getSubscriptionType())) {
            this.endDate = LocalDate.now(ZoneOffset.UTC).plusYears(1);
        }
        this.creationTime = ZonedDateTime.now(ZoneOffset.UTC);
    }

    public Membership(CafebazaarEvent event) {
        this.cafebazaarEvent = event;
        this.owner = event.getOwner();
        this.startDate = localDateFrom(event.purchaseTime());
        if (SubscriptionType.ONE_MONTH.equals(event.getSubscriptionType())) {
            this.endDate = localDateFrom(event.purchaseTime()).plusMonths(1).plusDays(1);
        } else if (SubscriptionType.ONE_YEAR.equals(event.getSubscriptionType())) {
            this.endDate = localDateFrom(event.purchaseTime()).plusYears(1);
        }
        this.creationTime = ZonedDateTime.now(ZoneOffset.UTC);
    }

    public Membership(MarketEvent event) {
        this.marketEvent = event;
        this.owner = event.getOwner();
        this.startDate = LocalDate.now(ZoneOffset.UTC);
        switch(event.getSubscriptionType()) {
            case ONE_MONTH:
                this.endDate = this.startDate.plusMonths(1).plusDays(1);
                break;
            case ONE_YEAR:
                this.endDate = this.startDate.plusYears(1);
                break;
        }
        this.creationTime = ZonedDateTime.now(ZoneOffset.UTC);
    }

    public boolean isExtensionNeeded(XMarketReceipt receipt) {
        Date ptime = new Date(receipt.getPurchaseTime());
        LocalDate pdate = localDateFrom(ptime);
        LocalDate edate = this.endDate.minusDays(2);    // Mines 2 days to make sure new starting date doesn't overlap with previous end date.
        return edate.isBefore(pdate);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public Membership owner(String owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Membership startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Membership endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public ZonedDateTime getCreationTime() {
        return creationTime;
    }

    public Membership creationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public ZarinpalEvent getZarinpalEvent() {
        return zarinpalEvent;
    }

    public Membership zarinpalEvent(ZarinpalEvent zarinpalEvent) {
        this.zarinpalEvent = zarinpalEvent;
        return this;
    }

    public void setZarinpalEvent(ZarinpalEvent zarinpalEvent) {
        this.zarinpalEvent = zarinpalEvent;
    }

    public CafebazaarEvent getCafebazaarEvent() {
        return cafebazaarEvent;
    }

    public Membership cafebazaarEvent(CafebazaarEvent cafebazaarEvent) {
        this.cafebazaarEvent = cafebazaarEvent;
        return this;
    }

    public void setCafebazaarEvent(CafebazaarEvent cafebazaarEvent) {
        this.cafebazaarEvent = cafebazaarEvent;
    }

    public MarketEvent getMarketEvent() {
        return marketEvent;
    }

    public void setMarketEvent(MarketEvent marketEvent) {
        this.marketEvent = marketEvent;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Membership membership = (Membership) o;
        if (membership.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), membership.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Membership{" +
            "id=" + getId() +
            ", owner='" + getOwner() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", creationTime='" + getCreationTime() + "'" +
            "}";
    }

}
