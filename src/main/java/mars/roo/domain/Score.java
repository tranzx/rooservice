package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import mars.roo.domain.enumeration.Language;
import mars.roo.domain.enumeration.LearnDir;

import mars.roo.domain.enumeration.ScoreType;

import mars.roo.domain.enumeration.Level;

/**
 * A Score.
 */
@Entity
@Table(name = "score")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Score implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "owner", nullable = false, updatable = false)
    private String owner;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "learn_dir", nullable = false, updatable = false)
    private LearnDir learnDir;

    @Enumerated(EnumType.STRING)
    @Column(name = "target", updatable = false)
    private Language target;

    @NotNull
    @Column(name = "score", nullable = false, updatable = false)
    private Integer score;

    @NotNull
    @Column(name = "star", nullable = false, updatable = false)
    private Integer star;

    @NotNull
    @Column(name = "score_date", nullable = false, updatable = false)
    private LocalDate scoreDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "score_type", nullable = false, updatable = false)
    private ScoreType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "difficulty_level", updatable = false)
    private Level difficultyLevel;

    @Column(name = "lesson_uuid", updatable = false)
    private String lessonUuid;

    @Column(name = "category_uuid", updatable = false)
    private String categoryUuid;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public Score owner(String owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public LearnDir getLearnDir() {
        return learnDir;
    }

    public Score learnDir(LearnDir learnDir) {
        this.learnDir = learnDir;
        return this;
    }

    public void setLearnDir(LearnDir learnDir) {
        this.learnDir = learnDir;
    }

    public Language getTarget() {
        return target;
    }

    public Score target(Language language) {
        this.target = language;
        return this;
    }

    public void setTarget(Language target) {
        this.target = target;
    }

    public Integer getScore() {
        return score;
    }

    public Score score(Integer score) {
        this.score = score;
        return this;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getStar() {
        return star;
    }

    public Score star(Integer star) {
        this.star = star;
        return this;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public LocalDate getScoreDate() {
        return scoreDate;
    }

    public Score scoreDate(LocalDate scoreDate) {
        this.scoreDate = scoreDate;
        return this;
    }

    public void setScoreDate(LocalDate scoreDate) {
        this.scoreDate = scoreDate;
    }

    public ScoreType getType() {
        return type;
    }

    public Score type(ScoreType type) {
        this.type = type;
        return this;
    }

    public void setType(ScoreType type) {
        this.type = type;
    }

    public Level getDifficultyLevel() {
        return difficultyLevel;
    }

    public Score difficultyLevel(Level difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
        return this;
    }

    public void setDifficultyLevel(Level difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public String getLessonUuid() {
        return lessonUuid;
    }

    public Score lessonUuid(String lessonUuid) {
        this.lessonUuid = lessonUuid;
        return this;
    }

    public void setLessonUuid(String lessonUuid) {
        this.lessonUuid = lessonUuid;
    }

    public String getCategoryUuid() {
        return categoryUuid;
    }

    public Score categoryUuid(String categoryUuid) {
        this.categoryUuid = categoryUuid;
        return this;
    }

    public void setCategoryUuid(String categoryUuid) {
        this.categoryUuid = categoryUuid;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Score score = (Score) o;
        if (score.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), score.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Score{" +
            "id=" + getId() +
            ", owner='" + getOwner() + "'" +
            ", learnDir='" + getLearnDir() + "'" +
            ", score=" + getScore() +
            ", star=" + getStar() +
            ", scoreDate='" + getScoreDate() + "'" +
            ", type='" + getType() + "'" +
            ", difficultyLevel='" + getDifficultyLevel() + "'" +
            ", lessonUuid='" + getLessonUuid() + "'" +
            ", categoryUuid='" + getCategoryUuid() + "'" +
            "}";
    }
}
