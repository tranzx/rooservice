package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import mars.roo.domain.enumeration.Language;

/**
 * A CommWord.
 */
@Entity
@Table(name = "comm_word")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CommWord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "language", nullable = false)
    private Language language;

    @Column(name = "text")
    private String text;

    @Column(name = "base")
    private Boolean base;

    @Column(name = "reported")
    private Boolean reported;

    @Column(name = "author")
    private String author;

    @Column(name = "created")
    private ZonedDateTime created;

    @Column(name = "updated")
    private ZonedDateTime updated;

    @ManyToOne(optional = false)
    @NotNull
    private Word word;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public CommWord uuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Language getLanguage() {
        return language;
    }

    public CommWord language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public CommWord text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean isBase() {
        return base;
    }

    public CommWord base(Boolean base) {
        this.base = base;
        return this;
    }

    public void setBase(Boolean base) {
        this.base = base;
    }

    public Boolean isReported() {
        return reported;
    }

    public CommWord reported(Boolean reported) {
        this.reported = reported;
        return this;
    }

    public void setReported(Boolean reported) {
        this.reported = reported;
    }

    public String getAuthor() {
        return author;
    }

    public CommWord author(String author) {
        this.author = author;
        return this;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public CommWord created(ZonedDateTime created) {
        this.created = created;
        return this;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getUpdated() {
        return updated;
    }

    public CommWord updated(ZonedDateTime updated) {
        this.updated = updated;
        return this;
    }

    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }

    public Word getWord() {
        return word;
    }

    public CommWord word(Word word) {
        this.word = word;
        return this;
    }

    public void setWord(Word word) {
        this.word = word;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommWord commWord = (CommWord) o;
        if (commWord.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), commWord.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CommWord{" +
            "id=" + getId() +
            ", uuid='" + getUuid() + "'" +
            ", language='" + getLanguage() + "'" +
            ", text='" + getText() + "'" +
            ", base='" + isBase() + "'" +
            ", reported='" + isReported() + "'" +
            ", author='" + getAuthor() + "'" +
            ", created='" + getCreated() + "'" +
            ", updated='" + getUpdated() + "'" +
            "}";
    }
}
