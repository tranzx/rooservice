package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import mars.roo.domain.enumeration.Language;

import mars.roo.domain.enumeration.WordType;

/**
 * A LessonCommWord.
 */
@Entity
@Table(name = "lesson_comm_word")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LessonCommWord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Column(name = "index_order", nullable = false)
    private Integer indexOrder;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "language", nullable = false)
    private Language language;

    @NotNull
    @Column(name = "text", nullable = false)
    private String text;

    @Column(name = "text_eng")
    private String textEng;

    @Column(name = "vote")
    private Integer vote;

    @Column(name = "comm_word_uuid")
    private String commWordUuid;

    @Column(name = "picture")
    private Boolean picture;

    @Column(name = "capitalization")
    private Boolean capitalization;

    @Column(name = "background")
    private Boolean background;

    @Column(name = "base")
    private Boolean base;

    @Column(name = "author")
    private String author;

    @Enumerated(EnumType.STRING)
    @Column(name = "word_type")
    private WordType wordType;

    @NotNull
    @Column(name = "lesson_id", nullable = false)
    private Long lessonId;

    @NotNull
    @Column(name = "word_id", nullable = false)
    private Long wordId;

    @NotNull
    @Column(name = "word_uuid", nullable = false)
    private String wordUuid;

    @NotNull
    @Column(name = "lesson_uuid", nullable = false)
    private String lessonUuid;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public LessonCommWord() {
        // Needed by ORM
    }

    public LessonCommWord(LessonWord lw, CommWord cw) {
        id = String.format("%d+%d", lw.getId(), cw.getId());
        indexOrder = lw.getIndexOrder();
        language = cw.getLanguage();
        text = cw.getText();
        textEng = cw.getWord().getEnGb();
        commWordUuid = cw.getUuid();
        picture = lw.isPicture();
        base = cw.isBase();
        author = cw.getAuthor();
        wordType = cw.getWord().getWordType();
        lessonId = lw.getLesson().getId();
        wordId = lw.getWord().getId();
        wordUuid = lw.getWord().getUuid();
        lessonUuid = lw.getLesson().getUuid();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public String iorder() {
        return this.indexOrder.toString();
    }

    public LessonCommWord indexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
        return this;
    }

    public void setIndexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
    }

    public Language getLanguage() {
        return language;
    }

    public LessonCommWord language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public LessonCommWord text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextEng() {
        return textEng;
    }

    public void setTextEng(String textEng) {
        this.textEng = textEng;
    }

    public Integer getVote() {
        return vote;
    }

    public LessonCommWord vote(Integer vote) {
        this.vote = vote;
        return this;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    public String getCommWordUuid() {
        return commWordUuid;
    }

    public LessonCommWord commWordUuid(String commWordUuid) {
        this.commWordUuid = commWordUuid;
        return this;
    }

    public void setCommWordUuid(String commWordUuid) {
        this.commWordUuid = commWordUuid;
    }

    public Boolean isPicture() {
        return picture;
    }

    public LessonCommWord picture(Boolean picture) {
        this.picture = picture;
        return this;
    }

    public void setPicture(Boolean picture) {
        this.picture = picture;
    }

    public Boolean isCapitalization() {
        return capitalization;
    }

    public LessonCommWord capitalization(Boolean capitalization) {
        this.capitalization = capitalization;
        return this;
    }

    public void setCapitalization(Boolean capitalization) {
        this.capitalization = capitalization;
    }

    public Boolean isBase() {
        return base;
    }

    public Boolean isBackground() {
        return background;
    }

    public LessonCommWord background(Boolean background) {
        this.background = background;
        return this;
    }

    public void setBackground(Boolean background) {
        this.background = background;
    }

    public LessonCommWord base(Boolean base) {
        this.base = base;
        return this;
    }

    public void setBase(Boolean base) {
        this.base = base;
    }

    public String getAuthor() {
        return author;
    }

    public LessonCommWord author(String author) {
        this.author = author;
        return this;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public WordType getWordType() {
        return wordType;
    }

    public LessonCommWord wordType(WordType wordType) {
        this.wordType = wordType;
        return this;
    }

    public void setWordType(WordType wordType) {
        this.wordType = wordType;
    }

    public Long getLessonId() {
        return lessonId;
    }

    public LessonCommWord lessonId(Long lessonId) {
        this.lessonId = lessonId;
        return this;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public Long getWordId() {
        return wordId;
    }

    public LessonCommWord wordId(Long wordId) {
        this.wordId = wordId;
        return this;
    }

    public void setWordId(Long wordId) {
        this.wordId = wordId;
    }

    public String getWordUuid() {
        return wordUuid;
    }

    public LessonCommWord wordUuid(String wordUuid) {
        this.wordUuid = wordUuid;
        return this;
    }

    public void setWordUuid(String wordUuid) {
        this.wordUuid = wordUuid;
    }

    public String getLessonUuid() {
        return lessonUuid;
    }

    public LessonCommWord lessonUuid(String lessonUuid) {
        this.lessonUuid = lessonUuid;
        return this;
    }

    public void setLessonUuid(String lessonUuid) {
        this.lessonUuid = lessonUuid;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LessonCommWord lessonCommWord = (LessonCommWord) o;
        if (lessonCommWord.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lessonCommWord.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LessonCommWord{" +
            "id=" + getId() +
            ", indexOrder=" + getIndexOrder() +
            ", language='" + getLanguage() + "'" +
            ", text='" + getText() + "'" +
            ", vote=" + getVote() +
            ", commWordUuid='" + getCommWordUuid() + "'" +
            ", picture='" + isPicture() + "'" +
            ", wordType='" + getWordType() + "'" +
            ", lessonId=" + getLessonId() +
            ", wordId=" + getWordId() +
            ", wordUuid='" + getWordUuid() + "'" +
            ", lessonUuid='" + getLessonUuid() + "'" +
            "}";
    }
}
