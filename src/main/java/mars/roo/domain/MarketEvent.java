package mars.roo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import mars.roo.domain.enumeration.SubscriptionType;
import mars.roo.web.rest.xmarket.XMarketReceipt;
import mars.roo.web.rest.xmarket.XMarketSubscribeCommand;
import mars.roo.domain.enumeration.MarketType;

/**
 * A MarketEvent.
 */
@Entity
@Table(name = "market_event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MarketEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Column(name = "payment", nullable = false)
    private String payment;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "subscription_type", nullable = false)
    private SubscriptionType subscriptionType;

    @NotNull
    @Column(name = "owner", nullable = false)
    private String owner;

    @Column(name = "purchase_token")
    private String purchaseToken;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "market_type", nullable = false)
    private MarketType marketType;

    public MarketEvent() {
        // Needed by ORM
    }

    public MarketEvent(XMarketSubscribeCommand cmd, XMarketReceipt receipt, String owner) {
        this.owner = owner;
        this.description = cmd.getDescription();
        this.subscriptionType = cmd.getSubscriptionType();
        this.payment = cmd.getPaymentApiReturnString();
        this.marketType = MarketType.avvalmarket;
        this.purchaseToken = receipt.getPurchaseToken();
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public MarketEvent description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPayment() {
        return payment;
    }

    public MarketEvent payment(String payment) {
        this.payment = payment;
        return this;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public MarketEvent subscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
        return this;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getOwner() {
        return owner;
    }

    public MarketEvent owner(String owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    public MarketEvent purchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
        return this;
    }

    public void setPurchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
    }

    public MarketType getMarketType() {
        return marketType;
    }

    public MarketEvent marketType(MarketType marketType) {
        this.marketType = marketType;
        return this;
    }

    public void setMarketType(MarketType marketType) {
        this.marketType = marketType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MarketEvent marketEvent = (MarketEvent) o;
        if (marketEvent.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), marketEvent.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MarketEvent{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", payment='" + getPayment() + "'" +
            ", subscriptionType='" + getSubscriptionType() + "'" +
            ", owner='" + getOwner() + "'" +
            ", purchaseToken='" + getPurchaseToken() + "'" +
            ", marketType='" + getMarketType() + "'" +
            "}";
    }
}
