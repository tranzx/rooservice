package mars.roo.infrastructure.myket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import mars.roo.web.rest.xmarket.XMarketReceipt;

public class MyketApiClient {

    public final static String X_ACCESS_TOKEN = "cda96fb7-8138-436c-8712-c725c5f754db";
    public final static String API_BASE_URL = "http://developer.myket.ir/api";
    private final RestTemplate restTemplate;

    public MyketApiClient() {
        restTemplate = new RestTemplate();
    }

    public SubscriptionCheckDTO subscriptionCheck(XMarketReceipt receipt) {
        String url = String.format("%s/applications/%s/purchases/subscription/%s/tokens/%s", API_BASE_URL, receipt.getPackageName(), receipt.getProductId(), receipt.getPurchaseToken());
        log.info("PurchaseCheckUrl: {}", url);
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Access-Token", X_ACCESS_TOKEN);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<SubscriptionCheckDTO> res = restTemplate.exchange(url, HttpMethod.GET, entity, SubscriptionCheckDTO.class);
        return res.getBody();
    }

    private final Logger log = LoggerFactory.getLogger(getClass());
}
