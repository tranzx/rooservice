package mars.roo.infrastructure.myket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionCheckDTO {

    private Long startTimeMillis;
    private Long expiryTimeMillis;
    private String kind;
    private Boolean autoRenewing;
    private String developerPayload;

    public Long getStartTimeMillis() {
        return startTimeMillis;
    }
    public void setStartTimeMillis(Long startTimeMillis) {
        this.startTimeMillis = startTimeMillis;
    }
    public Long getExpiryTimeMillis() {
        return expiryTimeMillis;
    }
    public void setExpiryTimeMillis(Long expiryTimeMillis) {
        this.expiryTimeMillis = expiryTimeMillis;
    }
    public String getKind() {
        return kind;
    }
    public void setKind(String kind) {
        this.kind = kind;
    }
    public Boolean getAutoRenewing() {
        return autoRenewing;
    }
    public void setAutoRenewing(Boolean autoRenewing) {
        this.autoRenewing = autoRenewing;
    }
    public String getDeveloperPayload() {
        return developerPayload;
    }
    public void setDeveloperPayload(String developerPayload) {
        this.developerPayload = developerPayload;
    }

}