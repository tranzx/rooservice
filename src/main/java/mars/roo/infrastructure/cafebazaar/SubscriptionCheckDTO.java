package mars.roo.infrastructure.cafebazaar;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class SubscriptionCheckDTO {

    String kind;
    Long initiationTimestampMsec;
    Long validUntilTimestampMsec;
    Boolean autoRenewing;

    public Date endDate() {
        return new Date(validUntilTimestampMsec);
    }

    private boolean endDateIsAfterToday() {
        return endDate().toInstant().atZone(ZoneOffset.UTC).toLocalDate().isAfter(LocalDate.now(ZoneOffset.UTC));
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Long getInitiationTimestampMsec() {
        return initiationTimestampMsec;
    }

    public void setInitiationTimestampMsec(Long initiationTimestampMsec) {
        this.initiationTimestampMsec = initiationTimestampMsec;
    }

    public Long getValidUntilTimestampMsec() {
        return validUntilTimestampMsec;
    }

    public void setValidUntilTimestampMsec(Long validUntilTimestampMsec) {
        this.validUntilTimestampMsec = validUntilTimestampMsec;
    }

    public Boolean getAutoRenewing() {
        return autoRenewing;
    }

    public void setAutoRenewing(Boolean autoRenewing) {
        this.autoRenewing = autoRenewing;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public boolean isValid() {
        return initiationTimestampMsec != null &&
                validUntilTimestampMsec != null &&
                endDateIsAfterToday();
    }

}
