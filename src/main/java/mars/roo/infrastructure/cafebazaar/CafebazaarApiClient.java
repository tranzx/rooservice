package mars.roo.infrastructure.cafebazaar;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;

import mars.roo.domain.CafebazaarEvent;

@Service
public class CafebazaarApiClient {

    public final static String CLIENT_ID = "9wg0rUP5FGWkRjRwv5HVMuE9iL5Jrl37Oq5SubZP";
    public final static String CLIENT_SECRET = "Icafrp9jcM71MlJolQtSm2abDJWYHgqEnIIBtrjqKB162cqTRwhb5Cnt5rVV";
    public final static String TOKEN_REQUEST_URL = "https://pardakht.cafebazaar.ir/devapi/v2/auth/token/";
    public final static String REFRESH_TOKEN = "AsrRoeQ06N6wu0LCFN6UVS2oKgAcXf";
    public final static String TOKEN_TYPE = "Bearer";
    public final static List<String> SCOPES = Arrays.asList("androidpublisher");
    public final static String API_BASE_URL = "https://pardakht.cafebazaar.ir/devapi/v2/api";
    private OAuth2RestTemplate restTemplate;
    private Integer retryAttemptCounter = 0;

    public CafebazaarApiClient() {
        try {
            initializeRestTemplate();
        } catch (Exception e) {
            log.error("CafebazaarApiClient initalization failed!!", e);
        }
    }

    public SubscriptionCheckDTO subscriptionCheck(CafebazaarEvent event) {
        String url = String.format("%s/applications/%s/subscriptions/%s/purchases/%s?access_token=%s",
                API_BASE_URL, event.getPackageName(), event.getProductId(), event.getPurchaseToken(), restTemplate.getAccessToken().getValue());
        log.debug("SubscriptionCheckUrl: {}", url);
        try {
            ResponseEntity<SubscriptionCheckDTO> res = restTemplate.getForEntity(url, SubscriptionCheckDTO.class);
            return res.getBody();
        } catch(HttpClientErrorException e) {
            if (e.getRawStatusCode() == 401) {
                if (retryAttemptCounter < 2) {
                    obtainAccessTokenWithRefreshToken();
                    retryAttemptCounter++;
                    return subscriptionCheck(event);
                } else {
                    retryAttemptCounter = 0;
                }
            }
            log.error("HttpClientErrorException on checking Cafebazaar subscription: {}", url, e);
            throw e;
        }
    }

    public PurchaseCheckDTO purchaseCheck(CafebazaarEvent event) {
        String url = String.format("%s/validate/%s/inapp/%s/purchases/%s?access_token=%s",
                API_BASE_URL, event.getPackageName(), event.getProductId(), event.getPurchaseToken(), restTemplate.getAccessToken().getValue());
        log.debug("PurchaseCheckUrl: {}", url);
        try {
            ResponseEntity<PurchaseCheckDTO> res = restTemplate.getForEntity(url, PurchaseCheckDTO.class);
            return res.getBody();
        } catch(HttpClientErrorException e) {
            if (e.getRawStatusCode() == 401) {
                if (retryAttemptCounter < 2) {
                    obtainAccessTokenWithRefreshToken();
                    retryAttemptCounter++;
                    return purchaseCheck(event);
                } else {
                    retryAttemptCounter = 0;
                }
            }
            log.error("HttpClientErrorException on checking Cafebazaar subscription: {}", url, e);
            throw e;
        }
    }

    private void initializeRestTemplate() {
        if (restTemplate != null) {
            return;
        }
        DefaultOAuth2AccessToken accessToken = new DefaultOAuth2AccessToken("invalid");
        accessToken.setTokenType(TOKEN_TYPE);
        OAuth2RefreshToken refreshToken = new DefaultOAuth2RefreshToken(REFRESH_TOKEN);
        accessToken.setRefreshToken(refreshToken);

        restTemplate = new OAuth2RestTemplate(resourceDetails(), new DefaultOAuth2ClientContext(accessToken));
        obtainAccessTokenWithRefreshToken();
    }


    private void obtainAccessTokenWithRefreshToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("grant_type", "refresh_token");
        map.add("client_id", CLIENT_ID);
        map.add("client_secret", CLIENT_SECRET);
        map.add("refresh_token", REFRESH_TOKEN);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ResponseEntity<RefreshTokenDTO> res = restTemplate.postForEntity(TOKEN_REQUEST_URL, request, RefreshTokenDTO.class);
        if (!res.getStatusCode().equals(HttpStatus.OK)) {
            throw new RuntimeException("Error on obtaining ACCESS_TOEKN from Cafebazaar API. ADMIN_ATTENTION_NEEDED!!!");
        }
        RefreshTokenDTO dto = res.getBody();
        DefaultOAuth2AccessToken accessToken = new DefaultOAuth2AccessToken(dto.getAccess_token());
//      accessToken.setExpiration(new Date(new Date().getTime() + dto.getExpires_in()));  // We do refresh token manually, so we don't set value for expiration time to avoid auto refresh by restTemplate
        accessToken.setTokenType(TOKEN_TYPE);
        OAuth2RefreshToken refreshToken = new DefaultOAuth2RefreshToken(REFRESH_TOKEN);
        accessToken.setRefreshToken(refreshToken);

        OAuth2ClientContext clientContext = new DefaultOAuth2ClientContext(accessToken);
        restTemplate = new OAuth2RestTemplate(resourceDetails(), clientContext);
    }

    private ClientCredentialsResourceDetails resourceDetails() {
        ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
        resourceDetails.setClientSecret(CLIENT_SECRET);
        resourceDetails.setClientId(CLIENT_ID);
        resourceDetails.setAccessTokenUri(TOKEN_REQUEST_URL);
        resourceDetails.setScope(SCOPES);
        return resourceDetails;
    }

    private final Logger log = LoggerFactory.getLogger(getClass());
}
