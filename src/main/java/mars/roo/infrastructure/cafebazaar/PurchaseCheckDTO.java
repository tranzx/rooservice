package mars.roo.infrastructure.cafebazaar;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseCheckDTO {

    private Integer consumptionState;
    private Integer purchaseState;
    private String kind;
    private String developerPayload;
    private Long purchaseTime;

    public Integer getConsumptionState() {
        return consumptionState;
    }

    public void setConsumptionState(Integer consumptionState) {
        this.consumptionState = consumptionState;
    }

    public Integer getPurchaseState() {
        return purchaseState;
    }

    public void setPurchaseState(Integer purchaseState) {
        this.purchaseState = purchaseState;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getDeveloperPayload() {
        return developerPayload;
    }

    public void setDeveloperPayload(String developerPayload) {
        this.developerPayload = developerPayload;
    }

    public Long getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(Long purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

}
