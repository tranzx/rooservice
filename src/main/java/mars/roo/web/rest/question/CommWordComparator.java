package mars.roo.web.rest.question;

import java.util.Comparator;

import mars.roo.web.rest.question.CommWordPublicDTO.CWord;

public class CommWordComparator implements Comparator<CWord> {

    @Override
    public int compare(CWord cw1, CWord cw2) {
        if (cw1.getA() != null || cw2.getA() != null) {
            return cw1.getA() != null ? -1 : 1;
        }
        if (cw1.getB() != null || cw2.getB() != null) {
            return cw1.getB() != null ? -1 : 1;
        }
        if (cw1.getV() != null && cw2.getV() != null) {
            return cw2.getV().compareTo(cw1.getV());
        } else if (cw1.getV() != null || cw2.getV() != null) {
            return cw1.getV() != null ? -1 : 1;
        }
        return cw1.getW().compareTo(cw2.getW());
    }

}
