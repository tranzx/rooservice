package mars.roo.web.rest.question;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.roo.domain.Lesson;
import mars.roo.domain.LessonCommWord;
import mars.roo.domain.LessonWord;
import mars.roo.domain.Question;
import mars.roo.domain.enumeration.Language;
import mars.roo.domain.enumeration.LearnDir;
import mars.roo.domain.enumeration.Level;
import mars.roo.repository.CommWordRepository;
import mars.roo.repository.LessonCommWordRepository;
import mars.roo.repository.LessonRepository;
import mars.roo.repository.LessonWordRepository;
import mars.roo.service.QuestionService;
import mars.roo.web.rest.BaseAuthenticatedResource;

@RestController
@RequestMapping("/api/public")
public class QuestionPublicResource extends BaseAuthenticatedResource {

    private final QuestionService questionService;
    private final LessonRepository lessonRepository;
    private final CommWordRepository commWordRepository;
    private final LessonWordRepository lessonWordRepository;
    private final LessonCommWordRepository lessonCommWordRepository;
    private final Integer englishWordsTotal;

    public QuestionPublicResource(QuestionService questionService,
            LessonRepository lessonRepository,
            CommWordRepository commWordRepository,
            LessonWordRepository lessonWordRepository,
            LessonCommWordRepository lessonCommWordRepository) {
        this.questionService = questionService;
        this.lessonRepository = lessonRepository;
        this.commWordRepository = commWordRepository;
        this.lessonWordRepository = lessonWordRepository;
        this.lessonCommWordRepository = lessonCommWordRepository;
        this.englishWordsTotal = commWordRepository.countDistinctWordIdByLanguage(Language.EN_GB).intValue();
    }

    @GetMapping("/questions/{learnDir}/{difficulty}/{lessonUuid}")
    @Timed
    public ResponseEntity<LessonQuestionDTO> getQuestionPublicList(
            @PathVariable("learnDir") LearnDir learnDir,
            @PathVariable("difficulty") Level difficulty,
            @PathVariable("lessonUuid") String lessonUuid, Principal principal) {
        log.debug("Rest request to get list of questions for {}/{} and lesson uuid {}", learnDir, difficulty, lessonUuid);
        if (learnDir == null || difficulty == null || lessonUuid == null || StringUtils.isEmpty(lessonUuid)) {
            return ResponseEntity.badRequest().build();
        }
        Lesson lesson = lessonRepository.findOneByUuid(lessonUuid);
        if (lesson == null || (lesson.isForSell() && isAnonymous(principal))) {
            log.warn("{} want to access {} lesson {}", anonymous(principal), lesson, learnDir);
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).build();
        }
        List<Question> questions = questionService.findByLessonAndLevelOrderByIndexOrderAsc(lesson, difficulty);
        List<LessonWord> lwords = lessonWordRepository.findByLessonOrderByIndexOrderAsc(lesson);
        return ResponseEntity.ok(new LessonQuestionDTO(QuestionPublicDTO.toDTO(questions), lwords, learnDir));
    }

    @GetMapping("/words/{learnDir}/{lessonUuid}")
    @Timed
    public ResponseEntity<LessonWordPublicDTO> getWordsPubicList(
            @PathVariable("learnDir") LearnDir learnDir,
            @PathVariable("lessonUuid") String lessonUuid, Principal principal) {
        log.debug("Rest request to get list of words for {} with {}", learnDir, lessonUuid);
        if (learnDir == null || lessonUuid == null || StringUtils.isEmpty(lessonUuid)) {
            return ResponseEntity.badRequest().build();
        }
        Lesson lesson = lessonRepository.findOneByUuid(lessonUuid);
        if (lesson == null || (lesson.isForSell() && isAnonymous(principal))) {
            log.warn("{} want to access {} lesson {}", anonymous(principal), lesson, learnDir);
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).build();
        }
        List<Question> questions = questionService.findByLessonAndLevelOrderByIndexOrderAsc(lesson, lesson.getLevel());
        List<LessonCommWord> mwords = lessonCommWordRepository.findByLessonIdAndLanguage(lesson.getId(), learnDir.mother());
        List<LessonCommWord> twords = lessonCommWordRepository.findByLessonIdAndLanguage(lesson.getId(), learnDir.target());
        return ResponseEntity.ok(new LessonWordPublicDTO(QuestionPublicDTO.toDTO(questions), mwords, learnDir, twords, anonymous(principal)));
    }

    @GetMapping("/words/{language}/percentage")
    @Timed
    public ResponseEntity<Integer> completedPercentageFor(@PathVariable("language") Language language) {
        Long total = commWordRepository.countDistinctWordIdByLanguage(language);
        int percentage = total == 0l ? 0 : (total.intValue() * 100) / this.englishWordsTotal;
        log.info("{} percentage completed for {} ({} * 100) / {}", percentage, language, total, this.englishWordsTotal);
        return ResponseEntity.ok(percentage);
    }

    class LessonWordPublicDTO {
        private final List<QuestionPublicDTO> questions;
        private final Map<String, CommWordPublicDTO> words;

        public LessonWordPublicDTO(List<QuestionPublicDTO> questions, List<LessonCommWord> mwords,
                                   LearnDir learnDir, List<LessonCommWord> twords, String author) {
            this.questions = questions;
            this.words = new HashMap<String, CommWordPublicDTO>();
            for (LessonCommWord lcw: mwords) {
                if (words.containsKey(lcw.iorder())) {
                    words.get(lcw.iorder()).addCommWordToMother(lcw, author);
                } else {
                    words.put(lcw.iorder(), new CommWordPublicDTO(lcw, false, author));
                }
            }
            for (LessonCommWord lcw: twords) {
                if (words.containsKey(lcw.iorder())) {
                    words.get(lcw.iorder()).addCommWordToTarget(lcw, author);
                } else {
                    words.put(lcw.iorder(), new CommWordPublicDTO(lcw, true, author));
                }
            }
        }

        public List<QuestionPublicDTO> getQuestions() {
            return questions;
        }

        public Map<String, CommWordPublicDTO> getWords() {
            return words;
        }

    }

    class LessonQuestionDTO {

        private final List<QuestionPublicDTO> questions;
        private final Map<String, WordPublicDTO> words;

        public LessonQuestionDTO (List<QuestionPublicDTO> questions, List<LessonWord> lwords, LearnDir learnDir) {
            this.questions = questions;
            this.words = new HashMap<String, WordPublicDTO>();
            for (LessonWord lw: lwords) {
                this.words.put(lw.getIndexOrder().toString(), new WordPublicDTO(lw, learnDir));
            }
        }

        public List<QuestionPublicDTO> getQuestions() {
            return questions;
        }

        public Map<String, WordPublicDTO> getWords() {
            return words;
        }

    }

    private final Logger log = LoggerFactory.getLogger(getClass());

}
