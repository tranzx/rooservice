package mars.roo.web.rest.question;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import mars.roo.domain.LessonWord;
import mars.roo.domain.enumeration.LearnDir;
import mars.roo.domain.enumeration.WordType;

@JsonInclude(Include.NON_NULL)
public class WordPublicDTO {

    private final String t;     // target lookup
    private final String m;     // mother lookup
    private final String e;     // English lookup, useful when has picture.
    private final Integer c;    // Capitalisation
    private final Integer b;    // Background
    private final Integer v;    // WordType#Verb
    private final Integer p;    // Has picture

    public WordPublicDTO(LessonWord lw, LearnDir learnDir) {
        t = lw.getWord().value(learnDir.target());
        m = lw.getWord().value(learnDir.mother());
        c = lw.isCapitalization() != null && lw.isCapitalization() ? 1 : null;
        b = lw.isBackground() != null && lw.isBackground() ? 1 : null;
        v = lw.getWord().getWordType().equals(WordType.Verb) ? 1 : null;
        p = lw.isPicture() != null && lw.isPicture() ? 1 : null;
        e = null == p ? null : lw.getWord().getEnGb();
    }

    public String getT() {
        return t;
    }

    public String getM() {
        return m;
    }

    public Integer getC() {
        return c;
    }

    public Integer getB() {
        return b;
    }

    public Integer getV() {
        return v;
    }

    public Integer getP() {
        return p;
    }

    public String getE() {
        return e;
    }

    @Override
    public String toString() {
        return String.format("{\"t\":\"%s\", \"m\":\"%s\", \"c\":\"%s\"}", t, m, c);
    }

}
