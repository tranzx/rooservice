package mars.roo.web.rest.question;

import java.util.Objects;
import java.util.TreeSet;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import mars.roo.domain.LessonCommWord;
import mars.roo.domain.enumeration.WordType;

@JsonInclude(Include.NON_NULL)
public class CommWordPublicDTO {

    private final String u;         // uuid
    private final TreeSet<CWord> ts;// target language
    private final TreeSet<CWord> ms;// mother language
    private String e;               // English language, useful when has picture
    private final Integer v;        // WordType#Verb
    private final Integer p;        // Has picture
    private final Integer c;        // Capitialization
    private final Integer b;        // Backgroud word

    public CommWordPublicDTO(LessonCommWord lcw, boolean isTarget, String author) {
        u = lcw.getWordUuid();
        CommWordComparator comparator = new CommWordComparator();
        if (isTarget) {
            ts = new TreeSet<CWord>(comparator);
            ts.add(new CWord(lcw, author));
            ms = new TreeSet<CWord>(comparator);
        } else {
            ts = new TreeSet<CWord>(comparator);
            ms = new TreeSet<CWord>(comparator);
            ms.add(new CWord(lcw, author));
        }
        p = lcw.isPicture() != null && lcw.isPicture() ? 1 : null;
        e = lcw.isBase() != null && lcw.isBase() && p != null ? lcw.getTextEng() : null;
        c = lcw.isCapitalization() != null && lcw.isCapitalization() ? 1 : null;
        b = lcw.isBackground() != null && lcw.isBackground() ? 1 : null;
        v = WordType.Verb.equals(lcw.getWordType()) ? 1 : null;
    }

    public void addCommWordToTarget(LessonCommWord lcw, String author) {
        ts.add(new CWord(lcw, author));
        if (lcw.isBase() != null && lcw.isBase()) {
            e = lcw.getTextEng();
        }
    }

    public void addCommWordToMother(LessonCommWord lcw, String author) {
        ms.add(new CWord(lcw, author));
        if (lcw.isBase() != null && lcw.isBase()) {
            e = lcw.getTextEng();
        }
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getU() {
        return u;
    }

    public TreeSet<CWord> getTs() {
        return ts;
    }

    public TreeSet<CWord> getMs() {
        return ms;
    }

    public Integer getV() {
        return v;
    }

    public Integer getP() {
        return p;
    }

    public Integer getC() {
        return c;
    }

    public Integer getB() {
        return b;
    }

    @JsonInclude(Include.NON_NULL)
    public static class CWord {
        private final String u;     // uuid
        private final String w;     // text
        private final Integer v;    // vote
        private final Integer b;    // base
        private final Integer a;    // author
        public CWord(LessonCommWord lcw, String author) {
            u = lcw.getCommWordUuid();
            w = lcw.getText();
            v = lcw.getVote();
            b = lcw.isBase() != null && lcw.isBase() ? 1 : null;
            a = author != null && author.equalsIgnoreCase(lcw.getAuthor()) ? 1 : null;
        }
        public CWord(String word, Integer author, Integer base, Integer vote, String uuid) {
            this.w = word;
            this.v = vote;
            this.b = base;
            this.a = author;
            this.u = uuid;
        }
        public String getU() {
            return u;
        }
        public String getW() {
            return w;
        }
        public Integer getV() {
            return v;
        }
        public Integer getB() {
            return b;
        }
        public Integer getA() {
            return a;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            CWord cword = (CWord) obj;
            return Objects.equals(getU(), cword.getU());
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(getU());
        }
        @Override
        public String toString() {
            return getU();
        }
    }

}
