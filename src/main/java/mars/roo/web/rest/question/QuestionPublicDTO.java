package mars.roo.web.rest.question;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import mars.roo.domain.Question;
import mars.roo.domain.enumeration.QuestionType;

/**
 * @author developer
 *
 */
@JsonInclude(Include.NON_NULL)
public final class QuestionPublicDTO implements Comparable<QuestionPublicDTO> {

    private final String uuid;
    private final QuestionType type;
    private final Integer indexOrder;
    private final String dynamicPart;

    public QuestionPublicDTO(Question question) {
        uuid = question.getUuid();
        type = question.getType();
        indexOrder = question.getIndexOrder();
        dynamicPart = question.getDynamicPart();
    }

    public static List<QuestionPublicDTO> toDTO(List<Question> questions) {
        List<QuestionPublicDTO> result = new ArrayList<QuestionPublicDTO>();
        for (Question q: questions) {
            result.add(new QuestionPublicDTO(q));
        }
        return result;
    }

    @Override
    public int compareTo(QuestionPublicDTO other) {
        return indexOrder.compareTo(other.indexOrder);
    }

    // Getters.

    public String getUuid() {
        return uuid;
    }

    public QuestionType getType() {
        return type;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public String getDynamicPart() {
        return dynamicPart;
    }

}
