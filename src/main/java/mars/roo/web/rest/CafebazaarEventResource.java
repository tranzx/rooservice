package mars.roo.web.rest;

import com.codahale.metrics.annotation.Timed;
import mars.roo.domain.CafebazaarEvent;

import mars.roo.repository.CafebazaarEventRepository;
import mars.roo.web.rest.errors.BadRequestAlertException;
import mars.roo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CafebazaarEvent.
 */
@RestController
@RequestMapping("/api")
public class CafebazaarEventResource {

    private final Logger log = LoggerFactory.getLogger(CafebazaarEventResource.class);

    private static final String ENTITY_NAME = "cafebazaarEvent";

    private final CafebazaarEventRepository cafebazaarEventRepository;

    public CafebazaarEventResource(CafebazaarEventRepository cafebazaarEventRepository) {
        this.cafebazaarEventRepository = cafebazaarEventRepository;
    }

    /**
     * POST  /cafebazaar-events : Create a new cafebazaarEvent.
     *
     * @param cafebazaarEvent the cafebazaarEvent to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cafebazaarEvent, or with status 400 (Bad Request) if the cafebazaarEvent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cafebazaar-events")
    @Timed
    public ResponseEntity<CafebazaarEvent> createCafebazaarEvent(@Valid @RequestBody CafebazaarEvent cafebazaarEvent) throws URISyntaxException {
        log.debug("REST request to save CafebazaarEvent : {}", cafebazaarEvent);
        if (cafebazaarEvent.getId() != null) {
            throw new BadRequestAlertException("A new cafebazaarEvent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CafebazaarEvent result = cafebazaarEventRepository.save(cafebazaarEvent);
        return ResponseEntity.created(new URI("/api/cafebazaar-events/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cafebazaar-events : Updates an existing cafebazaarEvent.
     *
     * @param cafebazaarEvent the cafebazaarEvent to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cafebazaarEvent,
     * or with status 400 (Bad Request) if the cafebazaarEvent is not valid,
     * or with status 500 (Internal Server Error) if the cafebazaarEvent couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cafebazaar-events")
    @Timed
    public ResponseEntity<CafebazaarEvent> updateCafebazaarEvent(@Valid @RequestBody CafebazaarEvent cafebazaarEvent) throws URISyntaxException {
        log.debug("REST request to update CafebazaarEvent : {}", cafebazaarEvent);
        if (cafebazaarEvent.getId() == null) {
            return createCafebazaarEvent(cafebazaarEvent);
        }
        CafebazaarEvent result = cafebazaarEventRepository.save(cafebazaarEvent);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cafebazaarEvent.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cafebazaar-events : get all the cafebazaarEvents.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cafebazaarEvents in body
     */
    @GetMapping("/cafebazaar-events")
    @Timed
    public List<CafebazaarEvent> getAllCafebazaarEvents() {
        log.debug("REST request to get all CafebazaarEvents");
        return cafebazaarEventRepository.findAll();
        }

    /**
     * GET  /cafebazaar-events/:id : get the "id" cafebazaarEvent.
     *
     * @param id the id of the cafebazaarEvent to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cafebazaarEvent, or with status 404 (Not Found)
     */
    @GetMapping("/cafebazaar-events/{id}")
    @Timed
    public ResponseEntity<CafebazaarEvent> getCafebazaarEvent(@PathVariable Long id) {
        log.debug("REST request to get CafebazaarEvent : {}", id);
        CafebazaarEvent cafebazaarEvent = cafebazaarEventRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cafebazaarEvent));
    }

    /**
     * DELETE  /cafebazaar-events/:id : delete the "id" cafebazaarEvent.
     *
     * @param id the id of the cafebazaarEvent to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cafebazaar-events/{id}")
    @Timed
    public ResponseEntity<Void> deleteCafebazaarEvent(@PathVariable Long id) {
        log.debug("REST request to delete CafebazaarEvent : {}", id);
        cafebazaarEventRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
