/**
 * View Models used by Spring MVC REST controllers.
 */
package mars.roo.web.rest.vm;
