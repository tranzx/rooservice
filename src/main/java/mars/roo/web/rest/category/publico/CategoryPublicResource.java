package mars.roo.web.rest.category.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.roo.domain.Category;
import mars.roo.domain.enumeration.LearnDir;
import mars.roo.repository.CategoryRepository;

@RestController
@RequestMapping("/api/public")
public class CategoryPublicResource {

    private final CategoryRepository categoryRepository;
    private final static Map<LearnDir, List<CategoryPublicDTO>> categoryListIdentityMap = new HashMap<LearnDir, List<CategoryPublicDTO>>();

    public CategoryPublicResource(CategoryRepository categoryTranslRepository) {
        this.categoryRepository = categoryTranslRepository;
    }

    @GetMapping("/categories/{learnDir}")
    @Timed
    public ResponseEntity<List<CategoryPublicDTO>> getAllCategoriesPublic(@PathVariable("learnDir") LearnDir learnDir) {
        log.info("REST request to get list of public Categories");
        if (!categoryListIdentityMap.containsKey(learnDir)) {
            Sort sort = new Sort(Direction.ASC, "indexOrder");
            PageRequest request = new PageRequest(0, 40, sort);
            List<Category> list = categoryRepository.findAll(request).getContent();
            categoryListIdentityMap.put(learnDir, CategoryPublicDTO.toDTO(list, learnDir));
        }
        return new ResponseEntity<>(categoryListIdentityMap.get(learnDir), HttpStatus.OK);
    }

    @GetMapping("/categories/refresh")
    @Timed
    public ResponseEntity<Void> refreshCachedCategoryListIdentitiyMap() {
        categoryListIdentityMap.clear();
        return ResponseEntity.ok().build();
    }

    private final Logger log = LoggerFactory.getLogger(getClass());

}
