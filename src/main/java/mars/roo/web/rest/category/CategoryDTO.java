package mars.roo.web.rest.category;

import java.util.ArrayList;
import java.util.List;

import mars.roo.domain.Category;

/**
 * CategoryDTO for data-entry usage
 */
public final class CategoryDTO {

    private final Long id;
    private final String uuid;
    private final String titleEng;
    private final String titleFa;
    private final String titleDe;
    private final Integer indexOrder;
    private final Boolean forSell;
    private final Boolean cameNew;
    private final Boolean commingSoon;

    public CategoryDTO(Category category) {
        id = category.getId();
        uuid = category.getUuid();
        titleEng = category.getTitleEng();
        indexOrder = category.getIndexOrder();
        forSell = category.isForSell();
        cameNew = category.isCameNew();
        commingSoon = category.isCommingSoon();
        titleFa = category.getTitleFa();
        titleDe = category.getTitleDe();
    }

    public static List<CategoryDTO> toDTO(List<Category> entities) {
        List<CategoryDTO> result = new ArrayList<CategoryDTO>();
        for (Category entity: entities) {
            result.add(new CategoryDTO(entity));
        }
        return result;
    }

    public Long getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public String getTitleEng() {
        return titleEng;
    }

    public String getTitleFa() {
        return titleFa;
    }

    public String getTitleDe() {
        return titleDe;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public Boolean getForSell() {
        return forSell;
    }

    public Boolean getCameNew() {
        return cameNew;
    }

    public Boolean getCommingSoon() {
        return commingSoon;
    }

}
