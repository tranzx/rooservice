package mars.roo.web.rest.category;

import java.util.ArrayList;
import java.util.List;

import mars.roo.domain.Category;

public final class CategoryLightDTO {

    private final Long id;
    private final String titleEng;

    public CategoryLightDTO(Category category) {
        this.id = category.getId();
        this.titleEng = category.getTitleEng();
    }

    public Long getId() {
        return id;
    }

    public String getTitleEng() {
        return titleEng;
    }

    public static List<CategoryLightDTO> toDTO(List<Category> list) {
        List<CategoryLightDTO> result = new ArrayList<CategoryLightDTO>();
        list.stream().forEach(category -> {
            result.add(new CategoryLightDTO(category));
        });
        return result;
    }

}
