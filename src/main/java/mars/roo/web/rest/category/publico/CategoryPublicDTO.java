package mars.roo.web.rest.category.publico;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import mars.roo.domain.Category;
import mars.roo.domain.enumeration.Language;
import mars.roo.domain.enumeration.LearnDir;

/**
 * CategoryDTO for transfer to clients
 */
public final class CategoryPublicDTO implements Comparable<CategoryPublicDTO>, Comparator<CategoryPublicDTO> {

    private final String uuid;
    private final String title;
    private final Integer indexOrder;
    private final Boolean forSell;
    private final Boolean cameNew;
    private final Boolean commingSoon;

    private final static List<Integer> deutshNotReadyYetList = Arrays.asList();

    public CategoryPublicDTO(Category category, LearnDir learnDir) {
        uuid = category.getUuid();
        title = category.title(learnDir);
        indexOrder = category.getIndexOrder();
        forSell = category.isForSell();
        cameNew = learnDirConsiderationForDeutsh(category, learnDir, category.isCameNew(), false);
        commingSoon = learnDirConsiderationForDeutsh(category, learnDir, category.isCommingSoon(), true);
    }

    public static List<CategoryPublicDTO> toDTO(List<Category> entities, LearnDir learnDir) {
        List<CategoryPublicDTO> result = new ArrayList<CategoryPublicDTO>();
        for (Category entity: entities) {
            result.add(new CategoryPublicDTO(entity, learnDir));
        }
//        Collections.sort(result, new Comparator<CategoryPublicDTO>() {
//            @Override
//            public int compare(CategoryPublicDTO c1, CategoryPublicDTO c2) {
//                return c1.compareTo(c2);
//            }
//        });
        return result;
    }

    public Boolean learnDirConsiderationForDeutsh(Category category, LearnDir learnDir, boolean defaultValue, boolean alternativeValue) {
        if ( Language.DE_DE.equals(learnDir.target())) {
            if (deutshNotReadyYetList.contains(category.getIndexOrder())) {
                return alternativeValue;
            }
        }
        return defaultValue;
    }

    @Override
    public int compareTo(CategoryPublicDTO other) {
        return indexOrder.compareTo(other.indexOrder);
    }

    @Override
    public int compare(CategoryPublicDTO c1, CategoryPublicDTO c2) {
        return c1.compareTo(c2);
    }

    public String getUuid() {
        return uuid;
    }

    public String getTitle() {
        return title;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public Boolean getForSell() {
        return forSell;
    }

    public Boolean getCommingSoon() {
        return commingSoon;
    }

    public Boolean getCameNew() {
        return cameNew;
    }

}
