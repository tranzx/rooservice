package mars.roo.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import mars.roo.domain.Lesson;
import mars.roo.domain.Question;
import mars.roo.domain.enumeration.Level;
import mars.roo.service.LessonService;
import mars.roo.service.QuestionQueryService;
import mars.roo.service.QuestionService;
import mars.roo.service.dto.QuestionCriteria;
import mars.roo.web.rest.errors.BadRequestAlertException;
import mars.roo.web.rest.util.HeaderUtil;
import mars.roo.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Question.
 */
@RestController
@RequestMapping("/api")
public class QuestionResource {

    private final Logger log = LoggerFactory.getLogger(QuestionResource.class);

    private static final String ENTITY_NAME = "question";

    private final QuestionService questionService;

    private final QuestionQueryService questionQueryService;

    private final LessonService lessonService;

    public QuestionResource(QuestionService questionService, QuestionQueryService questionQueryService,
            LessonService lessonService) {
        this.questionService = questionService;
        this.questionQueryService = questionQueryService;
        this.lessonService = lessonService;
    }

    /**
     * POST  /questions : Create a new question.
     *
     * @param question the question to create
     * @return the ResponseEntity with status 201 (Created) and with body the new question, or with status 400 (Bad Request) if the question has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/questions")
    @Timed
    public ResponseEntity<Question> createQuestion(@Valid @RequestBody Question question) throws URISyntaxException {
        log.debug("REST request to save Question : {}", question);
        if (question.getId() != null) {
            throw new BadRequestAlertException("A new question cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Question result = questionService.save(question);
        return ResponseEntity.created(new URI("/api/questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /questions : Updates an existing question.
     *
     * @param question the question to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated question,
     * or with status 400 (Bad Request) if the question is not valid,
     * or with status 500 (Internal Server Error) if the question couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/questions")
    @Timed
    public ResponseEntity<Question> updateQuestion(@Valid @RequestBody Question question) throws URISyntaxException {
        log.debug("REST request to update Question : {}", question);
        if (question.getId() == null) {
            return createQuestion(question);
        }
        Question result = questionService.save(question);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, question.getId().toString()))
            .body(result);
    }

    @GetMapping("/questions/duplicate/{id}/{toIndex}/{level}")
    @Timed
    public ResponseEntity<Void> duplicateQuestion(
            @PathVariable("id") Long id,
            @PathVariable("toIndex") Integer toIndex,
            @PathVariable("level") Level level) {
        Question question = questionService.findOne(id);
        Lesson lesson = lessonService.findOneByCategoryAndLevelAndIndexOrder(question.getLesson().getCategory(), level, question.getLesson().getIndexOrder());
        if (lesson == null) {   // oh we need to duplicate lesson too
            lesson = question.getLesson();
            lesson.setId(null);
            lesson.setLevel(level);
            lesson.setUuid(UUID.randomUUID().toString());
        }
        question.setLesson(lesson);
        question.setId(null);
        question.setLevel(level);
        question.setIndexOrder(toIndex);
        question.setUuid(UUID.randomUUID().toString());
        questionService.save(question);
        return ResponseEntity.ok().build();
    }

    /**
     * GET  /questions : get all the questions.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of questions in body
     */
    @GetMapping("/questions")
    @Timed
    public ResponseEntity<List<QuestionDTO>> getAllQuestions(QuestionCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Questions by criteria: {}", criteria);
        Page<Question> page = questionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/questions");
        return new ResponseEntity<>(QuestionDTO.toDTO(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /questions : get all the questions.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the count of questions in body
     */
    @GetMapping("/questions/count/of")
    @Timed
    public ResponseEntity<Long> getAllQuestionsCount(QuestionCriteria criteria) {
        log.debug("REST request to get Questions count by criteria: {}", criteria);
        return ResponseEntity.ok(questionQueryService.countByCriteria(criteria));
    }


    /**
     * GET  /questions/:id : get the "id" question.
     *
     * @param id the id of the question to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the question, or with status 404 (Not Found)
     */
    @GetMapping("/questions/{id}")
    @Timed
    public ResponseEntity<Question> getQuestion(@PathVariable Long id) {
        log.debug("REST request to get Question : {}", id);
        Question question = questionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(question));
    }

    /**
     * DELETE  /questions/:id : delete the "id" question.
     *
     * @param id the id of the question to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/questions/{id}")
    @Timed
    public ResponseEntity<Void> deleteQuestion(@PathVariable Long id) {
        log.debug("REST request to delete Question : {}", id);
        questionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
