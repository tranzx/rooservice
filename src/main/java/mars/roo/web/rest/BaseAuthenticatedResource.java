package mars.roo.web.rest;

import java.security.Principal;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import io.github.jhipster.service.filter.StringFilter;
import mars.roo.web.rest.errors.InternalServerErrorException;

public class BaseAuthenticatedResource {

    protected String login(Principal principal) {
        return Optional.ofNullable(principal)
                .filter(it -> it instanceof OAuth2Authentication)
                .map(it -> ((OAuth2Authentication) it).getUserAuthentication())
                .map(authentication -> {
                    return authentication.getName();
                })
                .orElseThrow(() -> new InternalServerErrorException("User could not be found"));
    }

    protected String anonymous(Principal principal) {
        return Optional.ofNullable(principal)
                .filter(it -> it instanceof OAuth2Authentication)
                .map(it -> ((OAuth2Authentication) it).getUserAuthentication())
                .map(authentication -> {
                    return authentication.getName();
                })
                .orElse(null);
    }

    protected boolean isAnonymous(Principal principal) {
        return anonymous(principal) == null;
    }

    protected StringFilter getLoginFilter(Principal principal) {
        StringFilter filter = new StringFilter();
        filter.setEquals(login(principal));
        return filter;
    }

    protected boolean hasRole(String roleName, Principal principal) {
        return Optional.ofNullable(principal)
                .filter(it -> it instanceof OAuth2Authentication).map(it -> ((OAuth2Authentication) it).getAuthorities()
                        .stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(roleName)))
                .orElse(false);
    }
}
