package mars.roo.web.rest.score;

import java.time.LocalDate;
import java.time.ZoneOffset;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import mars.roo.domain.Score;
import mars.roo.domain.enumeration.LearnDir;
import mars.roo.domain.enumeration.Level;
import mars.roo.domain.enumeration.ScoreType;

public class ScoreCommand {

    @NotNull
    LearnDir learnDir;
    @NotNull
    @Min(1)
    @Max(10)
    Integer score;
    @NotNull
    @Min(0) // TODO supposed to be 1 but client-side code doesn't handle that yet.
    @Max(5)
    Integer star;
    @NotNull
    ScoreType type;
    @NotNull
    String lessonUuid;
    String categoryUuid;
    Level difficLevel;

    public Score toEntity(String login) {
        return new Score()
                .owner(login)
                .learnDir(learnDir)
                .target(learnDir.target())
                .type(type)
                .scoreDate(LocalDate.now(ZoneOffset.UTC))
                .score(score)
                .star(star)
                .lessonUuid(lessonUuid)
                .categoryUuid(categoryUuid)
                .difficultyLevel(difficLevel);
    }

    public LearnDir getLearnDir() {
        return learnDir;
    }

    public void setLearnDir(LearnDir learnDir) {
        this.learnDir = learnDir;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public ScoreType getType() {
        return type;
    }

    public void setType(ScoreType type) {
        this.type = type;
    }

    public String getLessonUuid() {
        return lessonUuid;
    }

    public void setLessonUuid(String lessonUuid) {
        this.lessonUuid = lessonUuid;
    }

    public String getCategoryUuid() {
        return categoryUuid;
    }

    public void setCategoryUuid(String categoryUuid) {
        this.categoryUuid = categoryUuid;
    }

    public Level getDifficLevel() {
        return difficLevel;
    }

    public void setDifficLevel(Level difficLevel) {
        this.difficLevel = difficLevel;
    }

}
