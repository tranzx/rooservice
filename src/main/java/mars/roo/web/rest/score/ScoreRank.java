package mars.roo.web.rest.score;

import java.util.ArrayList;
import java.util.List;

import mars.roo.service.ProfileService;

public class ScoreRank {

    final List<TopMember> topMembers;

    ScoreRank(List<Object[]> rowMembers, ProfileService profileService) {
        this.topMembers = new ArrayList<TopMember>();
        int counter = 1;
        for (Object[] row: rowMembers) {
            topMembers.add(new TopMember(profileService.getDname(row[0].toString()), integer(row[1]), row[0].toString(), counter++));
        }
    }

    public List<TopMember> getTopMembers() {
        return topMembers;
    }

    public boolean contians(String owner) {
        for (TopMember tm: topMembers) {
            if (owner.equalsIgnoreCase(tm.owner())) {
                return true;
            }
        }
        return false;
    }

    public void add(Object[] orow, String dname) {
        TopMember member = new TopMember(dname, integer(orow[1]), orow[0].toString(), integer(orow[2]));
        topMembers.add(member);
    }

    class TopMember {
        private final String dname;
        private final Integer total;
        private final Integer rank;
        private final String owner;

        TopMember(String dname, Integer total, String owner, Integer rank) {
            this.dname = dname;
            this.total = total;
            this.owner = owner;
            this.rank = rank;
        }

        public String getDname() {
            return dname;
        }

        public Integer getTotal() {
            return total;
        }

        public Integer getRank() {
            return rank;
        }

        public String owner() {
            return owner;
        }

    }

    public Integer integer(Object obj) {
        return ((Number) obj).intValue();
    }

}
