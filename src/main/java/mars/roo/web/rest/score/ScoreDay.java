package mars.roo.web.rest.score;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class ScoreDay {

    private final LocalDate day;
    private final Integer sum;
    private final DayOfWeek dayOfWeek;

    public ScoreDay(Object[] row) {
        sum = ((Number) row[0]).intValue();
        day = LocalDate.parse(row[1].toString());
        dayOfWeek = day.getDayOfWeek();
    }

    public LocalDate getDay() {
        return day;
    }

    public Integer getSum() {
        return sum;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

}
