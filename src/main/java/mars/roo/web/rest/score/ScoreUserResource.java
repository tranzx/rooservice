package mars.roo.web.rest.score;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.roo.domain.enumeration.LearnDir;
import mars.roo.domain.enumeration.Level;
import mars.roo.repository.ScoreRepository;
import mars.roo.service.ProfileService;
import mars.roo.web.rest.BaseAuthenticatedResource;

@RestController
@RequestMapping("/api/user/score")
public class ScoreUserResource extends BaseAuthenticatedResource {

    private final ScoreRepository scoreRepository;
    private final ProfileService profileService;

    public ScoreUserResource(ScoreRepository scoreRepository, ProfileService profileService) {
        this.scoreRepository = scoreRepository;
        this.profileService = profileService;
    }

    @PostMapping("/create")
    @Timed
    public ResponseEntity<ScoreCommand> createScore(@Valid @RequestBody ScoreCommand dto, Principal principal) throws URISyntaxException {
        log.info("An attempt for store score by {} at {}", login(principal), LocalDate.now(ZoneOffset.UTC));
        scoreRepository.save(dto.toEntity(login(principal)));
        return ResponseEntity.created(new URI("/api/user/score")).body(dto);
    }

    @GetMapping("/lookup/{learnDir}/{level}")
    @Timed
    public ResponseEntity<ScoreLookup> getLookup(Principal principal,
            @PathVariable("learnDir") LearnDir learnDir,
            @PathVariable("level") Level level) {
        Integer total = scoreRepository.queryTotalScore(login(principal), learnDir.name());
        List<Object[]> doneLessons = scoreRepository.doneLessonsPerCategory(login(principal), learnDir.name(), level.name());
        List<Object[]> maxStars = scoreRepository.maxStarPerLesson(login(principal), learnDir.name(), level.name());
        ScoreLookup sl = new ScoreLookup(learnDir, level, total, doneLessons, maxStars);
        return ResponseEntity.ok().body(sl);
    }

    @GetMapping("/last7/{learnDir}")
    @Timed
    public ResponseEntity<List<ScoreDay>> getLast7daysScores(Principal principal,
            @PathVariable("learnDir") LearnDir learnDir) {
        LocalDate today = LocalDate.now(ZoneOffset.UTC);
        List<Object[]> scores = scoreRepository.scoreBetween(login(principal), learnDir.name(),
                today.minusDays(6).toString(), today.plusDays(1).toString());
        List<ScoreDay> res = new ArrayList<ScoreDay>();
        for (Object[] row: scores) {
            res.add(new ScoreDay(row));
        }
        return ResponseEntity.ok(res);
    }

    @GetMapping("/top3/ever/{learnDir}")
    @Timed
    public ResponseEntity<ScoreRank> topMembers(Principal principal,
            @PathVariable("learnDir") LearnDir learnDir) {
        List<Object[]> topMembers = scoreRepository.topScores2(3, learnDir.target().name());
        ScoreRank rank = new ScoreRank(topMembers, profileService);
        if (!rank.contians(login(principal))) {
            List<Object[]> orow = scoreRepository.rankFor2(login(principal), learnDir.target().name());
            if (orow.size() > 0) {
                rank.add(orow.get(0), profileService.getDname(login(principal)));
            }
        }
        return ResponseEntity.ok(rank);
    }

    @GetMapping("/top3/month/{learnDir}")
    @Timed
    public ResponseEntity<ScoreRank> topMembersOfMonth(Principal principal,
            @PathVariable("learnDir") LearnDir learnDir) {
        List<Object[]> topMembers = scoreRepository.topScoresMonth2(3, learnDir.target().name());
        ScoreRank rank = new ScoreRank(topMembers, profileService);
        if (!rank.contians(login(principal))) {
            List<Object[]> orow = scoreRepository.rankForMonth2(login(principal), learnDir.target().name());
            if (orow.size() > 0) {
                rank.add(orow.get(0), profileService.getDname(orow.get(0)[0].toString()));
            }
        }
        return ResponseEntity.ok(rank);
    }

    private Logger log = LoggerFactory.getLogger(getClass());

}
