package mars.roo.web.rest.score;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mars.roo.domain.enumeration.LearnDir;
import mars.roo.domain.enumeration.Level;

/**
 * @author developer
 *
 */
public class ScoreLookup {

    final LearnDir learnDir;
    final Level difficultyLevel;
    final Integer total;
    final Map<String, Integer> categoryMap;
    final Map<String, Integer> lessonMap;

    public ScoreLookup(LearnDir ld, Level difLevel, Integer total, List<Object[]> doneLessons, List<Object[]> maxStars) {
        learnDir = ld;
        difficultyLevel = difLevel;
        this.total = total;
        categoryMap = new HashMap<String, Integer>();
        if (doneLessons != null && doneLessons.size() > 0) {
            doneLessons.forEach(row -> {
                Number count = (Number) row[1];
                if (count != null) {
                    categoryMap.put((String) row[0], count.intValue());
                }
            });
        }
        lessonMap = new HashMap<String, Integer>();
        if (maxStars != null && maxStars.size() > 0) {
            maxStars.forEach(row -> {
                Number stars = (Number) row[1];
                if (stars != null) {
                    lessonMap.put((String) row[0], stars.intValue());
                }
            });
        }
    }

    public LearnDir getLearnDir() {
        return learnDir;
    }

    public Level getDifficultyLevel() {
        return difficultyLevel;
    }

    public Integer getTotal() {
        return total;
    }

    public Map<String, Integer> getCategoryMap() {
        return categoryMap;
    }

    public Map<String, Integer> getLessonMap() {
        return lessonMap;
    }

}
