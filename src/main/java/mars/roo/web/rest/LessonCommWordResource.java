package mars.roo.web.rest;

import com.codahale.metrics.annotation.Timed;
import mars.roo.domain.LessonCommWord;

import mars.roo.repository.LessonCommWordRepository;
import mars.roo.web.rest.errors.BadRequestAlertException;
import mars.roo.web.rest.util.HeaderUtil;
import mars.roo.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LessonCommWord.
 */
@RestController
@RequestMapping("/api")
public class LessonCommWordResource {

    private final Logger log = LoggerFactory.getLogger(LessonCommWordResource.class);

    private static final String ENTITY_NAME = "lessonCommWord";

    private final LessonCommWordRepository lessonCommWordRepository;

    public LessonCommWordResource(LessonCommWordRepository lessonCommWordRepository) {
        this.lessonCommWordRepository = lessonCommWordRepository;
    }

    /**
     * POST  /lesson-comm-words : Create a new lessonCommWord.
     *
     * @param lessonCommWord the lessonCommWord to create
     * @return the ResponseEntity with status 201 (Created) and with body the new lessonCommWord, or with status 400 (Bad Request) if the lessonCommWord has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/lesson-comm-words")
    @Timed
    public ResponseEntity<LessonCommWord> createLessonCommWord(@Valid @RequestBody LessonCommWord lessonCommWord) throws URISyntaxException {
        log.debug("REST request to save LessonCommWord : {}", lessonCommWord);
        if (lessonCommWord.getId() != null) {
            throw new BadRequestAlertException("A new lessonCommWord cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LessonCommWord result = lessonCommWordRepository.save(lessonCommWord);
        return ResponseEntity.created(new URI("/api/lesson-comm-words/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /lesson-comm-words : Updates an existing lessonCommWord.
     *
     * @param lessonCommWord the lessonCommWord to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lessonCommWord,
     * or with status 400 (Bad Request) if the lessonCommWord is not valid,
     * or with status 500 (Internal Server Error) if the lessonCommWord couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/lesson-comm-words")
    @Timed
    public ResponseEntity<LessonCommWord> updateLessonCommWord(@Valid @RequestBody LessonCommWord lessonCommWord) throws URISyntaxException {
        log.debug("REST request to update LessonCommWord : {}", lessonCommWord);
        if (lessonCommWord.getId() == null) {
            return createLessonCommWord(lessonCommWord);
        }
        LessonCommWord result = lessonCommWordRepository.save(lessonCommWord);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lessonCommWord.getId().toString()))
            .body(result);
    }

    /**
     * GET  /lesson-comm-words : get all the lessonCommWords.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of lessonCommWords in body
     */
    @GetMapping("/lesson-comm-words")
    @Timed
    public ResponseEntity<List<LessonCommWord>> getAllLessonCommWords(Pageable pageable) {
        log.debug("REST request to get a page of LessonCommWords");
        Page<LessonCommWord> page = lessonCommWordRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/lesson-comm-words");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /lesson-comm-words/:id : get the "id" lessonCommWord.
     *
     * @param id the id of the lessonCommWord to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the lessonCommWord, or with status 404 (Not Found)
     */
    @GetMapping("/lesson-comm-words/{id}")
    @Timed
    public ResponseEntity<LessonCommWord> getLessonCommWord(@PathVariable String id) {
        log.debug("REST request to get LessonCommWord : {}", id);
        LessonCommWord lessonCommWord = lessonCommWordRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lessonCommWord));
    }

    /**
     * DELETE  /lesson-comm-words/:id : delete the "id" lessonCommWord.
     *
     * @param id the id of the lessonCommWord to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/lesson-comm-words/{id}")
    @Timed
    public ResponseEntity<Void> deleteLessonCommWord(@PathVariable String id) {
        log.debug("REST request to delete LessonCommWord : {}", id);
        lessonCommWordRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
