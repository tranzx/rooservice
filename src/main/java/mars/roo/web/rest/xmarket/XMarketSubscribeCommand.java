package mars.roo.web.rest.xmarket;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import mars.roo.domain.enumeration.MarketType;
import mars.roo.domain.enumeration.SubscriptionType;

public class XMarketSubscribeCommand {

    String description;
    @NotNull
    SubscriptionType subscriptionType;
    String paymentApiReturnString;

    public XMarketReceipt parseXMarketReceipt(MarketType market) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        switch(market) {
            case cafebazaar:
                return mapper.readValue(paymentApiReturnString, CafebazaarReceipt.class);
            default:
                return mapper.readValue(paymentApiReturnString, XMarketReceipt.class);
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getPaymentApiReturnString() {
        return paymentApiReturnString;
    }

    public void setPaymentApiReturnString(String paymentApiReturnString) {
        this.paymentApiReturnString = paymentApiReturnString;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Deprecated
    public CafebazaarReceipt parseCafebazaarReceipt() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(paymentApiReturnString, CafebazaarReceipt.class);
    }

}
