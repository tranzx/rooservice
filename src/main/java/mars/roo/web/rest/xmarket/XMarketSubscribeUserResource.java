package mars.roo.web.rest.xmarket;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.roo.domain.CafebazaarEvent;
import mars.roo.domain.MarketEvent;
import mars.roo.domain.Membership;
import mars.roo.domain.enumeration.MarketType;
import mars.roo.infrastructure.cafebazaar.CafebazaarApiClient;
import mars.roo.infrastructure.cafebazaar.SubscriptionCheckDTO;
import mars.roo.repository.CafebazaarEventRepository;
import mars.roo.repository.MarketEventRepository;
import mars.roo.repository.MembershipRepository;
import mars.roo.web.rest.BaseAuthenticatedResource;

@RestController
@RequestMapping("/api/user")
public class XMarketSubscribeUserResource extends BaseAuthenticatedResource {

    private final CafebazaarEventRepository cafebazaarEventRepository;
    private final MembershipRepository membershipRepository;
    private final CafebazaarApiClient cafebazaarApiClient;
    private final MarketEventRepository marketEventRepository;

    public XMarketSubscribeUserResource(CafebazaarEventRepository cafebazaarEventRepository,
                                  MembershipRepository membershipRepository,
                                  CafebazaarApiClient cafebazaarApiClient,
                                  MarketEventRepository marketEventRepository) {
        this.cafebazaarEventRepository = cafebazaarEventRepository;
        this.membershipRepository = membershipRepository;
        this.cafebazaarApiClient = cafebazaarApiClient;
        this.marketEventRepository = marketEventRepository;
    }

    @PostMapping("/xmarket/subscribe/{market}")
    @Timed
    public ResponseEntity<Void> subscribe(@Valid @RequestBody XMarketSubscribeCommand cmd,
            @PathVariable("market") MarketType market, Principal principal) {
        if (isAnonymous(principal)) {
            return ResponseEntity.badRequest().build();
        }
        log.info("An attempt to subscribe (with {} inappbilling) for {} with {}", market, login(principal), cmd.toString());
        switch(market) {
            case cafebazaar:
                return subscribeWithCafebazaar(cmd, login(principal));
            default:
                return subscribeWithXMarket(cmd, login(principal), market);
        }
    }

    private ResponseEntity<Void> subscribeWithXMarket(XMarketSubscribeCommand cmd, String login, MarketType market) {
        try {
            XMarketReceipt receipt = cmd.parseXMarketReceipt(market);
            MarketEvent event = marketEventRepository.findOneByPurchaseToken(receipt.getPurchaseToken());
            if (event == null) {
                event = marketEventRepository.save(new MarketEvent(cmd, receipt, login));
            }
            Membership alreadyExist = findOneByMarketEvent(event);
            if (alreadyExist != null) {
                log.warn("{} Membership already exist!", event.getMarketType());
                if (alreadyExist.isExtensionNeeded(receipt)) {
                    membershipRepository.save(alreadyExist.extensionFor(event, receipt));
                    log.info("{} Membership extended!", event.getMarketType());
                }
                return ResponseEntity.ok().build();
            }
            if (receipt.getPurchaseState().equals(0)) { // 0 here means purchase was fine.
                Membership membership = new Membership(event);
                membershipRepository.save(membership);
                log.info("New membership {} created.", membership.toString());
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        } catch (Exception e) {
            log.error("{}_RECEIPT_PARSING_ERROR_NEEDS_ADMIN_ATTENTION!!!", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private ResponseEntity<Void> subscribeWithCafebazaar(XMarketSubscribeCommand cmd, String login) {
        try {
            CafebazaarReceipt receipt = (CafebazaarReceipt) cmd.parseXMarketReceipt(MarketType.cafebazaar);
            CafebazaarEvent event = cafebazaarEventRepository.findOneByPurchaseToken(receipt.getPurchaseToken());
            if (event == null) {
                event = cafebazaarEventRepository.save(new CafebazaarEvent(cmd, receipt, login));
            }
            Membership alreadyExist = findOneByCafebazaarEvent(event);
            if (alreadyExist != null) {
                log.warn("Cafebazaar membership already exist!");
                SubscriptionCheckDTO subscriptionCheck = cafebazaarApiClient.subscriptionCheck(event);
                if (alreadyExist.isExtensionNeeded(subscriptionCheck)) {
                    membershipRepository.save(alreadyExist.extensionFor(event, subscriptionCheck));
                    log.info("Cafebazaar membership extended!");
                }
                return ResponseEntity.ok().build();
            }
            if (receipt.getPurchaseState().equals(0)) { // 0 means purchase was fine.
                SubscriptionCheckDTO subscriptionCheck = cafebazaarApiClient.subscriptionCheck(event);
                if (subscriptionCheck.isValid()) {
                    Membership newMembership = new Membership(event);
                    membershipRepository.save(newMembership);
                    log.info("New membership {} created.", newMembership.toString());
                    return ResponseEntity.ok().build();
                } else {
                    return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
                }
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        } catch (Exception e) {
            log.error("CAFEBAZAAR_RECEIPT_PARSING_ERROR_NEED_ADMIN_ATTENTION!!!", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Deprecated
    @PostMapping("/cafebazaar/subscribe")
    @Timed
    public ResponseEntity<Void> subscribeCafebazaarDeprecated(@Valid @RequestBody XMarketSubscribeCommand cmd, Principal principal) {
        if (isAnonymous(principal)) {
            return ResponseEntity.badRequest().build();
        }
        log.info("An attempt to subscribe (with cafebazaar inappbilling) for {} with {}", login(principal), cmd.toString());
        return this.subscribeWithCafebazaar(cmd, login(principal));
    }

    private Membership findOneByCafebazaarEvent(CafebazaarEvent event) {
        List<Membership> list = membershipRepository.findByCafebazaarEventOrderByEndDateDesc(event);
        return list != null && list.size() > 0 ? list.get(0) : null;
    }

    private Membership findOneByMarketEvent(MarketEvent event) {
        List<Membership> list = membershipRepository.findByMarketEventOrderByEndDateDesc(event);
        return list != null && list.size() > 0 ? list.get(0) : null;
    }

    private final Logger log = LoggerFactory.getLogger(getClass());

}
