package mars.roo.web.rest.zarinpal;

import javax.validation.constraints.NotNull;

import mars.roo.domain.enumeration.SubscriptionType;

public class ZarinCodeCommand {

    String description;
    @NotNull
    SubscriptionType subscriptionType;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

}
