package mars.roo.web.rest.zarinpal;

import java.time.LocalDate;

import mars.roo.domain.Membership;
import mars.roo.domain.ZarinpalEvent;

public class ZarinpalCheckDTO {

    final boolean member;
    final boolean recorded;
    final LocalDate startDate;
    final LocalDate endDate;
    final String paymentToken;
    final String statusMessageCode;
    final String owner;

    public ZarinpalCheckDTO(ZarinpalEvent event, Membership membership) {
        member = membership != null;
        if (member) {
            startDate = membership.getStartDate();
            endDate = membership.getEndDate();
        }
        else {
            startDate = null;
            endDate = null;
        }
        recorded = event != null;
        if (recorded) {
            paymentToken = event.getPaymentToken();
            statusMessageCode = event.getZarinStatus() != null ? event.getZarinStatus().message(): null;
            owner = event.getOwner();
        }
        else {
            paymentToken = null;
            statusMessageCode = null;
            owner = null;
        }
    }

    public boolean isMember() {
        return member;
    }

    public boolean isRecorded() {
        return recorded;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public String getPaymentToken() {
        return paymentToken;
    }

    public String getStatusMessageCode() {
        return statusMessageCode;
    }

}
