package mars.roo.web.rest.zarinpal;

public class ZarinCodeDTO {

    private String startPayUrl;

    public ZarinCodeDTO() {
    }

    public ZarinCodeDTO(String startPayUrl) {
        this.startPayUrl = startPayUrl;
    }

    public String getStartPayUrl() {
        return startPayUrl;
    }

    public void setStartPayUrl(String startPayUrl) {
        this.startPayUrl = startPayUrl;
    }

}
