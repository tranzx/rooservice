package mars.roo.web.rest.zarinpal;

import java.security.Principal;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.jzarinpal.client.JZarinpalClient;
import mars.jzarinpal.client.helpers.UrlHelper;
import mars.jzarinpal.client.impl.JZarinpalClientImpl;
import mars.jzarinpal.client.impl.UnexpectedStatusCodeException;
import mars.jzarinpal.domain.dto.PaymentRequestDto;
import mars.roo.domain.ZarinpalEvent;
import mars.roo.repository.ZarinpalEventRepository;
import mars.roo.web.rest.BaseAuthenticatedResource;

@RestController
@RequestMapping("/api/user/zarinpal")
public class ZarinpalUserResource extends BaseAuthenticatedResource {

    private final ZarinpalEventRepository zarinpalEventRepository;

    public ZarinpalUserResource(ZarinpalEventRepository zarinpalEventRepository) {
        this.zarinpalEventRepository = zarinpalEventRepository;
    }

    @PostMapping("/payment/token")
    @Timed
    public ResponseEntity<String> getAuthroityCode(@Valid @RequestBody ZarinCodeCommand cmd, Principal principal) {
        ZarinpalEvent event = ZarinpalEvent.from(cmd, login(principal));
        JZarinpalClient client = JZarinpalClientImpl.newClient();
        PaymentRequestDto dto = new PaymentRequestDto(event.getAmount(), cmd.getDescription());
        try {
            String authorityCode = client.paymentRequest(dto);
            event.setPaymentToken(authorityCode);
            zarinpalEventRepository.save(event);
            return ResponseEntity.ok(UrlHelper.zarinpalStartPayUrl(authorityCode));
        } catch (UnexpectedStatusCodeException e) {
            logger.error("ZarinpalToken", e);
            return ResponseEntity.status(e.getStatusCode()).build();
        } catch (Exception pe) {
            logger.error("ZarinpalToken", pe);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

}
