package mars.roo.web.rest.zarinpal;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.jzarinpal.client.JZarinpalClient;
import mars.jzarinpal.client.impl.JZarinpalClientImpl;
import mars.jzarinpal.client.impl.UnexpectedStatusCodeException;
import mars.jzarinpal.domain.ZarinStatus;
import mars.jzarinpal.domain.dto.PaymentVerificationDto;
import mars.roo.domain.Membership;
import mars.roo.domain.ZarinpalEvent;
import mars.roo.repository.MembershipRepository;
import mars.roo.repository.ZarinpalEventRepository;

@RestController
@RequestMapping("/api/public/zarinpal")
public class ZarinpalPublicResource {

    private final ZarinpalEventRepository zarinpalEventRepository;
    private final MembershipRepository membershipRepository;
    private final Environment env;
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    public ZarinpalPublicResource(ZarinpalEventRepository zarinpalEventRepository,
                                  MembershipRepository membershipRepository,
                                  Environment environment) {
        this.zarinpalEventRepository = zarinpalEventRepository;
        this.membershipRepository = membershipRepository;
        this.env = environment;
    }

    @GetMapping("/callback")
    @Timed
    public void paymentCallback(HttpServletRequest request,
                                HttpServletResponse response,
                                @RequestParam(value = "Authority", required = true) String authority,
                                @RequestParam(value = "Status", required = true) String status) {
        logger.debug("ZarinpalCallback authority={} status={}", authority, status);
        ZarinpalEvent event = zarinpalEventRepository.findOneByPaymentToken(authority);
        if (event == null) {
            logger.warn("No ZarinpalEvent found with authority code={}, status was {} by the way.", authority, status);
            redirectToView(request, response, authority);
            return;
        }
        Membership alreadyExist = membershipRepository.findOneByZarinpalEvent(event);
        if (alreadyExist != null) {
            logger.warn("ZarinpalCallback membership already exist for {}", event.getOwner());
            redirectToView(request, response, authority);
            return;
        }
        if (status.equals("OK")) {
            JZarinpalClient client = JZarinpalClientImpl.newClient();
            PaymentVerificationDto dto = new PaymentVerificationDto(event.getAmount(), authority);
            try {
                Long refId = client.paymentVerification(dto);
                if (!new Long(0).equals(refId)) {
                    logger.info("ZarinPal paymet verification was successful with refId: {}", refId);
                    event.setRefId(refId);
                    event.setZarinStatus(ZarinStatus.OK);
                }
                else {
                    logger.warn("ZarinpalCallback refId was ZERO.");
                    event.setZarinStatus(ZarinStatus.NOT_FOUND_PAYMENT);
                }
            } catch (UnexpectedStatusCodeException e) {
                logger.error("ZarinpalCallback UnexpectedStatusCode {}", e.getStatusCode(), e);
                event.setZarinStatus(ZarinStatus.fromHttpStatusCode(e.getStatusCode()));
            }
        }
        else {
            event.setZarinStatus(ZarinStatus.NOT_FOUND);
        }
        zarinpalEventRepository.save(event);
        if (ZarinStatus.OK.equals(event.getZarinStatus()) ||
                ZarinStatus.CREATED.equals(event.getZarinStatus())) {
            Membership newMembership = new Membership(event);
            membershipRepository.save(newMembership);
        }
        redirectToView(request, response, authority);
    }

    @GetMapping("/check/{authority}")
    public ResponseEntity<ZarinpalCheckDTO> check(@PathVariable("authority") String authority) {
        final ZarinpalEvent event = zarinpalEventRepository.findOneByPaymentToken(authority);
        Membership membership = null;
        if (event != null) {
            membership = membershipRepository.findOneByZarinpalEvent(event);
        }
        return ResponseEntity.ok(new ZarinpalCheckDTO(event, membership));
    }

    private void redirectToView(HttpServletRequest request, HttpServletResponse response, String authority) {
        String redirectUrl = String.format("%s/#zarinpal/%s", env.getProperty("SERVER_API_URL"), authority);
        try {
            redirectStrategy.sendRedirect(request, response, redirectUrl);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

}
