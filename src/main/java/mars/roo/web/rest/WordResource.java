package mars.roo.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.devskiller.friendly_id.FriendlyId;

import mars.roo.domain.Word;
import mars.roo.service.WordService;
import mars.roo.web.rest.errors.BadRequestAlertException;
import mars.roo.web.rest.util.HeaderUtil;
import mars.roo.web.rest.util.PaginationUtil;
import mars.roo.service.dto.WordCriteria;
import mars.roo.service.WordQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Word.
 */
@RestController
@RequestMapping("/api")
public class WordResource {

    private final Logger log = LoggerFactory.getLogger(WordResource.class);

    private static final String ENTITY_NAME = "word";

    private final WordService wordService;

    private final WordQueryService wordQueryService;

    public WordResource(WordService wordService, WordQueryService wordQueryService) {
        this.wordService = wordService;
        this.wordQueryService = wordQueryService;
    }

    /**
     * POST  /words : Create a new word.
     *
     * @param word the word to create
     * @return the ResponseEntity with status 201 (Created) and with body the new word, or with status 400 (Bad Request) if the word has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/words")
    @Timed
    public ResponseEntity<Word> createWord(@Valid @RequestBody Word word) throws URISyntaxException {
        log.debug("REST request to save Word : {}", word);
        if (word.getId() != null) {
            throw new BadRequestAlertException("A new word cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Word result = wordService.save(word);
        return ResponseEntity.created(new URI("/api/words/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /words : Updates an existing word.
     *
     * @param word the word to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated word,
     * or with status 400 (Bad Request) if the word is not valid,
     * or with status 500 (Internal Server Error) if the word couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/words")
    @Timed
    public ResponseEntity<Word> updateWord(@Valid @RequestBody Word word) throws URISyntaxException {
        log.debug("REST request to update Word : {}", word);
        if (word.getId() == null) {
            return createWord(word);
        }
        Word result = wordService.save(word);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, word.getId().toString()))
            .body(result);
    }

    /**
     * GET  /words : get all the words.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of words in body
     */
    @GetMapping("/words")
    @Timed
    public ResponseEntity<List<Word>> getAllWords(WordCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Words by criteria: {}", criteria);
        Page<Word> page = wordQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/words");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /words/:id : get the "id" word.
     *
     * @param id the id of the word to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the word, or with status 404 (Not Found)
     */
    @GetMapping("/words/{id}")
    @Timed
    public ResponseEntity<Word> getWord(@PathVariable Long id) {
        log.debug("REST request to get Word : {}", id);
        Word word = wordService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(word));
    }

    /**
     * DELETE  /words/:id : delete the "id" word.
     *
     * @param id the id of the word to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/words/{id}")
    @Timed
    public ResponseEntity<Void> deleteWord(@PathVariable Long id) {
        log.debug("REST request to delete Word : {}", id);
        wordService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/words/fixUuid")
    @Timed
    public ResponseEntity<Word> fixUuid() {
        log.debug("Attempt to fix UUID for Word entity");
        Pageable pageable = new PageRequest(0, Integer.MAX_VALUE);
        Page<Word> words = wordService.findAll(pageable);
        for (Word word : words.getContent()) {
            if (word.getUuid() == null) {
                word.setUuid(FriendlyId.createFriendlyId());
            }
        }
        wordService.save(words.getContent());
        return ResponseEntity.ok().build();
    }

}
