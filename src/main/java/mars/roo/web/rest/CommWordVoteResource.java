package mars.roo.web.rest;

import com.codahale.metrics.annotation.Timed;
import mars.roo.domain.CommWordVote;

import mars.roo.repository.CommWordVoteRepository;
import mars.roo.web.rest.errors.BadRequestAlertException;
import mars.roo.web.rest.util.HeaderUtil;
import mars.roo.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CommWordVote.
 */
@RestController
@RequestMapping("/api")
public class CommWordVoteResource {

    private final Logger log = LoggerFactory.getLogger(CommWordVoteResource.class);

    private static final String ENTITY_NAME = "commWordVote";

    private final CommWordVoteRepository commWordVoteRepository;

    public CommWordVoteResource(CommWordVoteRepository commWordVoteRepository) {
        this.commWordVoteRepository = commWordVoteRepository;
    }

    /**
     * POST  /comm-word-votes : Create a new commWordVote.
     *
     * @param commWordVote the commWordVote to create
     * @return the ResponseEntity with status 201 (Created) and with body the new commWordVote, or with status 400 (Bad Request) if the commWordVote has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/comm-word-votes")
    @Timed
    public ResponseEntity<CommWordVote> createCommWordVote(@Valid @RequestBody CommWordVote commWordVote) throws URISyntaxException {
        log.debug("REST request to save CommWordVote : {}", commWordVote);
        if (commWordVote.getId() != null) {
            throw new BadRequestAlertException("A new commWordVote cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommWordVote result = commWordVoteRepository.save(commWordVote);
        return ResponseEntity.created(new URI("/api/comm-word-votes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /comm-word-votes : Updates an existing commWordVote.
     *
     * @param commWordVote the commWordVote to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated commWordVote,
     * or with status 400 (Bad Request) if the commWordVote is not valid,
     * or with status 500 (Internal Server Error) if the commWordVote couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/comm-word-votes")
    @Timed
    public ResponseEntity<CommWordVote> updateCommWordVote(@Valid @RequestBody CommWordVote commWordVote) throws URISyntaxException {
        log.debug("REST request to update CommWordVote : {}", commWordVote);
        if (commWordVote.getId() == null) {
            return createCommWordVote(commWordVote);
        }
        CommWordVote result = commWordVoteRepository.save(commWordVote);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, commWordVote.getId().toString()))
            .body(result);
    }

    /**
     * GET  /comm-word-votes : get all the commWordVotes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of commWordVotes in body
     */
    @GetMapping("/comm-word-votes")
    @Timed
    public ResponseEntity<List<CommWordVote>> getAllCommWordVotes(Pageable pageable) {
        log.debug("REST request to get a page of CommWordVotes");
        Page<CommWordVote> page = commWordVoteRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/comm-word-votes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /comm-word-votes/:id : get the "id" commWordVote.
     *
     * @param id the id of the commWordVote to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the commWordVote, or with status 404 (Not Found)
     */
    @GetMapping("/comm-word-votes/{id}")
    @Timed
    public ResponseEntity<CommWordVote> getCommWordVote(@PathVariable Long id) {
        log.debug("REST request to get CommWordVote : {}", id);
        CommWordVote commWordVote = commWordVoteRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(commWordVote));
    }

    /**
     * DELETE  /comm-word-votes/:id : delete the "id" commWordVote.
     *
     * @param id the id of the commWordVote to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/comm-word-votes/{id}")
    @Timed
    public ResponseEntity<Void> deleteCommWordVote(@PathVariable Long id) {
        log.debug("REST request to delete CommWordVote : {}", id);
        commWordVoteRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
