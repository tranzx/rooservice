package mars.roo.web.rest.build;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

@RestController
@RequestMapping("/api/public/build")
public class BuildPublicResource {

    private final Integer latestVersionCode = 152;

    @GetMapping("/versionCode")
    @Timed
    public ResponseEntity<Integer> versionCode() {
        return ResponseEntity.ok(latestVersionCode);
    }

}
