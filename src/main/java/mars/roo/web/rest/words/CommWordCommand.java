package mars.roo.web.rest.words;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.devskiller.friendly_id.FriendlyId;

import mars.roo.domain.CommWord;
import mars.roo.domain.Word;
import mars.roo.domain.enumeration.Language;

public class CommWordCommand {

    @NotNull
    private String uuid;    // Note: this uuid can be Word#uuid or CommWord#uuid depends to the method action (create or update).
    @NotNull
    private Language language;
    @NotNull
    @NotEmpty
    private String text;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public CommWord createCommWord(Word word, String author, boolean isBase) {
        CommWord cw = new CommWord();
        cw.setWord(word);
        cw.setAuthor(author);
        cw.setBase(isBase);
        cw.setCreated(ZonedDateTime.now(ZoneOffset.UTC));
        cw.setLanguage(language);
        cw.setText(text);
        cw.setUuid(FriendlyId.createFriendlyId());
        return cw;
    }

}
