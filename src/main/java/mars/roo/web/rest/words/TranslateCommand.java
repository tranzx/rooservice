package mars.roo.web.rest.words;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class TranslateCommand {

    @NotNull
    @NotEmpty
    private String motherLanguage;
    @NotNull
    @NotEmpty
    private String targetLanguage;
    @NotNull
    @NotEmpty
    private String text;

    public String getMotherLanguage() {
        return motherLanguage;
    }
    public void setMotherLanguage(String motherLanguage) {
        this.motherLanguage = motherLanguage;
    }
    public String getTargetLanguage() {
        return targetLanguage;
    }
    public void setTargetLanguage(String targetLanguage) {
        this.targetLanguage = targetLanguage;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

}
