package mars.roo.web.rest.words;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.roo.domain.CommWord;
import mars.roo.domain.LessonCommWord;
import mars.roo.domain.LessonWord;
import mars.roo.domain.Word;
import mars.roo.repository.CommWordRepository;
import mars.roo.repository.LessonCommWordRepository;
import mars.roo.repository.LessonWordRepository;
import mars.roo.repository.WordRepository;
import mars.roo.security.AuthoritiesConstants;
import mars.roo.web.rest.BaseAuthenticatedResource;

@RestController
@RequestMapping("/api/user")
public class CommWordUserResource extends BaseAuthenticatedResource {

    private final WordRepository wordRepository;
    private final LessonWordRepository lessonWordRepository;
    private final CommWordRepository commWordRepository;
    private final LessonCommWordRepository lessonCommWordRepository;

    public CommWordUserResource(WordRepository wordRepository,
            LessonWordRepository lessonWordRepository,
            CommWordRepository commWordRepository,
            LessonCommWordRepository lessonCommWordRepository) {
        this.wordRepository = wordRepository;
        this.lessonWordRepository = lessonWordRepository;
        this.commWordRepository = commWordRepository;
        this.lessonCommWordRepository = lessonCommWordRepository;
    }

    @PostMapping("/comm-words")
    @Timed
    public ResponseEntity<Object> createCommWord(
            @Valid @RequestBody CommWordCommand cmd, Principal principal) throws URISyntaxException {
        log.debug("REST request to save CommWord: {} ", cmd);
        Word word = wordRepository.findOneByUuid(cmd.getUuid());
        CommWord commWord = commWordRepository.findOneByWordAndLanguageAndText(word, cmd.getLanguage(), cmd.getText());
        if (commWord != null)
            return ResponseEntity.badRequest().body("Already exist.");
        else {
            commWord = cmd.createCommWord(word, login(principal), hasRole(AuthoritiesConstants.ROODATA, principal));
            commWordRepository.save(commWord);
            cmd.setUuid(commWord.getUuid());    // Word#uuid replaced with CommWord#uuid
            List<LessonWord> lws = lessonWordRepository.findByWord(word);
            HashSet<LessonCommWord> lcwSet = new HashSet<LessonCommWord>();
            for (LessonWord lw: lws) {
                lcwSet.add(new LessonCommWord(lw, commWord));
            }
            lessonCommWordRepository.save(lcwSet);
        }
        return ResponseEntity.created(new URI("/api/user/comm-words/")).body(cmd);
    }

    @PutMapping("/comm-words")
    @Timed
    public ResponseEntity<Object> updateCommWord(Principal principal,
            @Valid @RequestBody CommWordCommand cmd) throws URISyntaxException {
        log.debug("REST request to update CommWord: {} ", cmd);
        CommWord commWord = commWordRepository.findOneByUuidAndAuthor(cmd.getUuid(), login(principal));
        if (commWord == null || (commWord.isReported() != null && commWord.isReported())) {
            return ResponseEntity.badRequest().body("Doesn't exist or marked as reported.");
        }
        List<CommWord> uniqueTexts = commWordRepository.findByWordAndLanguageAndText(commWord.getWord(), cmd.getLanguage(), cmd.getText());
        if (uniqueTexts.size() > 0 && uniqueTexts.stream().anyMatch(cw -> { return !cw.getUuid().equals(commWord.getUuid()); })) {
            return ResponseEntity.badRequest().body("Already exist.");
        }
        commWord.setText(cmd.getText());
        commWord.setUpdated(ZonedDateTime.now(ZoneOffset.UTC));
        commWordRepository.save(commWord);
        List<LessonCommWord> lcws = lessonCommWordRepository.findByCommWordUuidAndAuthor(commWord.getUuid(), login(principal));
        lcws.forEach(lcw -> lcw.setText(commWord.getText()));
        lessonCommWordRepository.save(lcws);
        return ResponseEntity.ok(cmd);
    }

    @Transactional
    @DeleteMapping("/comm-words/{uuid}")
    @Timed
    public ResponseEntity<Void> deleteCommWord(@PathVariable String uuid, Principal principal) {
        CommWord cw = commWordRepository.findOneByUuidAndAuthor(uuid, login(principal));
        if (cw != null) {
            lessonCommWordRepository.deleteByCommWordUuidAndAuthor(cw.getUuid(), login(principal));
            commWordRepository.delete(cw.getId());
            return ResponseEntity.ok().build();
        } else
            return ResponseEntity.badRequest().build();
    }

    @Transactional
    @PostMapping("/comm-words/report")
    @Timed
    public ResponseEntity<Void> flagAsReportedCommWord(@RequestBody CommWordCommand cmd, Principal principal) {
        CommWord cw = commWordRepository.findOneByUuid(cmd.getUuid(), login(principal));
        if (cw != null && !cw.isBase()) {
            lessonCommWordRepository.deleteByCommWordUuid(cw.getUuid());
            cw.setReported(true);
            commWordRepository.save(cw);
            // TODO send email to moderators, and store Reporter/Report-time/Report-description, etc for later access.
            return ResponseEntity.ok().build();
        } else
            return ResponseEntity.badRequest().build();
    }

    private final Logger log = LoggerFactory.getLogger(getClass());

}
