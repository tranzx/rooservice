package mars.roo.web.rest.words;

public final class TranslateDTO {

    private final String text;

    public TranslateDTO(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
