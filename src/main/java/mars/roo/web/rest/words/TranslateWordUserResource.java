package mars.roo.web.rest.words;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
//import com.google.cloud.translate.Translate;
//import com.google.cloud.translate.Translate.TranslateOption;
//import com.google.cloud.translate.TranslateOptions;
//import com.google.cloud.translate.Translation;

import mars.roo.web.rest.BaseAuthenticatedResource;

@RestController
@RequestMapping("/api/user")
public class TranslateWordUserResource extends BaseAuthenticatedResource {

//    private final Translate translate;
    private final Map<String, String> translatedTextCache = new HashMap<String, String>();
    private Integer sentCharactersLength = 0;

    public TranslateWordUserResource() {
//        this.translate = TranslateOptions.getDefaultInstance().getService();
    }

    @PostMapping("/translate")
    @Timed
    public ResponseEntity<TranslateDTO> translate(@Valid @RequestBody TranslateCommand cmd) {
        if (translatedTextCache.containsKey(keyForCache(cmd))) {
            return ResponseEntity.ok(new TranslateDTO(translatedTextCache.get(keyForCache(cmd))));
        }
        try {
//            Translation translation = translate.translate(cmd.getText(),
//                    TranslateOption.sourceLanguage(cmd.getMotherLanguage()),
//                    TranslateOption.targetLanguage(cmd.getTargetLanguage()));
//            String translatedText = translation.getTranslatedText();
            sentCharactersLength += cmd.getText().length();
//            translatedTextCache.put(keyForCache(cmd), translatedText);      // This will cost money, so it make sense to cache duplicate translations.
            logger.warn("Total characters sent to Google translate api is {}", sentCharactersLength);
            return ResponseEntity.ok(new TranslateDTO("FOO!"));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private String keyForCache(TranslateCommand cmd) {
        return String.format("%s-%s-%s", cmd.getMotherLanguage(), cmd.getTargetLanguage(), cmd.getText());
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

}
