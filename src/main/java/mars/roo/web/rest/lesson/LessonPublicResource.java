package mars.roo.web.rest.lesson;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.roo.domain.Category;
import mars.roo.domain.enumeration.Level;
import mars.roo.repository.CategoryRepository;
import mars.roo.service.LessonService;
import mars.roo.service.MembershipQueryService;
import mars.roo.web.rest.BaseAuthenticatedResource;

@RestController
@RequestMapping("/api/public")
public class LessonPublicResource extends BaseAuthenticatedResource {

    private final LessonService lessonService;
    private final CategoryRepository categoryRepository;
    private final MembershipQueryService membershipQueryService;

    public LessonPublicResource(LessonService lessonService,
            CategoryRepository categoryRepository,
            MembershipQueryService membershipQueryService) {
        this.lessonService = lessonService;
        this.categoryRepository = categoryRepository;
        this.membershipQueryService = membershipQueryService;
    }

    @GetMapping("/lessons/{level}/{categoryUuid}")
    @Timed
    public ResponseEntity<List<LessonPublicDTO>> getLessonListPublic(
            @PathVariable("level") Level level,
            @PathVariable("categoryUuid") String categoryUuid, Principal principal) {
        log.info("Rest request to get list of lessons for {} and category uuid {}", level, categoryUuid);
        if (level == null || categoryUuid == null || StringUtils.isEmpty(categoryUuid)) {
            return ResponseEntity.badRequest().build();
        }
        Category category = categoryRepository.findOneByUuid(categoryUuid);
        if (category == null
                || (category.isForSell() && isAnonymous(principal))
                || (category.isForSell() && !isAnonymous(principal) && !membershipQueryService.isMember(anonymous(principal)))) {
            log.warn("{} want to access {} category {} level but doesn't have access or category deosn't exist!", anonymous(principal), category, level);
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(LessonPublicDTO.toDTO(lessonService.findByCategoryAndLevel(category, level)));
    }

    private final Logger log = LoggerFactory.getLogger(getClass());

}
