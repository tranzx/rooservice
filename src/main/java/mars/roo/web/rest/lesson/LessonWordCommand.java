package mars.roo.web.rest.lesson;

import mars.roo.domain.Lesson;
import mars.roo.domain.LessonWord;
import mars.roo.domain.Word;
import mars.roo.domain.enumeration.WordType;

/**
 * @author developer
 *
 */
public class LessonWordCommand {

    private String enGb;
    private String faIr;
    private WordType wordType;
    private Boolean background;
    private Boolean capitalization;
    private Boolean picture;
    private Long lessonId;
    private Long wordId;

    public boolean hasWordId() {
        return wordId != null;
    }

    public Word toWord() {
        Word w = new Word();
        w.setEnGb(enGb);
        w.setFaIr(faIr);
        w.setWordType(wordType);
        return w;
    }

    public LessonWord toLessonWord(Lesson lesson, Word word, Integer indexOrder) {
        LessonWord lw = new LessonWord();
        lw.setWord(word);
        lw.setLesson(lesson);
        lw.setBackground(background);
        lw.setCapitalization(capitalization);
        lw.setPicture(picture);
        lw.setIndexOrder(indexOrder);
        return lw;
    }

    public boolean capitalizationChanged(Boolean dbCapitalization) {
        if (capitalization == null || capitalization == false) {
            return Boolean.TRUE.equals(dbCapitalization);
        } else {
            return Boolean.FALSE.equals(dbCapitalization);
        }
    }

    // getters / setters

    public String getEnGb() {
        return enGb;
    }

    public void setEnGb(String enGb) {
        this.enGb = enGb;
    }

    public String getFaIr() {
        return faIr;
    }

    public void setFaIr(String faIr) {
        this.faIr = faIr;
    }

    public WordType getWordType() {
        return wordType;
    }

    public void setWordType(WordType wordType) {
        this.wordType = wordType;
    }

    public Boolean getBackground() {
        return background;
    }

    public void setBackground(Boolean background) {
        this.background = background;
    }

    public Boolean getCapitalization() {
        return capitalization;
    }

    public void setCapitalization(Boolean capitalization) {
        this.capitalization = capitalization;
    }

    public Boolean getPicture() {
        return picture;
    }

    public void setPicture(Boolean picture) {
        this.picture = picture;
    }

    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public Long getWordId() {
        return wordId;
    }

    public void setWordId(Long wordId) {
        this.wordId = wordId;
    }

}
