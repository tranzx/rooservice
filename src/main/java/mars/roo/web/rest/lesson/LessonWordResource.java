package mars.roo.web.rest.lesson;

import com.codahale.metrics.annotation.Timed;
import mars.roo.domain.LessonWord;
import mars.roo.service.LessonWordService;
import mars.roo.web.rest.errors.BadRequestAlertException;
import mars.roo.web.rest.util.HeaderUtil;
import mars.roo.web.rest.util.PaginationUtil;
import mars.roo.service.dto.LessonWordCriteria;
import mars.roo.service.LessonWordQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing LessonWord.
 */
@RestController
@RequestMapping("/api")
public class LessonWordResource {

    private final Logger log = LoggerFactory.getLogger(LessonWordResource.class);

    private static final String ENTITY_NAME = "lessonWord";

    private final LessonWordService lessonWordService;

    private final LessonWordQueryService lessonWordQueryService;

    public LessonWordResource(LessonWordService lessonWordService, LessonWordQueryService lessonWordQueryService) {
        this.lessonWordService = lessonWordService;
        this.lessonWordQueryService = lessonWordQueryService;
    }

    /**
     * POST  /lesson-words : Create a new lessonWord.
     *
     * @param lessonWord the lessonWord to create
     * @return the ResponseEntity with status 201 (Created) and with body the new lessonWord, or with status 400 (Bad Request) if the lessonWord has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/lesson-words")
    @Timed
    public ResponseEntity<LessonWord> createLessonWord(@Valid @RequestBody LessonWord lessonWord) throws URISyntaxException {
        log.debug("REST request to save LessonWord : {}", lessonWord);
        if (lessonWord.getId() != null) {
            throw new BadRequestAlertException("A new lessonWord cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LessonWord result = lessonWordService.save(lessonWord);
        return ResponseEntity.created(new URI("/api/lesson-words/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/lesson-words/create-custom")
    @Timed
    public ResponseEntity<LessonWord> createLessonWordCustom(@Valid @RequestBody LessonWordCommand cmd) throws URISyntaxException {
        log.debug("REST request to save LessonWord : {}", cmd);
        LessonWord result = lessonWordService.saveOrFind(cmd);
        return ResponseEntity.ok(result);
    }

    /**
     * PUT  /lesson-words : Updates an existing lessonWord.
     *
     * @param lessonWord the lessonWord to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lessonWord,
     * or with status 400 (Bad Request) if the lessonWord is not valid,
     * or with status 500 (Internal Server Error) if the lessonWord couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/lesson-words")
    @Timed
    public ResponseEntity<LessonWord> updateLessonWord(@Valid @RequestBody LessonWord lessonWord) throws URISyntaxException {
        log.debug("REST request to update LessonWord : {}", lessonWord);
        if (lessonWord.getId() == null) {
            return createLessonWord(lessonWord);
        }
        LessonWord result = lessonWordService.save(lessonWord);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lessonWord.getId().toString()))
            .body(result);
    }

    @PutMapping("/lesson-words/updateAll")
    @Timed
    public ResponseEntity<List<LessonWord>> updateAllLessonWord(@RequestBody List<LessonWord> lessonWords) throws URISyntaxException {
        log.debug("REST request to update all LessonWord : {}", lessonWords.size());
        List<LessonWord> result = new ArrayList<LessonWord>();
        lessonWords.forEach(lessonWord -> {
            result.add(lessonWordService.save(lessonWord));
        });
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lessonWords.stream().map(e -> e.getId().toString()).collect(Collectors.joining("-"))))
            .body(result);
    }


    /**
     * GET  /lesson-words : get all the lessonWords.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of lessonWords in body
     */
    @GetMapping("/lesson-words")
    @Timed
    public ResponseEntity<List<LessonWord>> getAllLessonWords(LessonWordCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LessonWords by criteria: {}", criteria);
        Page<LessonWord> page = lessonWordQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/lesson-words");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /lesson-words/:id : get the "id" lessonWord.
     *
     * @param id the id of the lessonWord to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the lessonWord, or with status 404 (Not Found)
     */
    @GetMapping("/lesson-words/{id}")
    @Timed
    public ResponseEntity<LessonWord> getLessonWord(@PathVariable Long id) {
        log.debug("REST request to get LessonWord : {}", id);
        LessonWord lessonWord = lessonWordService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lessonWord));
    }

    /**
     * DELETE  /lesson-words/:id : delete the "id" lessonWord.
     *
     * @param id the id of the lessonWord to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/lesson-words/{id}")
    @Timed
    public ResponseEntity<Void> deleteLessonWord(@PathVariable Long id) {
        log.debug("REST request to delete LessonWord : {}", id);
        lessonWordService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
