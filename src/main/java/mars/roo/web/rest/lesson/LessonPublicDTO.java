package mars.roo.web.rest.lesson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mars.roo.domain.Lesson;

/**
 * LessonDTO suitable for transfer over the net to customers.
 */
public final class LessonPublicDTO implements Comparable<LessonPublicDTO>, Comparator<LessonPublicDTO> {

    private final String uuid;
    private final Integer indexOrder;

    public LessonPublicDTO(Lesson lt) {
        uuid = lt.getUuid();
        indexOrder = lt.getIndexOrder();
    }

    public String getUuid() {
        return uuid;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public static List<LessonPublicDTO> toDTO(List<Lesson> entities) {
        List<LessonPublicDTO> result = new ArrayList<LessonPublicDTO>();
        for (Lesson entity: entities) {
            result.add(new LessonPublicDTO(entity));
        }
        Collections.sort(result, new Comparator<LessonPublicDTO>() {
            @Override
            public int compare(LessonPublicDTO l1, LessonPublicDTO l2) {
                return l1.compareTo(l2);
            }
        });
        return result;
    }

    @Override
    public int compare(LessonPublicDTO l1, LessonPublicDTO l2) {
        return l1.compareTo(l2);
    }

    @Override
    public int compareTo(LessonPublicDTO other) {
        return indexOrder.compareTo(other.indexOrder);
    }

}
