package mars.roo.web.rest.lesson;

import mars.roo.domain.Lesson;

public final class LessonDailyDTO {

    private final String uuid;
    private final String title;
    private final String picture;

    LessonDailyDTO(Lesson lesson) {
        uuid = lesson.getUuid();
        title = lesson.getTitleEng();
        picture = lesson.getPictureName();
    }

    public String getUuid() {
        return uuid;
    }

    public String getTitle() {
        return title;
    }

    public String getPicture() {
        return picture;
    }

}
