package mars.roo.web.rest.lesson;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import mars.roo.domain.Lesson;
import mars.roo.domain.LessonWord;
import mars.roo.domain.Question;
import mars.roo.domain.enumeration.Level;
import mars.roo.service.LessonQueryService;
import mars.roo.service.LessonService;
import mars.roo.service.LessonWordService;
import mars.roo.service.QuestionService;
import mars.roo.service.dto.LessonCriteria;
import mars.roo.web.rest.errors.BadRequestAlertException;
import mars.roo.web.rest.util.HeaderUtil;
import mars.roo.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Lesson.
 */
@RestController
@RequestMapping("/api")
public class LessonResource {

    private final Logger log = LoggerFactory.getLogger(LessonResource.class);

    private static final String ENTITY_NAME = "lesson";

    private final LessonService lessonService;

    private final LessonQueryService lessonQueryService;

    private final LessonWordService lessonWordService;

    private final QuestionService questionService;

    public LessonResource(LessonService lessonService, LessonQueryService lessonQueryService,
                          LessonWordService lessonWordService, QuestionService questionService) {
        this.lessonService = lessonService;
        this.lessonQueryService = lessonQueryService;
        this.lessonWordService = lessonWordService;
        this.questionService = questionService;
    }

    /**
     * POST  /lessons : Create a new lesson.
     *
     * @param lesson the lesson to create
     * @return the ResponseEntity with status 201 (Created) and with body the new lesson, or with status 400 (Bad Request) if the lesson has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/lessons")
    @Timed
    public ResponseEntity<Lesson> createLesson(@Valid @RequestBody Lesson lesson) throws URISyntaxException {
        log.debug("REST request to save Lesson : {}", lesson);
        if (lesson.getId() != null) {
            throw new BadRequestAlertException("A new lesson cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Lesson result = lessonService.save(lesson);
        return ResponseEntity.created(new URI("/api/lessons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /lessons : Updates an existing lesson.
     *
     * @param lesson the lesson to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lesson,
     * or with status 400 (Bad Request) if the lesson is not valid,
     * or with status 500 (Internal Server Error) if the lesson couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/lessons")
    @Timed
    public ResponseEntity<Lesson> updateLesson(@Valid @RequestBody Lesson lesson) throws URISyntaxException {
        log.debug("REST request to update Lesson : {}", lesson);
        if (lesson.getId() == null) {
            return createLesson(lesson);
        }
        Lesson result = lessonService.save(lesson);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lesson.getId().toString()))
            .body(result);
    }

    /**
     * GET  /lessons : get all the lessons.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of lessons in body
     */
    @GetMapping("/lessons")
    @Timed
    public ResponseEntity<List<Lesson>> getAllLessons(LessonCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Lessons by criteria: {}", criteria);
        Page<Lesson> page = lessonQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/lessons");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /lessons/:id : get the "id" lesson.
     *
     * @param id the id of the lesson to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the lesson, or with status 404 (Not Found)
     */
    @GetMapping("/lessons/{id}")
    @Timed
    public ResponseEntity<Lesson> getLesson(@PathVariable Long id) {
        log.debug("REST request to get Lesson : {}", id);
        Lesson lesson = lessonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lesson));
    }

    @GetMapping("/lessons/{id}/{level}")
    @Timed
    public ResponseEntity<Lesson> switchLessonLevel(@PathVariable("id") Long id,
            @PathVariable("level") Level level) {
        log.debug("REST request to switch Lesson with : {} {}", id, level);
        Lesson origin = lessonService.findOne(id);
        Lesson lesson = lessonService.findOneByCategoryAndLevelAndIndexOrder(origin.getCategory(), level, origin.getIndexOrder());
        if (lesson == null) {
            List<LessonWord> originLessonWords = lessonWordService.findByLesson(origin);
            origin.setId(null);
            origin.setLevel(level);
            origin.setUuid(UUID.randomUUID().toString());
            lesson = lessonService.save(origin);
            for (LessonWord lw: originLessonWords) {
                lw.setId(null);
                lw.setLesson(lesson);
            }
            lessonWordService.save(originLessonWords);
            List<Question> originQuestions = questionService.findByLesson(lessonService.findOne(id));
            for (Question q: originQuestions) {
                q.setId(null);
                q.setUuid(UUID.randomUUID().toString());
                q.setLesson(lesson);
                q.setLevel(level);
            }
            questionService.save(originQuestions);
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lesson));
    }


    /**
     * DELETE  /lessons/:id : delete the "id" lesson.
     *
     * @param id the id of the lesson to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/lessons/{id}")
    @Timed
    public ResponseEntity<Void> deleteLesson(@PathVariable Long id) {
        log.debug("REST request to delete Lesson : {}", id);
        lessonWordService.deleteByLesson(lessonService.findOne(id));
        lessonService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @DeleteMapping("/lessons/{categoryId}/deleteQuestionsByCategory")
    @Timed
    @Transactional
    public ResponseEntity<Void> deleteQuestionsByCategory(@PathVariable Long categoryId) {
        log.debug("REST request to delete questions by category id: {}", categoryId);
        List<Lesson> lessons = lessonService.findByCategoryId(categoryId);
        lessons.forEach(lesson -> {
            questionService.deleteByLesson(lesson);
        });
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, categoryId.toString())).build();
    }

    @DeleteMapping("/lessons/{lessonId}/deleteQuestionsByLesson")
    @Timed
    @Transactional
    public ResponseEntity<Void> deleteQuestionsByLesson(@PathVariable Long lessonId) {
        log.debug("REST request to delete questions by lesson id: {}", lessonId);
        questionService.deleteByLesson(lessonService.findOne(lessonId));
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, lessonId.toString())).build();
    }

}
