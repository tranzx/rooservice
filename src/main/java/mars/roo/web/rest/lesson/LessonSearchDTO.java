package mars.roo.web.rest.lesson;

import java.util.ArrayList;
import java.util.List;

import mars.roo.domain.LessonSearch;
import mars.roo.domain.enumeration.Language;
import mars.roo.domain.enumeration.LearnDir;

public class LessonSearchDTO {

    private final String cUuid;
    private final Integer cIndex;
    private final String cTitle;

    private final String lUuid;
    private final Integer lIndex;

    public LessonSearchDTO(LessonSearch ls, LearnDir learnDir) {
        this.cUuid = ls.getCategoryUuid();
        this.cIndex  = ls.getCategoryIndexOrder();
        this.cTitle = learnDir.mother().equals(Language.FA_IR) ? ls.getCategoryTitleFa() : ls.getCategoryTitleEn();
        this.lUuid = ls.getLessonUuid();
        this.lIndex  = ls.getLessonIndexOrder();
    }

    public static List<LessonSearchDTO> toDTO(List<LessonSearch> entities, LearnDir learnDir) {
        List<LessonSearchDTO> list = new ArrayList<LessonSearchDTO>();
        for (LessonSearch ls: entities) {
            list.add(new LessonSearchDTO(ls, learnDir));
        }
        return list;
    }

    public String getcUuid() {
        return cUuid;
    }

    public Integer getcIndex() {
        return cIndex;
    }

    public String getcTitle() {
        return cTitle;
    }

    public String getlUuid() {
        return lUuid;
    }

    public Integer getlIndex() {
        return lIndex;
    }

    @Override
    public int hashCode() {
        return lUuid.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        LessonSearchDTO o = (LessonSearchDTO) obj;
        return lUuid.equals(o.lUuid);
    }

}
