package mars.roo.web.rest.lesson;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.roo.domain.LessonSearch;
import mars.roo.domain.enumeration.LearnDir;
import mars.roo.service.LessonSearchQueryService;
import mars.roo.service.LessonSearchService;
import mars.roo.service.dto.LessonSearchCriteria;
import mars.roo.web.rest.util.PaginationUtil;

/**
 * REST controller for managing LessonSearch.
 */
@RestController
@RequestMapping("/api/public")
public class LessonSearchResource {

    private final Logger log = LoggerFactory.getLogger(LessonSearchResource.class);

    private final LessonSearchQueryService lessonSearchQueryService;

    public LessonSearchResource(LessonSearchService lessonSearchService, LessonSearchQueryService lessonSearchQueryService) {
        this.lessonSearchQueryService = lessonSearchQueryService;
    }

    /**
     * GET  /lesson/search : lesson search end-point
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of lessonSearches in body
     */
    @GetMapping("/lesson/search/{learnDir}")
    @Timed
    public ResponseEntity<List<LessonSearchDTO>> search(LessonSearchCriteria criteria, Pageable pageable,
            @PathVariable("learnDir") LearnDir learnDir) {
        log.debug("REST request to get LessonSearches by criteria: {}", criteria);
        Page<LessonSearch> page = lessonSearchQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user/lesson/search");
        return new ResponseEntity<>(LessonSearchDTO.toDTO(page.getContent(), learnDir), headers, HttpStatus.OK);
    }

}
