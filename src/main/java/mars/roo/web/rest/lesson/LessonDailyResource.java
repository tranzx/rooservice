package mars.roo.web.rest.lesson;

import java.security.Principal;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.roo.domain.Lesson;
import mars.roo.domain.Score;
import mars.roo.service.LessonService;
import mars.roo.service.ScoreQueryService;
import mars.roo.web.rest.BaseAuthenticatedResource;

@RestController
@RequestMapping("/api/user")
public class LessonDailyResource extends BaseAuthenticatedResource {

    private final LessonService lessonService;
    private final ScoreQueryService scoreQueryService;

    public LessonDailyResource(LessonService lessonService, ScoreQueryService scoreQueryService) {
        this.lessonService = lessonService;
        this.scoreQueryService = scoreQueryService;
    }

    @GetMapping("/lesson/daily")
    @Timed
    public ResponseEntity<LessonDailyDTO> nextDailyLesson(Principal principal) {
        Score score = scoreQueryService.findPreviousDoneDailyScore(login(principal));
        Lesson previous;
        if (score != null) {
            previous = lessonService.findByUuid(score.getLessonUuid());
        } else {
            previous = new Lesson();   // First-time scenario.
            previous.setIndexOrder(0);
        }
        Lesson next = lessonService.findOneByIndexOrderAndCategoryIsNull(previous.getIndexOrder() + 1);
        if (next == null) { // This should really not happen! but in case there is no new lesson available.
            next = previous;
        }
        return ResponseEntity.ok(new LessonDailyDTO(next));
    }

}
