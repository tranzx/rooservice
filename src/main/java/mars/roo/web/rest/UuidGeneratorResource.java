package mars.roo.web.rest;

import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/uuid")
public class UuidGeneratorResource {

	@GetMapping("/generate")
	public ResponseEntity<String> generateUuid() {
		return ResponseEntity.ok().body(UUID.randomUUID().toString());
	}

}