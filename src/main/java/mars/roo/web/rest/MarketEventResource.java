package mars.roo.web.rest;

import com.codahale.metrics.annotation.Timed;
import mars.roo.domain.MarketEvent;

import mars.roo.repository.MarketEventRepository;
import mars.roo.web.rest.errors.BadRequestAlertException;
import mars.roo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MarketEvent.
 */
@RestController
@RequestMapping("/api")
public class MarketEventResource {

    private final Logger log = LoggerFactory.getLogger(MarketEventResource.class);

    private static final String ENTITY_NAME = "marketEvent";

    private final MarketEventRepository marketEventRepository;

    public MarketEventResource(MarketEventRepository marketEventRepository) {
        this.marketEventRepository = marketEventRepository;
    }

    /**
     * POST  /market-events : Create a new marketEvent.
     *
     * @param marketEvent the marketEvent to create
     * @return the ResponseEntity with status 201 (Created) and with body the new marketEvent, or with status 400 (Bad Request) if the marketEvent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/market-events")
    @Timed
    public ResponseEntity<MarketEvent> createMarketEvent(@Valid @RequestBody MarketEvent marketEvent) throws URISyntaxException {
        log.debug("REST request to save MarketEvent : {}", marketEvent);
        if (marketEvent.getId() != null) {
            throw new BadRequestAlertException("A new marketEvent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MarketEvent result = marketEventRepository.save(marketEvent);
        return ResponseEntity.created(new URI("/api/market-events/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /market-events : Updates an existing marketEvent.
     *
     * @param marketEvent the marketEvent to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated marketEvent,
     * or with status 400 (Bad Request) if the marketEvent is not valid,
     * or with status 500 (Internal Server Error) if the marketEvent couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/market-events")
    @Timed
    public ResponseEntity<MarketEvent> updateMarketEvent(@Valid @RequestBody MarketEvent marketEvent) throws URISyntaxException {
        log.debug("REST request to update MarketEvent : {}", marketEvent);
        if (marketEvent.getId() == null) {
            return createMarketEvent(marketEvent);
        }
        MarketEvent result = marketEventRepository.save(marketEvent);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, marketEvent.getId().toString()))
            .body(result);
    }

    /**
     * GET  /market-events : get all the marketEvents.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of marketEvents in body
     */
    @GetMapping("/market-events")
    @Timed
    public List<MarketEvent> getAllMarketEvents() {
        log.debug("REST request to get all MarketEvents");
        return marketEventRepository.findAll();
        }

    /**
     * GET  /market-events/:id : get the "id" marketEvent.
     *
     * @param id the id of the marketEvent to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the marketEvent, or with status 404 (Not Found)
     */
    @GetMapping("/market-events/{id}")
    @Timed
    public ResponseEntity<MarketEvent> getMarketEvent(@PathVariable Long id) {
        log.debug("REST request to get MarketEvent : {}", id);
        MarketEvent marketEvent = marketEventRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(marketEvent));
    }

    /**
     * DELETE  /market-events/:id : delete the "id" marketEvent.
     *
     * @param id the id of the marketEvent to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/market-events/{id}")
    @Timed
    public ResponseEntity<Void> deleteMarketEvent(@PathVariable Long id) {
        log.debug("REST request to delete MarketEvent : {}", id);
        marketEventRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
