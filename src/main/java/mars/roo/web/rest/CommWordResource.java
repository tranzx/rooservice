package mars.roo.web.rest;

import com.codahale.metrics.annotation.Timed;
import mars.roo.domain.CommWord;

import mars.roo.repository.CommWordRepository;
import mars.roo.web.rest.errors.BadRequestAlertException;
import mars.roo.web.rest.util.HeaderUtil;
import mars.roo.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CommWord.
 */
@RestController
@RequestMapping("/api")
public class CommWordResource {

    private final Logger log = LoggerFactory.getLogger(CommWordResource.class);

    private static final String ENTITY_NAME = "commWord";

    private final CommWordRepository commWordRepository;

    public CommWordResource(CommWordRepository commWordRepository) {
        this.commWordRepository = commWordRepository;
    }

    /**
     * POST  /comm-words : Create a new commWord.
     *
     * @param commWord the commWord to create
     * @return the ResponseEntity with status 201 (Created) and with body the new commWord, or with status 400 (Bad Request) if the commWord has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/comm-words")
    @Timed
    public ResponseEntity<CommWord> createCommWord(@Valid @RequestBody CommWord commWord) throws URISyntaxException {
        log.debug("REST request to save CommWord : {}", commWord);
        if (commWord.getId() != null) {
            throw new BadRequestAlertException("A new commWord cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommWord result = commWordRepository.save(commWord);
        return ResponseEntity.created(new URI("/api/comm-words/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /comm-words : Updates an existing commWord.
     *
     * @param commWord the commWord to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated commWord,
     * or with status 400 (Bad Request) if the commWord is not valid,
     * or with status 500 (Internal Server Error) if the commWord couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/comm-words")
    @Timed
    public ResponseEntity<CommWord> updateCommWord(@Valid @RequestBody CommWord commWord) throws URISyntaxException {
        log.debug("REST request to update CommWord : {}", commWord);
        if (commWord.getId() == null) {
            return createCommWord(commWord);
        }
        CommWord result = commWordRepository.save(commWord);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, commWord.getId().toString()))
            .body(result);
    }

    /**
     * GET  /comm-words : get all the commWords.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of commWords in body
     */
    @GetMapping("/comm-words")
    @Timed
    public ResponseEntity<List<CommWord>> getAllCommWords(Pageable pageable) {
        log.debug("REST request to get a page of CommWords");
        Page<CommWord> page = commWordRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/comm-words");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /comm-words/:id : get the "id" commWord.
     *
     * @param id the id of the commWord to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the commWord, or with status 404 (Not Found)
     */
    @GetMapping("/comm-words/{id}")
    @Timed
    public ResponseEntity<CommWord> getCommWord(@PathVariable Long id) {
        log.debug("REST request to get CommWord : {}", id);
        CommWord commWord = commWordRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(commWord));
    }

    /**
     * DELETE  /comm-words/:id : delete the "id" commWord.
     *
     * @param id the id of the commWord to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/comm-words/{id}")
    @Timed
    public ResponseEntity<Void> deleteCommWord(@PathVariable Long id) {
        log.debug("REST request to delete CommWord : {}", id);
        commWordRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
