package mars.roo.web.rest;

import java.util.ArrayList;
import java.util.List;

import mars.roo.domain.Question;
import mars.roo.domain.enumeration.Level;
import mars.roo.domain.enumeration.QuestionType;

public final class QuestionDTO {

    private final Long id;
    private final String uuid;
    private final QuestionType type;
    private final Level level;
    private final Integer indexOrder;
    private final String dynamicPart;

    public QuestionDTO(Question question) {
        id = question.getId();
        uuid = question.getUuid();
        type = question.getType();
        level = question.getLevel();
        indexOrder = question.getIndexOrder();
        dynamicPart = question.getDynamicPart();
    }

    public static List<QuestionDTO> toDTO(List<Question> questions) {
        List<QuestionDTO> result = new ArrayList<QuestionDTO>();
        questions.stream().forEach(question -> { result.add(new QuestionDTO(question)); });
        return result;
    }

    public Long getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public QuestionType getType() {
        return type;
    }

    public Level getLevel() {
        return level;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public String getDynamicPart() {
        return dynamicPart;
    }

}
