package mars.roo.web.rest.profile;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import mars.roo.domain.Profile;
import mars.roo.domain.enumeration.LearnDir;
import mars.roo.domain.enumeration.Level;

public class ProfileDTO {

    @NotEmpty
    @Size(min = 1, max = 32)
    @Pattern(regexp = "^[^<>':;/&%$]*$")
    private String dname;
    private LearnDir learnDir;
    private String language;
    private Level difficultyLevel;
    private Integer voiceSpeedRate;
    private Boolean soundEffects;
    private Boolean autoPlayVoice;
    private Boolean autoContinue;

    public ProfileDTO() {
        // default constructor needed by JsonMapping
    }

    public ProfileDTO(Profile profile) {
        dname = profile.getDname();
        learnDir = profile.getLearning();
        language = profile.getLanguage();
        difficultyLevel = profile.getDifficultyLevel();
        voiceSpeedRate = profile.getVoiceSpeedRate();
        soundEffects = profile.isSoundEffects();
        autoPlayVoice = profile.isAutoPlayVoice();
        autoContinue = profile.isAutoContinue();
    }

    public String getDname() {
        return dname;
    }

    public LearnDir getLearnDir() {
        return learnDir;
    }

    public String getLanguage() {
        return language;
    }

    public Level getDifficultyLevel() {
        return difficultyLevel;
    }

    public Integer getVoiceSpeedRate() {
        return voiceSpeedRate;
    }

    public Boolean getSoundEffects() {
        return soundEffects;
    }

    public Boolean getAutoPlayVoice() {
        return autoPlayVoice;
    }

    public Boolean getAutoContinue() {
        return autoContinue;
    }

}
