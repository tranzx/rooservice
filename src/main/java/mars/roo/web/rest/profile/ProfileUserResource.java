package mars.roo.web.rest.profile;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import mars.roo.domain.Profile;
import mars.roo.service.ProfileService;
import mars.roo.web.rest.BaseAuthenticatedResource;
import mars.roo.web.rest.errors.BadRequestAlertException;

@RestController
@RequestMapping("/api/user/profile")
public class ProfileUserResource extends BaseAuthenticatedResource {

    private final ProfileService profileService;

    public ProfileUserResource(ProfileService profileService) {
        this.profileService = profileService;
    }

    @GetMapping("/get")
    @Timed
    public ResponseEntity<ProfileDTO> getProfile(Principal principal) {
        Profile profile = getCurrentProfile(principal, null);
        if (profile == null) {
            throw new BadRequestAlertException("Profile doesn't exist", "profile", "notfound");
        }
        return ResponseEntity.ok(new ProfileDTO(profile));
    }

    @PostMapping("/create")
    @Timed
    public ResponseEntity<ProfileDTO> create(@Valid @RequestBody ProfileDTO dto, Principal principal) {
        Profile profile = getCurrentProfile(principal, dto);
        return ResponseEntity.ok(new ProfileDTO(profile));
    }

    @PostMapping("/update")
    @Timed
    public ResponseEntity<ProfileDTO> update(@Valid @RequestBody ProfileDTO dto, Principal principal) {
        Profile profile = getCurrentProfile(principal, dto);
        profile.updateWith(dto);
        Profile p = profileService.save(profile);
        return ResponseEntity.ok(new ProfileDTO(p));
    }

    private Profile getCurrentProfile(Principal principal, ProfileDTO dto) {
        Profile profile = profileService.findOneByOwner(login(principal));
        if (profile == null && dto != null) {
            profile = Profile.defaultProfile(login(principal), dto);
            profile = profileService.save(profile);
        }
        return profile;
    }

}
