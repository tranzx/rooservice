package mars.roo.web.rest;

import com.codahale.metrics.annotation.Timed;
import mars.roo.domain.ZarinpalEvent;
import mars.roo.service.ZarinpalEventService;
import mars.roo.web.rest.errors.BadRequestAlertException;
import mars.roo.web.rest.util.HeaderUtil;
import mars.roo.web.rest.util.PaginationUtil;
import mars.roo.service.dto.ZarinpalEventCriteria;
import mars.roo.service.ZarinpalEventQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ZarinpalEvent.
 */
@RestController
@RequestMapping("/api")
public class ZarinpalEventResource {

    private final Logger log = LoggerFactory.getLogger(ZarinpalEventResource.class);

    private static final String ENTITY_NAME = "zarinpalEvent";

    private final ZarinpalEventService zarinpalEventService;

    private final ZarinpalEventQueryService zarinpalEventQueryService;

    public ZarinpalEventResource(ZarinpalEventService zarinpalEventService, ZarinpalEventQueryService zarinpalEventQueryService) {
        this.zarinpalEventService = zarinpalEventService;
        this.zarinpalEventQueryService = zarinpalEventQueryService;
    }

    /**
     * POST  /zarinpal-events : Create a new zarinpalEvent.
     *
     * @param zarinpalEvent the zarinpalEvent to create
     * @return the ResponseEntity with status 201 (Created) and with body the new zarinpalEvent, or with status 400 (Bad Request) if the zarinpalEvent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/zarinpal-events")
    @Timed
    public ResponseEntity<ZarinpalEvent> createZarinpalEvent(@Valid @RequestBody ZarinpalEvent zarinpalEvent) throws URISyntaxException {
        log.debug("REST request to save ZarinpalEvent : {}", zarinpalEvent);
        if (zarinpalEvent.getId() != null) {
            throw new BadRequestAlertException("A new zarinpalEvent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ZarinpalEvent result = zarinpalEventService.save(zarinpalEvent);
        return ResponseEntity.created(new URI("/api/zarinpal-events/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /zarinpal-events : Updates an existing zarinpalEvent.
     *
     * @param zarinpalEvent the zarinpalEvent to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated zarinpalEvent,
     * or with status 400 (Bad Request) if the zarinpalEvent is not valid,
     * or with status 500 (Internal Server Error) if the zarinpalEvent couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/zarinpal-events")
    @Timed
    public ResponseEntity<ZarinpalEvent> updateZarinpalEvent(@Valid @RequestBody ZarinpalEvent zarinpalEvent) throws URISyntaxException {
        log.debug("REST request to update ZarinpalEvent : {}", zarinpalEvent);
        if (zarinpalEvent.getId() == null) {
            return createZarinpalEvent(zarinpalEvent);
        }
        ZarinpalEvent result = zarinpalEventService.save(zarinpalEvent);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, zarinpalEvent.getId().toString()))
            .body(result);
    }

    /**
     * GET  /zarinpal-events : get all the zarinpalEvents.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of zarinpalEvents in body
     */
    @GetMapping("/zarinpal-events")
    @Timed
    public ResponseEntity<List<ZarinpalEvent>> getAllZarinpalEvents(ZarinpalEventCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ZarinpalEvents by criteria: {}", criteria);
        Page<ZarinpalEvent> page = zarinpalEventQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/zarinpal-events");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /zarinpal-events/:id : get the "id" zarinpalEvent.
     *
     * @param id the id of the zarinpalEvent to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the zarinpalEvent, or with status 404 (Not Found)
     */
    @GetMapping("/zarinpal-events/{id}")
    @Timed
    public ResponseEntity<ZarinpalEvent> getZarinpalEvent(@PathVariable Long id) {
        log.debug("REST request to get ZarinpalEvent : {}", id);
        ZarinpalEvent zarinpalEvent = zarinpalEventService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(zarinpalEvent));
    }

    /**
     * DELETE  /zarinpal-events/:id : delete the "id" zarinpalEvent.
     *
     * @param id the id of the zarinpalEvent to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/zarinpal-events/{id}")
    @Timed
    public ResponseEntity<Void> deleteZarinpalEvent(@PathVariable Long id) {
        log.debug("REST request to delete ZarinpalEvent : {}", id);
        zarinpalEventService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
