package mars.roo.repository;

import java.io.Serializable;
import java.util.Collections;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.util.Assert;

import mars.roo.domain.LessonSearch;
import mars.roo.domain.LessonSearch_;

public class MyCustomSimpleJpaRepository<ENTITY, ID extends Serializable> extends
        SimpleJpaRepository<ENTITY, ID> implements JpaSpecificationExecutor<ENTITY> {

    private final EntityManager em;

    public MyCustomSimpleJpaRepository(JpaEntityInformation<ENTITY, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.em = entityManager;
    }

    public MyCustomSimpleJpaRepository(Class<ENTITY> entityClass, EntityManager em) {
        super(entityClass, em);
        this.em = em;
    }

    @Override
    protected <S extends ENTITY> TypedQuery<S> getQuery(Specification<S> spec, Class<S> domainClass, Sort sort) {
        log.debug("getQuery overrided for {}", domainClass.getSimpleName());
        if (domainClass.getSimpleName().equalsIgnoreCase("LessonSearch")) {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<S> query = builder.createQuery(domainClass);

            query.distinct(true);       // This is important for dropping duplicated results.
            Root<LessonSearch> root = applySpecificationToCriteria((Specification<LessonSearch>)spec, LessonSearch.class, query);
            query.multiselect(root.get(LessonSearch_.categoryUuid), root.get(LessonSearch_.categoryIndexOrder),
                    root.get(LessonSearch_.categoryTitleEn), root.get(LessonSearch_.categoryTitleFa), root.get(LessonSearch_.categoryTitleDe),
                    root.get(LessonSearch_.lessonUuid), root.get(LessonSearch_.lessonIndexOrder));
            if (sort != null) {
                query.orderBy(QueryUtils.toOrders(sort, root, builder));
            }
            return em.createQuery(query);
        }
        return super.getQuery(spec, domainClass, sort);
    }

    @Override
    protected <S extends ENTITY> TypedQuery<Long> getCountQuery(Specification<S> spec, Class<S> domainClass) {
        if (domainClass.getSimpleName().equalsIgnoreCase("LessonSearch")) {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Long> query = builder.createQuery(Long.class);
            Root<LessonSearch> root = applySpecificationToCriteria((Specification<LessonSearch>)spec, LessonSearch.class, query);
            query.select(builder.countDistinct(root.get(LessonSearch_.lessonUuid)));
            query.orderBy(Collections.<Order> emptyList());
            return em.createQuery(query);
        }
        return super.getCountQuery(spec, domainClass);
    }

    /**
     * See SimpleJpaRepository#applySpecificationToCriteria
     */
    private <S, U extends ENTITY> Root<LessonSearch> applySpecificationToCriteria(Specification<LessonSearch> spec, Class<LessonSearch> domainClass,
            CriteriaQuery<S> query) {

        Assert.notNull(domainClass, "Domain class must not be null!");
        Assert.notNull(query, "CriteriaQuery must not be null!");

        Root<LessonSearch> root = query.from(domainClass);

        if (spec == null) {
            return root;
        }

        CriteriaBuilder builder = em.getCriteriaBuilder();
        Predicate predicate = spec.toPredicate(root, query, builder);

        if (predicate != null) {
            query.where(predicate);
        }

        return root;
    }

    private final Logger log = LoggerFactory.getLogger(getClass());
}
