package mars.roo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mars.roo.domain.Score;


/**
 * Spring Data JPA repository for the Score entity.
 */
@Repository
public interface ScoreRepository extends JpaRepository<Score, Long>, JpaSpecificationExecutor<Score> {

    @Query(value = "SELECT sum(s.score) from Score s WHERE s.owner = :owner AND s.learn_dir = :learnDir", nativeQuery = true)
    Integer queryTotalScore(@Param("owner") String login, @Param("learnDir") String learnDir);

    @Query(value = "SELECT s.category_uuid, COUNT(DISTINCT s.lesson_uuid) FROM Score s "
            + "WHERE s.owner=:owner AND s.learn_dir=:learnDir AND s.difficulty_level=:level AND s.score_type='LESSON'"
            + "GROUP BY s.category_uuid", nativeQuery = true)
    List<Object[]> doneLessonsPerCategory(@Param("owner") String login, @Param("learnDir") String learnDir,
            @Param("level") String level);

    @Query(value = "SELECT s.lesson_uuid, MAX(s.star) FROM Score s "
            + "WHERE s.owner=:owner AND s.learn_dir=:learnDir AND s.difficulty_level=:level AND s.score_type='LESSON' "
            + "GROUP BY s.lesson_uuid", nativeQuery = true)
    List<Object[]> maxStarPerLesson(@Param("owner") String login, @Param("learnDir") String learnDir,
            @Param("level") String level);

    @Query(value = "SELECT sum(s.score), s.score_date FROM Score s "
            + "WHERE s.owner=:owner AND s.learn_dir=:learnDir AND s.score_date BETWEEN date(:from) AND date(:to)"
            + "GROUP BY s.score_date ORDER BY s.score_date", nativeQuery = true)
    List<Object[]> scoreBetween(@Param("owner") String owner, @Param("learnDir") String learnDir,
            @Param("from") String from, @Param("to") String to);

    @Query(value = "SELECT s.owner, s.total_score FROM score_rank s "
            + "WHERE s.learn_dir=:learnDir LIMIT :top", nativeQuery = true)
    List<Object[]> topScores(@Param("top") Integer top, @Param("learnDir") String learnDir);

    @Query(value = "SELECT s.owner, s.total_score, s.row_num FROM score_rank s "
            + "WHERE s.owner=:owner AND s.learn_dir=:learnDir", nativeQuery = true)
    List<Object[]> rankFor(@Param("owner") String owner, @Param("learnDir") String learnDir);

    @Query(value = "SELECT s.owner, s.total_score FROM score_rank_month s "
            + "WHERE s.learn_dir=:learnDir LIMIT :top", nativeQuery = true)
    List<Object[]> topScoresMonth(@Param("top") Integer top, @Param("learnDir") String learnDir);

    @Query(value = "SELECT s.owner, s.total_score, s.row_num FROM score_rank_month s "
            + "WHERE s.owner=:owner AND s.learn_dir=:learnDir", nativeQuery = true)
    List<Object[]> rankForMonth(@Param("owner") String owner, @Param("learnDir") String learnDir);


    @Query(value = "SELECT s.owner, s.total_score FROM score_rank_2 s "
            + "WHERE s.target=:target LIMIT :top", nativeQuery = true)
    List<Object[]> topScores2(@Param("top") Integer top, @Param("target") String targetLanguage);

    @Query(value = "SELECT s.owner, s.total_score, s.row_num FROM score_rank_2 s "
            + "WHERE s.owner=:owner AND s.target=:target", nativeQuery = true)
    List<Object[]> rankFor2(@Param("owner") String owner, @Param("target") String targetLanguage);

    @Query(value = "SELECT s.owner, s.total_score FROM score_rank_month_2 s "
            + "WHERE s.target=:target LIMIT :top", nativeQuery = true)
    List<Object[]> topScoresMonth2(@Param("top") Integer top, @Param("target") String targetLanguage);

    @Query(value = "SELECT s.owner, s.total_score, s.row_num FROM score_rank_month_2 s "
            + "WHERE s.owner=:owner AND s.target=:target", nativeQuery = true)
    List<Object[]> rankForMonth2(@Param("owner") String owner, @Param("target") String targetLanguage);
}
