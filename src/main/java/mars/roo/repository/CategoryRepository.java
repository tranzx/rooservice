package mars.roo.repository;

import mars.roo.domain.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Category entity.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category findOneByUuid(String categoryUuid);

    List<Category> findAllByOrderByIndexOrder();

}
