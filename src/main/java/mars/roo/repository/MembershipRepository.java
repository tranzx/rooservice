package mars.roo.repository;

import mars.roo.domain.CafebazaarEvent;
import mars.roo.domain.MarketEvent;
import mars.roo.domain.Membership;
import mars.roo.domain.ZarinpalEvent;

import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Membership entity.
 */
@Repository
public interface MembershipRepository extends JpaRepository<Membership, Long>, JpaSpecificationExecutor<Membership> {

    Membership findOneByZarinpalEvent(ZarinpalEvent event);

    Membership findOneByCafebazaarEvent(CafebazaarEvent event);

    List<Membership> findByCafebazaarEventOrderByEndDateDesc(CafebazaarEvent event);

    List<Membership> findByMarketEventOrderByEndDateDesc(MarketEvent event);

}
