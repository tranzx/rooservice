package mars.roo.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

public class MyCustomRepositoryFactoryBean<ENTITY, ID extends Serializable, REPO extends JpaRepository<ENTITY, ID>>
        extends JpaRepositoryFactoryBean<REPO, ENTITY, ID> {

    public MyCustomRepositoryFactoryBean(Class<? extends REPO> repositoryInterface) {
        super(repositoryInterface);
    }

    @Override
    protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
        return new MyRepositoryFactory(entityManager);
    }

    private static class MyRepositoryFactory<ENTITY, ID extends Serializable> extends JpaRepositoryFactory {

        private final EntityManager em;

        public MyRepositoryFactory(EntityManager em) {
            super(em);
            this.em = em;
        }

        protected Object getTargetRepository(RepositoryMetadata metadata) {
            MyCustomSimpleJpaRepository<?, ?> repository = new MyCustomSimpleJpaRepository<ENTITY, ID>((Class<ENTITY>) metadata.getDomainType(), em);
            return repository;
        }

        @Override
        protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
            return MyCustomSimpleJpaRepository.class;
        }
    }
}
