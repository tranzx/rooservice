package mars.roo.repository;

import mars.roo.domain.Lesson;
import mars.roo.domain.Question;
import mars.roo.domain.enumeration.Level;

import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Question entity.
 */
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long>, JpaSpecificationExecutor<Question> {

    List<Question> findByLessonAndLevelOrderByIndexOrderAsc(Lesson lesson, Level level);

    List<Question> findByLesson(Lesson lesson);

    void deleteByLesson(Lesson lesson);

}
