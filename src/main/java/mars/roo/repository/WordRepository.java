package mars.roo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import mars.roo.domain.Word;


/**
 * Spring Data JPA repository for the Word entity.
 */
@Repository
public interface WordRepository extends JpaRepository<Word, Long>, JpaSpecificationExecutor<Word> {

    Word findOneByUuid(String uuid);

}
