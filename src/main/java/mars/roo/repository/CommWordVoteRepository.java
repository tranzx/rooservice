package mars.roo.repository;

import mars.roo.domain.CommWordVote;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CommWordVote entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommWordVoteRepository extends JpaRepository<CommWordVote, Long> {

}
