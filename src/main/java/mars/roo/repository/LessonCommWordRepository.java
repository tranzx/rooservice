package mars.roo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mars.roo.domain.LessonCommWord;
import mars.roo.domain.enumeration.Language;


/**
 * Spring Data JPA repository for the LessonCommWord entity.
 */
@Repository
public interface LessonCommWordRepository extends JpaRepository<LessonCommWord, String> {

    List<LessonCommWord> findByLessonIdAndLanguage(Long lessonId, Language language);

    List<LessonCommWord> findByWordIdAndLanguage(Long wordId, Language enGb);

    List<LessonCommWord> findByCommWordUuidAndAuthor(String commWordUuid, String author);

    void deleteByCommWordUuidAndAuthor(String commWordUuid, String author);

    void deleteByCommWordUuid(String uuid);

    List<LessonCommWord> findByLessonIdAndWordId(Long lessonId, Long wordId);

    void deleteByLessonId(Long lessonId);

}
