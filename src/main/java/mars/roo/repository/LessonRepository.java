package mars.roo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import mars.roo.domain.Category;
import mars.roo.domain.Lesson;
import mars.roo.domain.enumeration.Level;


/**
 * Spring Data JPA repository for the Lesson entity.
 */
@Repository
public interface LessonRepository extends JpaRepository<Lesson, Long>, JpaSpecificationExecutor<Lesson> {

    Lesson findOneByUuid(String lessonUuid);

    Lesson findOneByCategoryAndLevelAndIndexOrder(Category category, Level level, Integer indexOrder);

    List<Lesson> findByCategoryAndLevel(Category category, Level level);

    List<Lesson> findTop2ByCategoryAndLevelOrderByIndexOrderDesc(Category category, Level level);

    Lesson findOneByIndexOrderAndCategoryIsNull(Integer indexOrder);

    List<Lesson> findByCategoryId(Long categoryId);

}
