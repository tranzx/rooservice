package mars.roo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import mars.roo.domain.Lesson;
import mars.roo.domain.LessonWord;
import mars.roo.domain.Word;


/**
 * Spring Data JPA repository for the LessonWord entity.
 */
@Repository
public interface LessonWordRepository extends JpaRepository<LessonWord, Long>, JpaSpecificationExecutor<LessonWord> {

    LessonWord findOneByLessonAndWord(Lesson lesson, Word word);

    Long countByLesson(Lesson lesson);

    List<LessonWord> findByLesson(Lesson lesson);

    void deleteByLesson(Lesson lesson);

    LessonWord findTop1ByLessonOrderByIndexOrderDesc(Lesson lesson);

    List<LessonWord> findByLessonOrderByIndexOrderAsc(Lesson lesson);

    List<LessonWord> findByWord(Word word);

}
