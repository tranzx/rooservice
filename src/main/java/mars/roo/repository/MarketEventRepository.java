package mars.roo.repository;

import mars.roo.domain.MarketEvent;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MarketEvent entity.
 */
@Repository
public interface MarketEventRepository extends JpaRepository<MarketEvent, Long> {

    MarketEvent findOneByPurchaseToken(String purchaseToken);

}
