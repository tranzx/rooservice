package mars.roo.repository;

import mars.roo.domain.LessonSearch;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the LessonSearch entity.
 */
@Repository
public interface LessonSearchRepository extends JpaRepository<LessonSearch, Long>, JpaSpecificationExecutor<LessonSearch> {

}
