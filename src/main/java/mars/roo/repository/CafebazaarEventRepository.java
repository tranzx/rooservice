package mars.roo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mars.roo.domain.CafebazaarEvent;


/**
 * Spring Data JPA repository for the CafebazaarEvent entity.
 */
@Repository
public interface CafebazaarEventRepository extends JpaRepository<CafebazaarEvent, Long> {

    CafebazaarEvent findOneByPurchaseToken(String purchaseToken);

}
