package mars.roo.repository;

import mars.roo.domain.ZarinpalEvent;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ZarinpalEvent entity.
 */
@Repository
public interface ZarinpalEventRepository extends JpaRepository<ZarinpalEvent, Long>, JpaSpecificationExecutor<ZarinpalEvent> {

    ZarinpalEvent findOneByPaymentToken(String authority);

}
