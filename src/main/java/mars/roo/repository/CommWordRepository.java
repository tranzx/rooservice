package mars.roo.repository;

import mars.roo.domain.CommWord;
import mars.roo.domain.Word;
import mars.roo.domain.enumeration.Language;

import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CommWord entity.
 */
@Repository
public interface CommWordRepository extends JpaRepository<CommWord, Long> {

    CommWord findOneByWordAndLanguageAndAuthor(Word word, Language language, String author);

    CommWord findOneByUuidAndAuthor(String uuid, String author);

    CommWord findOneByUuid(String uuid, String login);

    CommWord findOneByWordAndLanguageAndText(Word word, Language language, String text);

    List<CommWord> findByWordAndLanguageAndText(Word word, Language language, String text);

    Long countDistinctWordIdByLanguage(Language language);

}
