package mars.roo.config;

import java.util.concurrent.TimeUnit;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.github.jhipster.config.JHipsterProperties;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache("oAuth2Authentication", jcacheConfiguration);
            cm.createCache(mars.roo.domain.Category.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.Lesson.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.Question.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.Score.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.Profile.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.ZarinpalEvent.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.Membership.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.LessonWord.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.Word.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.CafebazaarEvent.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.LessonSearch.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.CommWord.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.CommWordVote.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.LessonCommWord.class.getName(), jcacheConfiguration);
            cm.createCache(mars.roo.domain.MarketEvent.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
